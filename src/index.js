/**
 * 云技师 - Index
 * http://menger.me
 * @大梦
 */

'use strict';
import React from 'react'
import {View, StyleSheet, NetInfo} from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import JPushModule from 'jpush-react-native'
import JShareModule from 'jshare-react-native'

import Navigation from './routers/Navigation'

export default class Index extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this._initSetting(); // 初始化设置，先调用
        this._handleLoginState(); // 处理登陆状态
        this._handlePushListener(); // 通知
    }

    componentWillUnmount() {
        this._removePushListener();
    };

    _initSetting = () => {
        if (__IOS__) {
            // 启动极光分享 ios需要
            JShareModule.setup();
            JPushModule.setBadge(0, (badgeNumber) => {});
        } else {
            // 仅仅在安卓调用
            JPushModule.initPush();
            JPushModule.notifyJSDidLoad(resultCode => {
                if (resultCode === 0) {
                }
            });
        }
        // debug模式
        JShareModule.setDebug({enable: __DEV__});
        //设置微信ID
        // XPay.setWxId(Constant.WECHAT_APPID);
        //设置支付宝URL Schemes
        // XPay.setAlipayScheme(Constant.ALIPAY_SCHME);
    };

    setAlias = (alias) => {
        JPushModule.getAlias(map => {
            if (map.errorCode === 0 && map.alias) {
                console.log('Get alias succeed, alias: ' + map.alias)
            } else {
                console.log('Get alias failed, errorCode: ' + map.errorCode);
                JPushModule.setAlias(alias, map => {
                    if (map.errorCode === 0) {
                        console.log('set alias succeed', map);
                    } else {
                        console.log('set alias failed, errorCode: ', map.errorCode);
                    }
                })
            }
        })
    };

    deleteAlias = () => {
        JPushModule.deleteAlias(map => {
            if (map.errorCode === 0) {
                console.log('delete alias succeed')
            } else {
                console.log('delete alias failed, errorCode: ', map.errorCode)
            }
        })
    };

    setBadge = () => {
        JPushModule.setBadge(0, (badgeNumber) => {});
    }

    _handlePushListener = () => {
        JPushModule.addReceiveCustomMsgListener(map => {
            // 自定义消息
            console.log('extras: ' + map.extras);
        });

        JPushModule.addReceiveNotificationListener(map => {
            // 接收推送事件     并且点击推送（ios9及以下）需要判断系统版本
            console.log('alertContent: ', map.alertContent);
            console.log('extras: ', map.extras);
        });

        JPushModule.addReceiveOpenNotificationListener(map => {
            __IOS__ && this.setBadge();
            // 点击推送事件，iOS10及以上
            // console.log('Opening notification!');
            console.log('map.extra: ' + map.extras);
        });

        JPushModule.addGetRegistrationIdListener(registrationId => {
            // 设备注册成功
            console.log('Device register succeed, registrationId ' + registrationId);
        });
    };

    _removePushListener = () => {
        // JPushModule.removeReceiveCustomMsgListener();
        // JPushModule.removeReceiveNotificationListener();
        // JPushModule.removeReceiveOpenNotificationListener();
        // JPushModule.removeGetRegistrationIdListener();
        JPushModule.clearAllNotifications();
    };

    _requestWelcomeData = async (component) => {
        let url = ServicesApi.systemSetting;
        let result = await Services.Get(url, true);
        if (result && result.code === 1) {
            global.showAd = result.data.show_ad;
            if (result.data.show_ad === 1) {
                RouterHelper.reset('', 'Welcome', {
                    component,
                    bannerData: result.data.ad_data,
                    onCallBack: () => {
                        this.timer && clearTimeout(this.timer);
                    }
                });
                this.timer = setTimeout(() => {
                    RouterHelper.reset('', component);
                }, 4000);
            } else {
                RouterHelper.reset('', component);
            }
            SplashScreen.hide();
        }
    };

    _handleLoginState = async () => {
        const localData = await StorageManager.load(Constant.USER_INFO_KEY);
        // console.log('本地信息--->', localData);
        if (localData.code === 1) {
            if (localData.data.token === undefined || localData.data.token === '') {
                // 未登录
                this.deleteAlias();
                this._requestWelcomeData('Login');
                // SplashScreen.hide();
            } else {
                // 已经登录;
                this.loginOfToken(localData.data);
            }
        } else {
            // 第一次安装app
            this._requestWelcomeData('Login');
            // SplashScreen.hide();
        }
    };

    loginOfToken = async (userInfo) => {
        // console.log(userInfo);
        global.token = userInfo.token;
        let url = ServicesApi.get_member_info;
        try {
            let result = await Services.Get(url);
            if (result && parseInt(result.code) === 1) {
                this.setAlias(userInfo.token);
                this.saveUserInfo(userInfo);
                this._requestWelcomeData('Tab');
                // SplashScreen.hide();
            } else {
                Toast.toastShort('用户登录失效，请重新登录');
                this.deleteAlias();
                this.cleanUserInfo();
                this._requestWelcomeData('Login');
                // SplashScreen.hide();
            }
        } catch (e) {
            this.deleteAlias();
            // console.log(e);
            SplashScreen.hide();
        }
    };

    saveUserInfo = (userInfo) => {
        // console.log(userInfo);
        global.token = userInfo.token;
        StorageManager.save(Constant.USER_INFO_KEY, userInfo);
    };

    cleanUserInfo = () => {
        global.token = '';
        StorageManager.remove(Constant.USER_INFO_KEY);
    };

    render() {
        return (
            <View style={styles.container}>
                <Navigation/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});