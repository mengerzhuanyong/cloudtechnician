/**
 * 云技师 - RouterConfig
 * http://menger.me
 * @大梦
 */

'use strict';

import {createStackNavigator, createBottomTabNavigator} from 'react-navigation'
import StackViewStyleInterpolator from 'react-navigation/src/views/StackView/StackViewStyleInterpolator'
import {configRouter, tabOptions} from './RouterTool'

// Welcome
import Welcome from '../pages/welcome/Welcome'
// WebViewPage
import WebViewPage from '../pages/common/webview'
// Login
import Login from '../pages/login'
// Register
import Register from '../pages/login/register'
// RetrievePassword
import RetrievePassword from '../pages/login/retrievePassword'
// Home
import Home from '../pages/home'
// About
import About from '../pages/about'
// Invest
import Invest from '../pages/invest'
// Forum
import Forum from '../pages/forum/forum'
// ArticleList
import ArticleList from '../pages/forum/ArticleList'
// Fund
import Fund from '../pages/fund'
// FundDetail
import FundDetail from '../pages/fund/fundDetail'
// JijinHetong
import JijinHetong from '../pages/fund/jijinHetong'
// JijinGonggao
import JijinGonggao from '../pages/fund/jijinGonggao'
// Renshengou
import Renshengou from '../pages/fund/renshengou'
// Jisuanqi
import Jisuanqi from '../pages/fund/jisuanqi'
// Renzheng
import Renzheng from '../pages/fund/renzheng'
// RenzhengType
import RenzhengType from '../pages/fund/renzhengType'
// Yinhangka
import Yinhangka from '../pages/fund/yinhangka'
// AddBank
import AddBank from '../pages/fund/addBank'
// Pinggu
import Pinggu from '../pages/fund/pinggu'
// UserRetrial
import UserRetrial from '../pages/fund/userRetrial'
// Tijiaochenggong
import Tijiaochenggong from '../pages/fund/tijiaochenggong'
// Mine
import Mine from '../pages/user'
// Shezhi
import Shezhi from '../pages/user/shezhi'
// Qiandao
import Qiandao from '../pages/user/qiandao'
// Tuijian
import Tuijian from '../pages/user/tuijian'
// TuijianZhuce
import TuijianZhuce from '../pages/user/tuijianZhuce'
// TuijianBangka
import TuijianBangka from '../pages/user/tuijianBangka'
// TuijianShoutou
import TuijianShoutou from '../pages/user/tuijianShoutou'
// Jifen
import Jifen from '../pages/user/jifen'
// Huiyuan
import Huiyuan from '../pages/user/huiyuan'
// Zichan
import Zichan from '../pages/user/zichan'
// Shangcheng
import Shangcheng from '../pages/user/shangcheng'
// Shoucang
import Shoucang from '../pages/user/shoucang'
// Guanyu
import Guanyu from '../pages/user/guanyu'
// Xieyi
import Xieyi from '../pages/user/xieyi'
// Gerenxinxi
import Gerenxinxi from '../pages/user/gerenxinxi'
// SetMima
import SetMima from '../pages/user/setMima'
// SetMobile
import SetMobile from '../pages/user/setMobile'
// Jijin
import Jijin from '../pages/user/jijin'
// Order
import Order from '../pages/user/order'
// OrderRenshengouDetail
import OrderRenshengouDetail from '../pages/user/orderRenshengouDetail'
// OrderShuhuiDetail
import OrderShuhuiDetail from '../pages/user/orderShuhuiDetail'
// OrderZhuanrangDetail
import OrderZhuanrangDetail from '../pages/user/orderZhuanrangDetail'
// Shuhui
import Shuhui from '../pages/user/shuhui'
// Zhuanrang
import Zhuanrang from '../pages/user/zhuanrang'
// Erweima
import Erweima from '../pages/user/erweima'
// Leijishouyi
import Leijishouyi from '../pages/user/leijishouyi'
// NewsWebDetail
import NewsWebDetail from '../pages/common/newsWebDetail'
// Xiaoxi
import Xiaoxi from '../pages/xiaoxi'
// Kefu
import Kefu from '../pages/kefu'
// XiaoxiDetail
import XiaoxiDetail from '../pages/common/xiaoxiDetail'
// Setting
import Setting from "../pages/user/setting";
// OrderManage
import OrderManage from "../pages/invest/OrderManage";
// TrainContent
import TrainContent from "../pages/about/TrainContent";
// ProductInfo
import ProductInfo from "../pages/about/ProductInfo";
// OrderDetail
import OrderDetail from "../pages/invest/orderDetail";
// InputQualityBill
import InputQualityBill from "../pages/invest/InputQualityBill";
// Tixian
import Tixian from  '../pages/user/tixian'
// DateManage
import DateManage from "../pages/user/DateManage";
// ShareUtils
import ShareUtils from "../components/common/shareUtils";

const Tab = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: tabOptions({
            title: '首页',
            normalIcon: Images.icon_tabbar_home,
            selectedIcon: Images.icon_tabbar_home_cur
        })
    },
    Forum: {
        screen: Forum,
        navigationOptions: tabOptions({
            title: '云社区',
            normalIcon: Images.icon_tabbar_forum,
            selectedIcon: Images.icon_tabbar_forum_cur
        })
    },
    OrderManage: {
        screen: OrderManage,
        navigationOptions: tabOptions({
            title: '订单',
            normalIcon: Images.icon_tabbar_order,
            selectedIcon: Images.icon_tabbar_order_cur
        })
    },
    Mine: {
        screen: Mine,
        navigationOptions: tabOptions({
            title: '我的',
            normalIcon: Images.icon_tabbar_mine,
            selectedIcon: Images.icon_tabbar_mine_cur
        })
    },
}, {
    initialRouteName: 'Home',
    tabBarOptions: {
        showIcon: true,
        indicatorStyle: {height: 0},
        activeTintColor: GlobalStyles.themeColor,
        style: {
            backgroundColor: '#fff'
        },
        tabStyle: {
            margin: 2,
        },
    },
    lazy: true, //懒加载
    swipeEnabled: false,
    animationEnabled: false, //关闭安卓底栏动画
    tabBarPosition: 'bottom',
});

const StackNavigator = createStackNavigator(configRouter({
    Tab: {screen: Tab},
    WebViewPage: {screen: WebViewPage},
    Welcome: {screen: Welcome},
    Login: {screen: Login},
    Register: {screen: Register},
    RetrievePassword: {screen: RetrievePassword},
    Invest: {screen: Invest},
    About: {screen: About},
    TrainContent:{screen:TrainContent},
    Xiaoxi: {screen: Xiaoxi},
    Kefu: {screen: Kefu},
    ShareUtils: {screen: ShareUtils},
    FundDetail: {screen: FundDetail},
    JijinHetong: {screen: JijinHetong},
    JijinGonggao: {screen: JijinGonggao},
    Renshengou: {screen: Renshengou},
    Jisuanqi: {screen: Jisuanqi},
    Renzheng: {screen: Renzheng},
    RenzhengType: {screen: RenzhengType},
    Yinhangka: {screen: Yinhangka},
    AddBank: {screen: AddBank},
    Pinggu: {screen: Pinggu},
    UserRetrial: {screen: UserRetrial},
    Tixian: {screen: Tixian},
    Tijiaochenggong: {screen: Tijiaochenggong},
    NewsWebDetail: {screen: NewsWebDetail},
    Shezhi: {screen: Shezhi},
    Qiandao: {screen: Qiandao},
    Tuijian: {screen: Tuijian},
    TuijianZhuce: {screen: TuijianZhuce},
    TuijianBangka: {screen: TuijianBangka},
    TuijianShoutou: {screen: TuijianShoutou},
    Jifen: {screen: Jifen},
    Huiyuan: {screen: Huiyuan},
    DateManage: {screen: DateManage},
    Zichan: {screen: Zichan},
    Shangcheng: {screen: Shangcheng},
    Shoucang: {screen: Shoucang},
    Guanyu: {screen: Guanyu},
    Setting:{screen:Setting},
    Xieyi: {screen: Xieyi},
    Mine: {screen: Mine},
    Home: {screen: Home},
    Fund: {screen: Fund},
    SetMobile: {screen: SetMobile},
    Jijin: {screen: Jijin},
    Order: {screen: Order},
    OrderManage:{screen: OrderManage},
    OrderRenshengouDetail: {screen: OrderRenshengouDetail},
    OrderShuhuiDetail: {screen: OrderShuhuiDetail},
    OrderZhuanrangDetail: {screen: OrderZhuanrangDetail},
    Shuhui: {screen: Shuhui},
    Zhuanrang: {screen: Zhuanrang},
    Erweima: {screen: Erweima},
    Leijishouyi: {screen: Leijishouyi},
    ProductInfo:{screen: ProductInfo},
    SetMima: {screen: SetMima},
    Gerenxinxi: {screen: Gerenxinxi},
    XiaoxiDetail: {screen: XiaoxiDetail},
    OrderDetail:{screen: OrderDetail},
    InputQualityBill:{screen:InputQualityBill},
    ArticleList:{screen:ArticleList},

}), {
    initialRouteName: 'Login',
    cardStyle: {
        shadowOpacity: 0,
        shadowRadius: 0,
        backgroundColor: GlobalStyles.pageBackgroundColor,
    },
    navigationOptions: {
        header: null,
        gesturesEnabled: true
    },
    transitionConfig: () => {
        return {
            screenInterpolator: (sceneProps) => {
                return StackViewStyleInterpolator.forHorizontal(sceneProps)
            },
        }
    }
});

export {StackNavigator};
