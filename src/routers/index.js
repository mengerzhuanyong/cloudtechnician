/**
 * 速芽物流 - NAVIGATOR
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    View,
} from 'react-native'
import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation'
// import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator'
import GlobalStyles from '../constants/GlobalStyles'

import stroage from '../store'
import '../store/Global'
import { consoleLog } from '../utils/utilsToast'

import TabBarItem from '../components/common/TabBarItem'
import ShareUtils from '../components/common/shareUtils'
import WebViewPage from '../pages/common/webview'

import Login from '../pages/login'
import Register from '../pages/login/register'
import Repassword from '../pages/login/retrievePassword'

import Home from '../pages/home'

import About from '../pages/about'

import Invest from '../pages/invest'

import Fund from '../pages/fund'
import FundDetail from '../pages/fund/fundDetail'
import JijinHetong from '../pages/fund/jijinHetong'
import JijinGonggao from '../pages/fund/jijinGonggao'
import Renshengou from '../pages/fund/renshengou'
import Jisuanqi from '../pages/fund/jisuanqi'
import Renzheng from '../pages/fund/renzheng'
import RenzhengType from '../pages/fund/renzhengType'
import Yinhangka from '../pages/fund/yinhangka'
import AddBank from '../pages/fund/addBank'
import Pinggu from '../pages/fund/pinggu'
import Tijiaochenggong from '../pages/fund/tijiaochenggong'

import User from '../pages/user'
import Shezhi from '../pages/user/shezhi'
import Qiandao from '../pages/user/qiandao'
import Tuijian from '../pages/user/tuijian'
import TuijianZhuce from '../pages/user/tuijianZhuce'
import TuijianBangka from '../pages/user/tuijianBangka'
import TuijianShoutou from '../pages/user/tuijianShoutou'
import Jifen from '../pages/user/jifen'
import Huiyuan from '../pages/user/huiyuan'
import Zichan from '../pages/user/zichan'
import Shangcheng from '../pages/user/shangcheng'
import Shoucang from '../pages/user/shoucang'
import Guanyu from '../pages/user/guanyu'
import Xieyi from '../pages/user/xieyi'
import Gerenxinxi from '../pages/user/gerenxinxi'
import SetMima from '../pages/user/setMima'
import SetMobile from '../pages/user/setMobile'
import Jijin from '../pages/user/jijin'
import Order from '../pages/user/order'
import OrderRenshengouDetail from '../pages/user/orderRenshengouDetail'
import OrderShuhuiDetail from '../pages/user/orderShuhuiDetail'
import OrderZhuanrangDetail from '../pages/user/orderZhuanrangDetail'
import Shuhui from '../pages/user/shuhui'
import Zhuanrang from '../pages/user/zhuanrang'
import Erweima from '../pages/user/erweima'
import Leijishouyi from '../pages/user/leijishouyi'

import NewsWebDetail from '../pages/common/newsWebDetail'

import Xiaoxi from '../pages/xiaoxi'
import Kefu from '../pages/kefu'
import XiaoxiDetail from '../pages/common/xiaoxiDetail'
import Setting from "../pages/user/setting";
import OrderManage from "../pages/invest/OrderManage";
import TrainContent from "../pages/about/TrainContent";
import ProductInfo from "../pages/about/ProductInfo";
import OrderDetail from "../pages/invest/orderDetail";
import InputQualityBill from "../pages/invest/InputQualityBill";

import Tixian from  '../pages/user/tixian'
import DateManage from "../pages/user/DateManage";

const TabNavScreen = TabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: ({ navigation }) => ({
                header: null,
                tabBarLabel: '首页',
                tabBarIcon: ({focused, tintColor}) => (
                    <TabBarItem
                        subScript = {false}
                        tintColor = {tintColor}
                        focused = {focused}
                        normalImage = {Images.icon_tabbar_home}
                        selectedImage = {Images.icon_tabbar_home_cur}
                    />
                ),
            }),
        },
        Fund: {
            screen: Fund,
            navigationOptions: ({ navigation }) => ({
                header: null,
                tabBarLabel: '云社区',
                tabBarIcon: ({focused, tintColor}) => (
                    <TabBarItem
                        subScript = {false}
                        tintColor = {tintColor}
                        focused = {focused}
                        normalImage = {Images.icon_tabbar_fund}
                        selectedImage = {Images.icon_tabbar_fund_cur}
                    />
                ),
            }),
        },
        User: {
            screen: User,
            navigationOptions: ({ navigation }) => ({
                header: null,
                tabBarLabel: '我的',
                tabBarIcon: ({focused, tintColor}) => (
                    <TabBarItem
                        subScript = {false}
                        tintColor = {tintColor}
                        focused = {focused}
                        normalImage = {Images.icon_tabbar_mine}
                        selectedImage = {Images.icon_tabbar_mine_cur}
                    />
                ),
            }),
        },
    },
    {
        initialRouteName: 'Home',
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        swipeEnabled: true,
        tabBarOptions: {
            activeTintColor: '#617995',
            inactiveTintColor: '#a4aab3',
            style: { backgroundColor: '#ffffff' },
            labelStyle: { fontSize: 12, marginBottom: 4,}
        },
    }

);

const App = StackNavigator(
    {
        TabNavScreen: {
            screen: TabNavScreen
        },
        WebViewPage: {
            screen: WebViewPage,
            navigationOptions: ({navigation}) => ({
                header: null,
            }),
        },
        Login: {
            screen: Login,
            navigationOptions: ({navigation}) => ({
                header: null,
                title: '账号登录',
            }),
        },
        Register: {
            screen: Register,
            navigationOptions: ({navigation}) => ({
                title: '账号注册',
            }),
        },
        Repassword: {
            screen: Repassword,
            navigationOptions: ({navigation}) => ({
                title: '忘记密码',
            }),
        },
        Invest: {
            screen: Invest,
            navigationOptions: ({navigation}) => ({
                title: '投资视界',
            }),
        },
        About: {
            screen: About,
            navigationOptions: ({navigation}) => ({
                title: '走进我们',
            }),
        },
        TrainContent:{
            screen:TrainContent,
            navigationOptions: ({navigation}) => ({
                title: '内容精选',
            }),

        },
        Xiaoxi: {
            screen: Xiaoxi,
            navigationOptions: ({navigation}) => ({
                title: '消息',
            }),
        },
        Kefu: {
            screen: Kefu,
            navigationOptions: ({navigation}) => ({
                title: '客服',
            }),
        },
        ShareUtils: {
            screen: ShareUtils,
            navigationOptions: ({navigation}) => ({
                title: '分享',
            }),
        },
        FundDetail: {
            screen: FundDetail,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        JijinHetong: {
            screen: JijinHetong,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        JijinGonggao: {
            screen: JijinGonggao,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        Renshengou: {
            screen: Renshengou,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        Jisuanqi: {
            screen: Jisuanqi,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        Renzheng: {
            screen: Renzheng,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        RenzhengType: {
            screen: RenzhengType,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        Yinhangka: {
            screen: Yinhangka,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        AddBank: {
            screen: AddBank,
            navigationOptions: ({navigation}) => ({
                title: '添加银行卡',
            }),
        },
        Pinggu: {
            screen: Pinggu,
            navigationOptions: ({navigation}) => ({
                title: '技能认证',
            }),
        },
        Tixian: {
            screen: Tixian,
            navigationOptions: ({navigation}) => ({
                title: '我的提现',
            }),
        },
        Tijiaochenggong: {
            screen: Tijiaochenggong,
            navigationOptions: ({navigation}) => ({
                title: '基金详情',
            }),
        },
        NewsWebDetail: {
            screen: NewsWebDetail,
            navigationOptions: ({navigation}) => ({
                title: '新闻详情',
            }),
        },
        Shezhi: {
            screen: Shezhi,
            navigationOptions: ({navigation}) => ({
                title: '设置',
            }),
        },
        Qiandao: {
            screen: Qiandao,
            navigationOptions: ({navigation}) => ({
                title: '签到',
            }),
        },
        Tuijian: {
            screen: Tuijian,
            navigationOptions: ({navigation}) => ({
                title: '推荐',
            }),
        },
        TuijianZhuce: {
            screen: TuijianZhuce,
            navigationOptions: ({navigation}) => ({
                title: '推荐',
            }),
        },
        TuijianBangka: {
            screen: TuijianBangka,
            navigationOptions: ({navigation}) => ({
                title: '推荐',
            }),
        },
        TuijianShoutou: {
            screen: TuijianShoutou,
            navigationOptions: ({navigation}) => ({
                title: '推荐',
            }),
        },
        Jifen: {
            screen: Jifen,
            navigationOptions: ({navigation}) => ({
                title: '我的积分',
            }),
        },
        Huiyuan: {
            screen: Huiyuan,
            navigationOptions: ({navigation}) => ({
                title: '会员中心',
            }),
        },
        DateManage: {
            screen: DateManage,
            navigationOptions: ({navigation}) => ({
                title: '会员中心',
            }),
        },
        Zichan: {
            screen: Zichan,
            navigationOptions: ({navigation}) => ({
                title: '我的资产',
            }),
        },
        Shangcheng: {
            screen: Shangcheng,
            navigationOptions: ({navigation}) => ({
                title: '积分商城',
            }),
        },
        Shoucang: {
            screen: Shoucang,
            navigationOptions: ({navigation}) => ({
                title: '我的收藏',
            }),
        },
        Guanyu: {
            screen: Guanyu,
            navigationOptions: ({navigation}) => ({
                title: '关于我们',
            }),
        },
        Setting:{
          screen:Setting,
            navigationOptions: ({navigation}) => ({
                title: '设置',
            }),
        },
        Xieyi: {
            screen: Xieyi,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        User: {
            screen: User,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Home: {
            screen: Home,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Fund: {
            screen: Fund,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        SetMobile: {
            screen: SetMobile,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Jijin: {
            screen: Jijin,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Order: {
            screen: Order,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        OrderManage:{
            screen: OrderManage,
            navigationOptions: ({navigation}) => ({
                title: '订单管理',
            }),
        },
        OrderRenshengouDetail: {
            screen: OrderRenshengouDetail,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        OrderShuhuiDetail: {
            screen: OrderShuhuiDetail,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        OrderZhuanrangDetail: {
            screen: OrderZhuanrangDetail,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Shuhui: {
            screen: Shuhui,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Zhuanrang: {
            screen: Zhuanrang,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Erweima: {
            screen: Erweima,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Leijishouyi: {
            screen: Leijishouyi,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        ProductInfo:{
            screen: ProductInfo,
            navigationOptions: ({navigation}) => ({
                title: '产品内容精选',
            }),
        },
        SetMima: {
            screen: SetMima,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        Gerenxinxi: {
            screen: Gerenxinxi,
            navigationOptions: ({navigation}) => ({
                title: '个人信息',
            }),
        },
        XiaoxiDetail: {
            screen: XiaoxiDetail,
            navigationOptions: ({navigation}) => ({
                title: '用户协议',
            }),
        },
        OrderDetail:{
            screen: OrderDetail,
            navigationOptions: ({navigation}) => ({
                title: '订单管理',
            }),
        },
        InputQualityBill:{
            screen:InputQualityBill,
            navigationOptions: ({navigation}) => ({
                title: '质保单录入',
            }),
        }
    },
    {
        mode: 'card',
        headerMode: 'screen',
        initialRouteName: 'Login',
        navigationOptions: {
            header: null,
            headerBackTitle: null,
            headerTintColor: GlobalStyles.themeColor,
            headerStyle: {
                backgroundColor: '#fff',
            },
            showIcon: true,
            headerTitleStyle: {
                alignSelf: 'center',
            },
            headerRight: (
                <View />
            ),
        },
        // transitionConfig:()=>({
        //     screenInterpolator: CardStackStyleInterpolator.forHorizontal,
        // }),
    }
);
const defaultGetStateForAction = App.router.getStateForAction;

App.router.getStateForAction = (action, state) => {

    if ((action.routeName === 'User' && !global.user.loginState) || (action.routeName === 'Kefu' && !global.user.loginState) || (action.routeName === 'Xiaoxi' && !global.user.loginState)) {
        this.routes = [
            ...state.routes,
            {
                key: 'id-' + Date.now(),
                routeName: 'Login',
                params: {
                    name: 'name1'
                }
            },
        ];
        return {
            ...state,
            routes,
            index: this.routes.length - 1,
        };
    }
    return defaultGetStateForAction(action, state);
};

export default App;