/**
 * 云技师 - ArticleTabList
 * http://menger.me
 * @大梦
 */


'use strict';

import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';

import SpinnerLoading from "../../components/loading/SpinnerLoading";
import ListView from "../../components/list/ListView";
import ArticleItem from "../../components/item/ArticleItem";
import {HorizontalLine} from "../../components/common/commonLine";

export default class ArticleTabList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            loading: false,
            dataSource: [],
            cate_id: this.props.cate_id,
        };
        this.page = 1;
        this.pageSize = 10;
    }

    static defaultProps = {
        cate_id: 0,
    };

    componentDidMount() {
        let {cate_id} = this.props;
        this.requestDataSource(this.page, cate_id);
    }

    componentWillReceiveProps(nextProps) {
        // console.log('componentWillReceiveProps---->', nextProps);
        if (nextProps.cate_id !== this.props.cate_id) {
            this.page = 1;
            this.requestDataSource(this.page, nextProps.cate_id);
        }
    }

    componentWillUnmount() {
        let timers = [this.timer1, this.timer2];
        ClearTimer(timers);
    }

    _captureRef = (v) => {
        this.flatListRef = v;
    };

    _keyExtractor = (item, index) => {
        return `z_${index}`
    };

    requestDataSource = async (page, cate_id) => {
        let {dataSource} = this.state;
        let {sort_value} = this.props;
        let url = ServicesApi.ArticleList;
        let data = {
            page,
            cate_id,
            desc: sort_value,
            page_size: this.pageSize,
        };
        try {
            let result = await Services.Post(url, data, true);
            let endStatus = false;
            if (result && parseInt(result.code) === 1) {
                endStatus = result.data.article_list.length < data.page_size;
                if (data.page === 1) {
                    dataSource = result.data.article_list;
                } else {
                    let temp = dataSource;
                    if (result.data.article_list.length !== 0) {
                        dataSource = temp.concat(result.data.article_list)
                    }
                }
            } else {
                endStatus = true;
            }
            this.onStopLoading(dataSource, endStatus);
        } catch (e) {
            console.log('e---->', e);
            this.onStopLoading(dataSource, true);
        }
    };

    onStopLoading = (dataSource, status = true) => {
        this.setState({
            ready: true,
            dataSource,
        });
        this.flatListRef && this.flatListRef.stopRefresh();
        this.flatListRef && this.flatListRef.stopEndReached({allLoad: status});
    }

    _onRefresh = () => {
        this.page = 1;
        let {cate_id} = this.props;
        this.requestDataSource(this.page, cate_id);
    };

    _onEndReached = () => {
        this.page++;
        let {cate_id} = this.props;
        this.requestDataSource(this.page, cate_id);
    };

    renderItemSeparatorComponent = () => {
        return <HorizontalLine lineStyle={styles.horLine}/>;
    };

    _renderEmptyComponent = () => {
        return (
            <View style={GlobalStyles.emptyComponentView}>
                <Text style={GlobalStyles.emptyText}>亲！您还没有相关的订单哦</Text>
            </View>
        );
    };

    _renderListItem = ({item, index}) => {
        return (
            <ArticleItem
                item={item}
                onPushToDetail={() => this.onPushToDetail(item)}
                onCallBack={() => this._onRefresh()}
                {...this.props}
            />
        );
    };

    onPushToDetail = (item) => {
        RouterHelper.navigate(item.title, 'NewsWebDetail', {link: item.article_url})
    };

    render() {
        let {ready, dataSource} = this.state;
        return (
            <View style={styles.container}>
                {ready ?
                    <ListView
                        style={styles.listContent}
                        initialRefresh={true}
                        ref={this._captureRef}
                        data={dataSource}
                        removeClippedSubviews={false}
                        renderItem={this._renderListItem}
                        keyExtractor={this._keyExtractor}
                        onEndReached={this._onEndReached}
                        onRefresh={this._onRefresh}
                        ItemSeparatorComponent={this.renderItemSeparatorComponent}
                    />
                    : <SpinnerLoading/>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    // 内容区
    content: {
        flex: 1,
    },
    headerComponentView: {},
    // 列表区
    listContent: {
        flex: 1,
        width: SCREEN_WIDTH,
        backgroundColor: '#fff'
    },
    horLine: {
        marginVertical: 10,
        backgroundColor: '#dbdbdb',
    },
});