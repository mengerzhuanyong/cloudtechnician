/**
 * 云技师 - ArticleItem
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'


export default class ArticleItem extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        item: {
            id: '',
            title: '',
            thumb: '',
            create_time: '',
            is_collection: '',
            views: '',
            article_url: '',
        },
        onPress: () => {},
        onCallBack: () => {},
        separator: false,
    };
    
    render() {
        let {item, onPress, separator, onCallBack} = this.props;
        return (
            <TouchableOpacity
                onPress={() => RouterHelper.navigate(item.title, 'NewsWebDetail', {
                    link: item.article_url,
                    content_id: item.id,
                    is_collection: item.is_collection,
                    onCallBack: () => onCallBack(),
                })}
                style={[styles.articleItemStyle, GlobalStyles.flexRowBetween, separator && styles.topSeparator]}
            >
                <View style={[styles.articleThumbView]}>
                    <Image source={item.thumb ? {uri: item.thumb} : Images.img_logo} style={styles.articleThumbImg}/>
                </View>
                <View style={[styles.articleContentItem]}>
                    <View style={styles.itemTitleView}>
                        <Text style={styles.itemTitle}>{item.title}</Text>
                    </View>
                    <View style={[styles.itemInfoCon, GlobalStyles.flexRowBetween]}>
                        <Text style={styles.infoText}>{item.create_time}</Text>
                        <Text style={styles.infoText}>阅读量：{item.views}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

}

const styles = StyleSheet.create({
    articleItemStyle: {
        flex: 1,
        paddingVertical: 10,
        marginHorizontal: 15,
        backgroundColor: '#fff'
    },
    topSeparator: {
        borderColor: '#ddd',
        borderTopWidth: GlobalStyles.minPixel,
    },
    articleThumbView: {
        marginRight: 15,
        borderRadius: 8,
        overflow: 'hidden',
        width: ScaleSize(160),
        height: ScaleSize(100),
    },
    articleThumbImg: {
        borderRadius: 8,
        resizeMode: 'contain',
        width: ScaleSize(160),
        height: ScaleSize(100),
    },
    articleContentItem: {
        flex: 1,
        height: ScaleSize(100),
        // backgroundColor: '#123',
        justifyContent: 'space-around',
    },
    itemInfoView: {
        flex: 1,
    },
    itemTitleView: {},
    itemTitle: {
        fontSize: 15,
        color: '#333',
        lineHeight: 22,
    },
    itemInfoCon: {},
    infoIcon: {},
    infoText: {
        fontSize: 13,
        color: '#666',
    },
});