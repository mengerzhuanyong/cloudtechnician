/**
 * 云技师 - OrderItem
 * http://menger.me
 * @大梦
 */


'use strict';

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Text, ViewPropTypes, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {Button} from 'teaset'
import RouterHelper from "../../routers/RouterHelper";

export default class OrderItem extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {};
    }

    static propTypes = {
        ...ViewPropTypes,
        title: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.number]),
        titleStyle: Text.propTypes.style,
        activeTitleStyle: Text.propTypes.style,
        active: PropTypes.bool,
        badge: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.number]),
        onAddWidth: PropTypes.func,
    };

    static defaultProps = {
        ...View.defaultProps,
        active: false,
        item: {},
        onRefresh: () => {},
    };

    postRequest = (url, data) => {
        let {onRefresh} = this.props;
        Services.Post(url, data, true)
            .then(result => {
                if (result.code == 1) {
                    onRefresh && onRefresh();
                }
            })
            .catch(error => {
                console.log(error);
                Toast.show('error');
            })
    };

    pickUpOrderItem = () => {
        let {item} = this.props;
        let url = ServicesApi.changeOrderStatus;
        let data = {
            id: item.id,
            status: 1,
        };
        let params = {
            title: '温馨提示',
            detail: '您是否要接受该订单？',
            actions: [
                {
                    title: '取消',
                    onPress: () => {},
                },
                {
                    title: '确定',
                    onPress: () => this.postRequest(url, data),
                }
            ]
        };
        AlertManager.show(params);
    };

    refuseOrderItem = () => {
        let {item} = this.props;
        let url = ServicesApi.changeOrderStatus;
        let data = {
            id: item.id,
            status: -1,
        };

        let params = {
            title: '温馨提示',
            detail: '您是否要拒绝该订单？',
            actions: [
                {
                    title: '取消',
                    onPress: () => {},
                },
                {
                    title: '确定',
                    onPress: () => this.postRequest(url, data),
                }
            ]
        };
        AlertManager.show(params);
    };

    completeOrderItem = () => {
        let {item} = this.props;
        let url = ServicesApi.changeOrderStatus;
        let data = {
            id: item.id,
            status: 2,
        };

        let params = {
            title: '温馨提示',
            detail: '您是否已完成该订单？',
            actions: [
                {
                    title: '取消',
                    onPress: () => {},
                },
                {
                    title: '确定',
                    onPress: () => this.postRequest(url, data),
                }
            ]
        };
        AlertManager.show(params);
    };

    submitOrderItem = () => {
        let {item, onRefresh} = this.props;
        let pageTitle = item.is_zhibao > 0 ? '查看质保单' : '录入质保单';
        RouterHelper.navigate(pageTitle, 'InputQualityBill', {
            orderInfo: item,
            onCallBack: () => onRefresh(),
        });
    };


    renderOrderBtnContent = () => {
        let {item} = this.props;
        let {id, is_zhibao, server_address, server_time, server_type_name, status} = item;
        if (status === 0) {
            return (
                <View style={styles.orderBtnView}>
                    <TouchableOpacity
                        style={[styles.orderBtnItemView, styles.orderBtnItemViewCur]}
                        onPress={this.pickUpOrderItem}
                    >
                        <Text style={[styles.orderBtnItemName, styles.orderBtnItemNameCur]}>接&nbsp;&nbsp;单</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.orderBtnItemView]}
                        onPress={this.refuseOrderItem}>
                        <Text style={[styles.orderBtnItemName,]}>拒&nbsp;&nbsp;绝</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        if (status === 1) {
            return (
                <View style={styles.orderBtnView}>
                    <View style={[styles.orderBtnItemView, styles.orderBtnItemViewNoBor]}>
                        <Text style={[styles.orderBtnItemName,]}>服务中</Text>
                    </View>
                    <View style={[styles.orderBtnItemView, styles.orderBtnItemViewCur]}>
                        <Text style={[styles.orderBtnItemName, styles.orderBtnItemNameCur]}>完&nbsp;&nbsp;成</Text>
                    </View>
                </View>
            );
        }
        if (status === 2) {
            return (
                <View style={styles.orderBtnView}>
                    <View style={[styles.orderBtnItemView, styles.orderBtnItemViewNoBor, styles.customWidth]}>
                        <Text style={[styles.orderBtnItemName,]}>已&nbsp;&nbsp;完&nbsp;&nbsp;成</Text>
                    </View>
                    <TouchableOpacity
                        style={[styles.orderBtnItemView, is_zhibao < 1 && styles.orderBtnItemViewCur, styles.customWidth]}
                        onPress={this.submitOrderItem}
                    >
                        <Text style={[styles.orderBtnItemName, is_zhibao < 1 && styles.orderBtnItemNameCur]}>{is_zhibao < 1 ? '录入质保单' : '查看质保单'}</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        return null;
    };

    render() {
        let {item, onRefresh} = this.props;
        return (
            <TouchableOpacity
                style={styles.orderItemView}
                onPress={() => RouterHelper.navigate('订单详情', 'OrderDetail', {
                    id: item.id,
                    status: item.status,
                    onCallBack: () => onRefresh(),
                })}
            >
                <View style={styles.orderItemInfoView}>
                    <Text style={styles.orderItemTitle}>{item.server_type_name}</Text>
                    <Text style={styles.orderInfoItem}>订单号：{item.sn}</Text>
                    <Text style={styles.orderInfoItem}>时间：{item.server_time}</Text>
                    <Text style={styles.orderInfoItem}>地点：{item.server_address}</Text>
                </View>

                {this.renderOrderBtnContent()}
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    orderItemView: {
        padding: 15,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'space-between',
    },
    orderItemInfoView: {
        flex: 1,
        minHeight: 70,
        marginRight: 5,
        justifyContent: 'space-around',
    },
    orderItemTitle: {
        padding: 3,
        color: '#617995',
        fontSize: 18,
        fontWeight: 'bold'
    },
    orderInfoItem: {
        fontSize: 14,
        marginTop: 3,
        color: '#333',
        lineHeight: 18,
    },

    orderBtnView: {
        height: 70,
        minWidth: 70,
        alignItems: 'flex-end',
        justifyContent: 'space-around',
    },
    orderBtnItemView: {
        width: 55,
        height: 28,
        borderRadius: 5,
        alignItems: 'center',
        borderColor: '#5c7091',
        justifyContent: 'center',
        borderWidth: GlobalStyles.minPixel,
    },
    orderBtnItemViewCur: {
        backgroundColor: '#5c7091',
    },
    orderBtnItemViewNoBor: {
        borderColor: '#fff',
    },
    customWidth: {
        width: 90,
    },
    orderBtnItemName: {
        color: '#333',
    },
    orderBtnItemNameCur: {
        color: '#fff',
    }
});