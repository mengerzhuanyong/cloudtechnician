/**
 * 云技师 - CardItem
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native'


export default class CardItem extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        item: {
            id: '',
            title: '',
            thumb: '',
            create_time: '',
            is_collection: '',
            views: '',
            article_url: '',
        },
        onPress: () => {},
        onCallBack: () => {},
    };
    
    render() {
        let {item, onPress, onCallBack} = this.props;
        return (
            <TouchableOpacity
                style={styles.cardItemStyle}
                onPress={() => RouterHelper.navigate(item.title, 'NewsWebDetail', {
                    link: item.article_url,
                    content_id: item.article_id || item.id,
                    is_collection: item.is_collection,
                    onCallBack: () => onCallBack(),
                })}
            >
                <View style={[styles.cardContentItem, GlobalStyles.flexRowBetween, styles.itemTitleView]}>
                    <Text style={styles.itemTitle}>{item.title}</Text>
                </View>
                <View style={[styles.cardContentItem, styles.cardThumbView]}>
                    <Image source={item.thumb ? {uri: item.thumb} : Images.img_logo} style={styles.cardThumbImg}/>
                </View>
                <View style={[styles.cardContentItem, GlobalStyles.flexRowBetween, styles.itemInfoView]}>
                    <View style={[styles.itemInfoCon, GlobalStyles.flexRowBetween]}>
                        <Image source={item.is_collection == 0 ? Images.icon_shoucang : Images.icon_shoucang_cur} style={styles.infoIcon}/>
                        <Text style={styles.infoText}>{item.create_time}</Text>
                    </View>
                    <View style={[styles.itemInfoCon, GlobalStyles.flexRowBetween]}>
                        <Text style={styles.infoText}>阅读量：{item.views}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

}

const styles = StyleSheet.create({
    cardItemStyle: {
        padding: 8,
        borderRadius: 8,
        marginVertical: 5,
        borderColor: '#eee',
        marginHorizontal: 15,
        borderWidth: GlobalStyles.minPixel,
    },
    cardContentItem: {},
    itemTitleView: {},
    itemTitle: {
        fontSize: 15,
        color: '#333',
        lineHeight: 22,
    },
    cardThumbView: {
        flex: 1,
        marginVertical: 10,
    },
    cardThumbImg: {
        resizeMode: 'cover',
        width: SCREEN_WIDTH - 46,
        height: (SCREEN_WIDTH - 46) / 2.5,
    },
    itemInfoView: {},
    itemInfoCon: {},
    infoIcon: {
        width: 18,
        height: 18,
        marginRight: 5,
        resizeMode: 'contain',
    },
    infoText: {
        fontSize: 12,
        color: '#666',
    },
});