'use strict';
import React, {Component, PureComponent} from 'react';
import {View, StyleSheet, Image, TouchableOpacity, ViewPropTypes, CameraRoll} from 'react-native';
import {AlbumView, TransformView} from 'teaset';
import PropTypes from 'prop-types'
import ImageView from './ImageView';
import RNFS from 'react-native-fs'
import {toastShort} from "../../utils/utilsToast";

const storeLocation = `${RNFS.DocumentDirectoryPath}`;

class LargePicture extends PureComponent {

    static propTypes = {
        ...Image.propTypes,
        style: ViewPropTypes.style,
        imageStyle: ViewPropTypes.style,
        minScale: PropTypes.number,
        maxScale: PropTypes.number,
    };

    static defaultProps = {
        imagesArray: [],
        activeIndex: 0,
        ...Image.defaultProps,
    };

    _onLongPress = () => {
        const {source} = this.props;
        const save = async () => {
            if (__IOS__) {
                const result = await CameraRoll.saveToCameraRoll(source.uri, 'photo');
                console.log(result);
                if (result) {
                    toastShort('图片已保存至相册');
                } else {
                    toastShort('保存失败');
                }
            } else {
                let fileName = new Date().getTime() + ".png";
                let downloadPath = `${storeLocation}/${fileName}`;
                const ret = RNFS.downloadFile({fromUrl: source.uri, toFile: downloadPath});
                ret.promise
                    .then(res => {
                        if (res && res.statusCode === 200) {
                            let promise = CameraRoll.saveToCameraRoll("file://" + downloadPath);
                            promise.then((result) => {
                                console.log(result);
                                toastShort("图片已保存至相册");
                            }).catch((error) => {
                                console.log(error);
                                toastShort("保存失败");
                            })
                        }
                    })
            }
        };
        ActionsManager.show({
            title: '',
            actions: [
                {title: '保存图片', onPress: save}
            ]
        });
    };

    _onPress = () => {
        AlertManager.hide();
    };

    _onPressShow = () => {
        const {source, minScale, maxScale, imagesArray, activeIndex} = this.props;
        let _imagesArray = imagesArray.map((item, index) => {
            let _newItem = {};
            _newItem = {uri: item};
            return _newItem;
        });
        console.log(_imagesArray);
        const content = (
           // <View style={{flex: 1}}>
           //     <TransformView
           //         style={styles.transformView}
           //     >
           //         <ImageView
           //             style={styles.largeImage}
           //             useMaxImage={true}
           //             source={source}
           //             maxImageWidth={SCREEN_WIDTH}
           //         />
           //     </TransformView>
           // </View>
            <AlbumView
                style={{flex: 1}}
                control={true}
                defaultIndex={activeIndex}
                images={_imagesArray}
                minScale={minScale}
                maxScale={maxScale}
                onPress={this._onPressHide}
                onLongPress={this._onLongPress}
                delayLongPress={300}
            />
        );
        AlertManager.showPopView(content, {
            style: {flex: 1},
            type: 'zoomOut',
            overlayOpacity: 1.0,
            containerStyle: {flex: 1}
        })
    };

    _onPressHide = () => {
        AlertManager.hide();
    };

    render() {
        const {style, imageStyle, ...others} = this.props;
        return (
            <TouchableOpacity
                style={style}
                activeOpacity={0.9}
                onPress={this._onPressShow}
            >
                <ImageView style={[styles.imageStyle, imageStyle]} {...others} />
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    transformView: {},
    imageStyle: {},
    largeImage: {},
});

export default LargePicture