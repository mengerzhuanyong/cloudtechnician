/**
 * 速芽物流 - SendSMS
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native'


export default class SendSMS extends Component {

    render(){
        const { sendSMS } = this.props;

        return (
            <TouchableOpacity
                style = {GlobalStyles.btnGetCodeView}
                onPress = {sendSMS}
            >
                <Text style={GlobalStyles.btnGetCodeItem}>获取验证码</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    
});