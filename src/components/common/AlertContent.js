'use strict'
import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types'
import { fontSize, scaleSize, isEmpty } from '../../utils/utilsTool';
import GlobalStyles from '../../constants/GlobalStyles'


export default class AlertContent extends React.PureComponent {

    static propTypes = {
        title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        titleStyle: Text.propTypes.style,
        detail: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        detailStyle: Text.propTypes.style,
        actions: PropTypes.arrayOf(PropTypes.shape({ title: PropTypes.string, titleStyle: Text.propTypes.style, onPress: PropTypes.func }))
        // 例如,
        //   actions: [
        //         { title: '取消', titleStyle: {}, onPress: () => alert('取消') },
        //         { title: '确定', titleStyle: {}, onPress: () => alert('取消') },
        //     ]
    };

    static defaultProps = {
        actions: []
    };

    constructor(props) {
        super(props)
        this.lastActionTime = 0
        this.interval = 500
    };

    componentWillUnmount() {
        // 卸载
        // clearTimeout(this.times)
    }

    _onPress = (onPressItem) => {
        let nowTime = Moment().format('x')
        if ((nowTime - this.lastActionTime) <= this.interval) {
            // console.warn('间隔时间内重复点击了');
            return;
        }
        requestAnimationFrame(() => {
            this.lastActionTime = nowTime;
            onPressItem && onPressItem()
        })
        AlertManager.hide()
    };

    separator = (index) => {
        const { actions } = this.props
        let separator;
        if (actions.length === 1) {
            separator = null
        } else {
            separator = actions.length - 1 === index ? null : styles.separator
        }
        return separator
    }

    _renderTitle = () => {
        const { title, titleStyle } = this.props
        if (React.isValidElement(title)) {
            return title
        } else if (!isEmpty(title)) {
            return <Text style={[styles.title, titleStyle]}>{title}</Text>
        }
        return null
    };

    _renderDetail = () => {
        const { detail, detailStyle } = this.props
        if (React.isValidElement(detail)) {
            return detail
        } else if (!isEmpty(detail)) {
            return <Text style={[styles.detail, detailStyle]}>{detail}</Text>
        }
        return null
    };

    _rederAction = () => {
        const {actions, actionContainerStyle} = this.props
        if (actions.length === 0) {
            return null
        }
        return (
            <View style={[styles.actionContainer, actionContainerStyle]}>
                {actions.map((item, index) => {
                    const { title, titleStyle, actionStyle, onPress } = item
                    return (
                        <TouchableHighlight
                            underlayColor={'#eeeeee'}
                            style={styles.actionHighlight}
                            key={`action_${index}`}
                            onPress={() => this._onPress(onPress)}>
                            <View style={[styles.action, actionStyle, this.separator(index)]}>
                                <Text style={[styles.actionText, titleStyle]}>
                                    {title}
                                </Text>
                            </View>
                        </TouchableHighlight>
                    )
                })}
            </View>
        )
    }

    render() {
        let {style} = this.props;
        return (
            <View style={[styles.container, style]}>
                {this._renderTitle()}
                {this._renderDetail()}
                {this._rederAction()}
            </View>
        );
    }
}
// define your styles
const styles = StyleSheet.create({
    container: {
        width: GlobalStyles.alertWidth,
        minHeight: GlobalStyles.alertMinHeight,
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        overflow: 'hidden',
    },
    title: {
        marginTop: 20,
        maxWidth: GlobalStyles.alertTitleMaxWidth,
        textAlign: 'center',
        fontSize: GlobalStyles.alertTitleFontSize,
        fontWeight: 'bold',
        color: GlobalStyles.alertTitleColor,
    },
    detail: {
        marginTop: 12,
        maxWidth: GlobalStyles.alertDetailMaxWidth,
        textAlign: 'center',
        fontSize: GlobalStyles.alertDetailFontSize,
        lineHeight: scaleSize(40),
        color: GlobalStyles.alertDetailColor,
    },
    actionContainer: {
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: GlobalStyles.alertWidth,
        height: GlobalStyles.alertActionHeight,
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: GlobalStyles.alertSeparatorColor,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    actionHighlight: {
        flex: 1,
        height: GlobalStyles.alertActionHeight,
    },
    action: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: GlobalStyles.alertActionHeight,
    },
    actionText: {
        color: GlobalStyles.alertActionColor,
        fontSize: GlobalStyles.alertActionFontSize,
    },
    separator: {
        borderRightWidth: StyleSheet.hairlineWidth,
        borderRightColor: GlobalStyles.alertSeparatorColor,
    },
});
