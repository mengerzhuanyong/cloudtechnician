/**
 * 速芽物流 - ActivityIndicatorItem
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    StyleSheet,
    ActivityIndicator
} from 'react-native'


export default class ActivityIndicatorItem extends Component {

    render(){
        const { color } = this.props;
        return (
            <ActivityIndicator size="large" color={color ? color : GlobalStyles.themeColor} style={styles.activityIndicator} />
        );
    }
}

const styles = StyleSheet.create({    
    activityIndicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});