/**
 * 鼎亚汽车 - 我的
 * http://menger.me
 * @大梦
 */

import React, { Component } from 'react';
import {
    Text,
    View,
    Alert,
    Modal,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
} from 'react-native';



import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

const {width,height} = Dimensions.get('window');

export default class ShareUtils extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isVisible: this.props.show,
            id: this.props.id,
            title: this.props.title,
            description: this.props.description,
            thumbImage: this.props.thumbImage,
            webpageUrl: this.props.webpageUrl,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isVisible: nextProps.show
        });
    }

    closeModal() {
        this.setState({
            isVisible: false
        });
        this.props.closeModal(false);
    }

    _shareToTimeline = () => {
        this.props.closeModal(false);
        toastShort('即将上线，敬请期待！');
    }


    _shareToSession = () => {
        this.props.closeModal(false);
        toastShort('即将上线，敬请期待！');
    }

    _shareToWeibo = () => {
        this.props.closeModal(false);
        toastShort('即将上线，敬请期待！');
    }


    renderDialog() {
        return (
            <View style={styles.shareView}>
                <View style={styles.topView}>
                    <View style={styles.shareViewContent}>
                        <TouchableOpacity style={styles.shareItem} onPress={() => {this._shareToSession()}}>
                            <View style={styles.imgWrap}><Image source={Images.icon_share_haoyou} style={styles.shareItemIcon} /></View>
                            <Text style={styles.shareItemTitle}>微信</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.shareItem} onPress={() => {this._shareToTimeline()}}>
                            <View style={styles.imgWrap}><Image source={Images.icon_share_pengyouquan} style={styles.shareItemIcon} /></View>
                            <Text style={styles.shareItemTitle}>朋友圈</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.shareItem} onPress={() => {this._shareToWeibo()}}>
                            <View style={styles.imgWrap}><Image source={Images.icon_share_weibo} style={styles.shareItemIcon} /></View>
                            <Text style={styles.shareItemTitle}>微博</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.bottomView}>
                    <Text style={styles.btnItem}>取消</Text>
                </View>
            </View>
        )
    }

    render() {

        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.isVisible}
                    animationType={'fade'}
                    onRequestClose={() => this.closeModal()}
                >
                    <TouchableOpacity
                        style={styles.shareContainer}
                        activeOpacity={1}
                        onPress={() => this.closeModal()}
                    >
                        {this.renderDialog()}
                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    shareContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    shareView: {
        bottom: 0,
        position: "absolute",
        width:width,
    },
    topView: {
        backgroundColor: '#f8f8f8',
        paddingBottom: 10,
        paddingTop: 10
    },
    titleView: {
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
    },
    shareViewTitle: {
        fontSize: 16,
        color: '#888',
        borderColor: '#aaa',
        borderBottomWidth: 1,
    },
    shareViewContent: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        borderBottomWidth: 1,
        borderBottomColor: '#ececec',
        paddingBottom: 15
    },
    shareItem: {
        backgroundColor: '#f8f8f8',
        flex: 1,
        alignItems:'center',
    },
    shareItemIcon: {
        width: 34,
        height: 34,
        resizeMode: 'contain',
        position: 'absolute',
        top: 13,
        left: 13,

    },
    shareItemTitle: {
        fontSize: 14,
        color: '#666',
        height: 30,
        lineHeight: 30
    },
    bottomView: {
        height: 60,
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    btnItem: {
        fontSize: 18,
        color: '#333',
    },
    collectContent: {
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'flex-start',
        
    },
    collectItem: {
        width: width*0.2,
        alignItems:'center',
    },
    collectItemIcon: {
        width: 34,
        height: 34,
        resizeMode: 'contain',
        position: 'absolute',
        top: 13,
        left: 13,

    },
    collectItemTitle: {
        fontSize: 14,
        color: '#666',
        height: 30,
        lineHeight: 30

    },
    imgWrap: {
        backgroundColor: '#fff',
        width: 60,
        height: 60,
        borderRadius: 30,
        position: 'relative',

    }
});