'use strict';
import React, {Component, PureComponent} from 'react';
import {View, StyleSheet, Image, TouchableOpacity, ViewPropTypes, CameraRoll} from 'react-native';
import {TransformView} from 'teaset';
import PropTypes from 'prop-types'
import ImageView from './ImageView';
import RNFS from "react-native-fs";
import {toastShort} from "../../utils/utilsToast";
import GlobalStyles from "../../constants/GlobalStyles";

const storeLocation = `${RNFS.DocumentDirectoryPath}`;

export default class ShowImageContent extends PureComponent {

    static propTypes = {
        ...Image.propTypes,
        style: ViewPropTypes.style,
        imageStyle: ViewPropTypes.style,
        minScale: PropTypes.number,
        maxScale: PropTypes.number,
    };

    static defaultProps = {
        ...Image.defaultProps,
    };

    _onLongPress = () => {
        const {source} = this.props;
        const save = async () => {
            if (__IOS__) {
                const result = await CameraRoll.saveToCameraRoll(source.uri, 'photo');
                console.log(result);
                if (result) {
                    toastShort('图片已保存至相册');
                } else {
                    toastShort('保存失败');
                }
            } else {
                let fileName = new Date().getTime() + ".png";
                let downloadPath = `${storeLocation}/${fileName}`;
                const ret = RNFS.downloadFile({fromUrl: source.uri, toFile: downloadPath});
                ret.promise
                    .then(res => {
                        if (res && res.statusCode === 200) {
                            let promise = CameraRoll.saveToCameraRoll("file://" + downloadPath);
                            promise.then((result) => {
                                console.log(result);
                                toastShort("图片已保存至相册");
                            }).catch((error) => {
                                console.log(error);
                                toastShort("保存失败");
                            })
                        }
                    })
            }
        };
        ActionsManager.show({
            title: '',
            actions: [
                {title: '保存图片', onPress: save}
            ]
        });
    };

    _onPress = () => {
        AlertManager.hide();
    };

    _onPressHide = () => {
        AlertManager.hide();
    };

    render() {
        const {source, minScale, maxScale, ...others} = this.props;
        return (
            <View style={{flex: 1}}>
                <TransformView
                    style={styles.transformView}
                    minScale={minScale}
                    maxScale={maxScale}
                    onPress={this._onPressHide}
                    onLongPress={this._onLongPress}
                    delayLongPress={300}
                >
                    <ImageView
                        style={styles.largeImage}
                        useMaxImage={true}
                        source={source ? source : Images.img_banner}
                        maxImageWidth={GlobalStyles.width}
                    />
                </TransformView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    transformView: {},
    imageStyle: {},
    largeImage: {},
});