/**
 * 速芽物流 - BANNER
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    StyleSheet,
    TouchableOpacity
} from 'react-native'

import Swiper from 'react-native-swiper'
import { toastShort, consoleLog } from '../../utils/utilsToast'





export default class Banner extends Component {

    constructor(props){
        super(props);
        this.state = {
            swiperShow: false,
            banners: [],
        };
    }

    componentDidMount() {
        this.loadNetData();
        setTimeout(() => {
            this.setState({
                swiperShow: true
            });
        }, 0)
    }

    componentWillReceiveProps(nextProps){
        // consoleLog('首页轮播', nextProps);
        if (nextProps.bannerData) {
            this.setState({
                banners: nextProps.bannerData
            })
        }
    }

    updateState = (state) => {
        if (!this) { return };
        this.setState(state);
    }

    loadNetData = () => {
        
    }

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }

    toWebview = (link, compent) => {
        const { navigate } = this.props.navigation;
        navigate(compent, {
            link:link,
        })
    }

    renderBanner = (row) => {
        if(this.state.swiperShow) {
            if(row.length <= 0) {
                return null;
            }
            let banners = row.map((obj,index)=>{
                let url = this.state.banners[index].images;
                if(url == ''){
                    return (
                        <View
                            style={GlobalStyles.bannerViewWrap}
                            key={"bubble_"+index}
                            activeOpacity = {1}
                        >
                            <View style={GlobalStyles.bannerViewWrap}>
                                <Image source={{uri:this.state.banners[index].images}} style={GlobalStyles.bannerImg} />
                            </View>
                        </View>
                    )
                }else{
                    return (
                        <TouchableOpacity
                            style={GlobalStyles.bannerViewWrap}
                            key={"bubble_"+index}
                            activeOpacity = {1}
                            onPress={()=>{this.toWebview(url, 'NewsWebDetail')}}
                        >
                            <View style={GlobalStyles.bannerViewWrap}>
                                <Image source={{uri:this.state.banners[index].images}} style={GlobalStyles.bannerImg} />
                            </View>
                        </TouchableOpacity>
                    )
                }
                    
            });
            return (
                <Swiper
                    index={0}
                    loop={true}
                    autoplay={true}
                    horizontal={true}
                    removeClippedSubviews={false}
                    showsPagination={true}
                    autoplayTimeout={5}
                    height={210}
                    width={GlobalStyles.width}
                    style={{paddingTop:0,marginTop:0}}
                    lazy={true}
                    dot = {<View style={GlobalStyles.bannerDot} />}
                    activeDot = {<View style={GlobalStyles.bannerActiveDot} />}
                >
                    {banners}
                </Swiper>
            )
        }
    }

    render(){
        const { banners } = this.state;
        return (
            <View style={GlobalStyles.bannerContainer}>
                {this.renderBanner(banners)}
                <View style={GlobalStyles.bannerTop}>
                    <TouchableOpacity onPress={() => {this.onPushNavigator('Xiaoxi')}}>
                        {/*<Image source={Images.icon_news} style={GlobalStyles.iconsNews} />*/}
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {this.onPushNavigator('Kefu')}} style={GlobalStyles.hide}>
                        <Image source={Images.icon_kefu} style={GlobalStyles.iconsKefu} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});