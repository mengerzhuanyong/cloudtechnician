/**
 * 云技师 - BannerComponent
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    Platform,
    TextInput,
    ScrollView,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native'
import {Carousel} from "teaset";
// import SpinnerLoading from "../loading/SpinnerLoading";

export default class BannerComponent extends Component {

    constructor(props){
        super(props);
        this.state = {
            swiperShow: false,
            bannerData: this.props.bannerData,
        };
    }

    static defaultProps = {
        bannerData: [],
        resizeMode: 'cover',
    };

    componentDidMount() {
        this.timer =  setTimeout(() => {
            this.setState({
                swiperShow: true
            });
        }, 0)
    }

    componentWillReceiveProps(nextProps){
        console.log('nextProps---->', nextProps);
        if (this.state.bannerData !== nextProps.bannerData) {
            this.setState({
                bannerData: nextProps.bannerData
            });
        }
    }

    componentWillUnmount(){
        this.timer && clearTimeout(this.timer);
    }

    onPushToNextPage = (pageTitle, component, params = {}) => {
        RouterHelper.navigate(pageTitle, component, params);
    };

    renderBanner = (row) => {
        let {style, resizeMode} = this.props;
        if (this.state.swiperShow) {
            if (row.length <= 0) {
                return null;
            }
            let banners = row.map((item, index) => {
                return (
                    <TouchableOpacity
                        style={styles.bannerViewWrap}
                        key={"banner_" + index}
                        activeOpacity={1}
                        onPress={() => {
                            item.link && this.onPushToNextPage(item.title, 'WebViewPage', {url: item.link});
                        }}
                    >
                        <ImageBackground
                            resizeMode={resizeMode}
                            style={[styles.headBackImage, style]}
                            source={item.images ? {uri: item.images} : Images.images_index_banner}
                        />
                    </TouchableOpacity>
                )
            });
            // 这里不能输出信息，否则会陷入死循环
            return banners;
        }
    };

    render() {
        const {bannerData} = this.state;
        let {style, showControl, ...other} = this.props;
        return (
            <View style={[styles.container, style]}>
                <Carousel
                    style={[styles.headBackCarousel, style]}
                    control={showControl ?
                        <Carousel.Control
                            style={styles.carouselControlView}
                            dot={<View style={styles.carouselControl}/>}
                            activeDot={<View style={[styles.carouselControl, styles.carouselControlCur]}/>}
                        />
                        : false
                    }
                    {...other}
                >{this.renderBanner(bannerData)}</Carousel>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: __IOS__ ? 0 : -20,
        // backgroundColor: '#f1f2f3',
        height: SCREEN_WIDTH * 0.45,
    },
    bannerViewWrap: {},

    carouselControlView: {
        marginBottom: 10,
        alignItems: 'flex-end',
    },
    carouselControl: {
        marginRight: 10,
        width: ScaleSize(25),
        height: ScaleSize(10),
        borderRadius: ScaleSize(8),
        backgroundColor: "rgba(255, 255, 255, 0.5)",
    },
    carouselControlCur: {
        backgroundColor: '#fff',
    },
    headBackCarousel: {
        overflow: 'hidden',
        width: SCREEN_WIDTH,
        height: SCREEN_WIDTH * 0.45,
    },
    headBackImage: {
        width: SCREEN_WIDTH,
        height: SCREEN_WIDTH * 0.45,
    },
});