
//用户登录数据
global.user = {
    loginState: '', //登录状态
    userData: [], //用户数据
};

//用户投资者数据
global.invest = {
    investType: '', //投资者类型
    investInfo: [], //投资数据
};


//刷新的时候重新获得用户数据
storage.load({
    key: 'loginState',
}).then(ret => {
    global.user.loginState = true;
    global.user.userData = ret;
}).catch(err => {
    global.user.loginState = false;
    global.user.userData = '';
})

storage.load({
    key: 'investInfo',
}).then(ret => {
    global.invest.investInfo = ret;
}).catch(err => {
    global.invest.investInfo = '';
})