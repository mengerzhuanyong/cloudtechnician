/**
 * 云技师 - ActionsManager
 * http://menger.me
 * @大梦
 */

'use strict';

import React from 'react'
import {View, Text} from 'react-native';
import {ActionSheet, Overlay, Label} from 'teaset';
import {bouncer} from "../utils/utilsTool";

class ActionsManager {

    static pullViewRefs = [];

    /** 参数
     *
     **/
    // let params = {
    //     actions: [
    //         {title: 'Say hello', onPress: () => alert('Hello')},
    //         {title: 'Do nothing'},
    //         {title: 'Disabled', disabled: true},
    //     ],
    //     cancelAction: {
    //         title: 'Cancel'
    //     }
    // };

    // 先使用teaset自带的组件，后续自定义组件
    static show = (params) => {
        const actions = params.actions;
        const cancelAction = params.cancelAction ? params.cancelAction : {title: '取消'};
        ActionSheet.show(actions, cancelAction);
    };

    static hide() {
        this.pullViewRefs = bouncer(this.pullViewRefs.slice()); // 过滤
        if (this.pullViewRefs.length > 0) {
            const lastRef = this.pullViewRefs.pop();
            lastRef.close();
        }
    }
}


export default ActionsManager;