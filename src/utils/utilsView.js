/**
 * 速芽物流 - ViewUtils
 * http://menger.me
 * @大梦
 */

'use strict'

import React  from 'react';
import {
    TouchableHighlight,
    Image,
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import GlobalStyles from '../constants/GlobalStyles'

export default class UtilsView {
    
    
    /**
     * 获取设置页的Item
     * @Author   Menger
     * @DateTime 2018-01-17
     * @param callBack 单击item的回调
     * @param icon 左侧图标
     * @param text 显示的文本
     * @param tintStyle 图标着色
     * @param expandableIco 右侧图标
     * @return {XML}
     */
    static getSettingItem(callBack, icon, text, tintStyle, expandableIco) {
        return (
            <TouchableHighlight
                onPress = {callBack}
            >
                <View style={[styles.setting_item_container]}>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        {icon ?
                            <Image source={icon} style={[{opacity: 1, width: 16, height: 16, marginRight: 10,resizeMode:'stretch'}, tintStyle]} />
                            :
                            <View style={{opacity: 1, width: 16, height: 16, marginRight: 10,}}/>
                        }
                        <Text>{text}</Text>
                    </View>
                    <Image
                        source = {expandableIco ? expandableIco : Images.icon_angle_left_white}
                        style = {[styles.settingBtnIcon, tintStyle]}
                    />
                </View>
            </TouchableHighlight>
        )
    }
    
    /**
     * 左侧导航按钮
     * @Author   Menger
     * @DateTime 2018-01-17
     * @param    callBack
     * @return   {XML}
     */
    static getLeftButton = (callBack) => {
        return (
            <TouchableOpacity
                style = {{padding: 8}}
                onPress = {callBack}
            >
                <Image style={{width: 20, height: 20, tintColor: '#333'}} source={Images.icon_angle_left_white}/>
            </TouchableOpacity>
        )
    }
    /**
     * 获取左侧设置按钮
     * @param callBack
     * @returns {*}
     */
    static getLeftSetButton = (callBack) => {
        return (
            <TouchableOpacity
                style = {{padding: 8}}
                onPress = {callBack}
            >
                <Image style={{width: 20, height: 20, marginLeft:15}} source={Images.person_setting}/>
            </TouchableOpacity>
        )
    }

    /**
     * 右侧消息按钮 - 白底黑色
     * @Author   aiqiu
     * @DateTime 2018-01-17
     * @param    more
     * @return   {XML}
     */
    static getRightXiaoxiButton = (more) => {
        return (
            <TouchableOpacity
                style = {{padding: 8, paddingRight: 13}}
                onPress = {more}
            >
                <Image style={{width: 20, height: 20,}} source={Images.person_xiaoxi}/>
            </TouchableOpacity>
        )
    }
    
    /**
     * 左侧导航按钮 - 白底黑色
     * @Author   aiqiu
     * @DateTime 2018-01-17
     * @param    callBack
     * @return   {XML}
     */
    static getLeftBlackButton = (callBack) => {
        return (
            <TouchableOpacity
                style = {{padding: 8}}
                onPress = {callBack}
            >
                <Image style={{width: 20, height: 20,}} source={Images.icon_angle_left_black}/>
            </TouchableOpacity>
        )
    }


    
    /**
     * 左侧导航按钮 - 白底黑色----添加关闭回到首页功能
     * @Author   aiqiu
     * @DateTime 2018-01-17
     * @param    callBack
     * @return   {XML}
     */
    static getLeftBlackCloseButton = (callBack, goHome) => {
        return (
            <View style={[GlobalStyles.flexRowStart, {padding: 8}]}>
                <TouchableOpacity
                    style = {{paddingRight: 12}}
                    onPress = {callBack}
                >
                    <Image style={{width: 20, height: 20,}} source={Images.icon_angle_left_black}/>
                </TouchableOpacity>
                <TouchableOpacity
                    style = {{}}
                    onPress = {goHome}
                >
                    <Image style={{width: 20, height: 20,}} source={Images.icon_angle_left_black_close}/>
                </TouchableOpacity>
            </View>
        )
    }
    
    /**
     * 左侧导航按钮
     * @Author   Menger
     * @DateTime 2018-01-17
     * @param    callBack
     * @return   {XML}
     */
    static getLeftXiaoxiBlackButton = (callBack) => {
        return (
            <TouchableOpacity
                style = {{padding: 8, paddingLeft: 13}}
                onPress = {callBack}
            >
                <Image source={Images.icon_news_black} style={[GlobalStyles.iconsNews, {width: 22, height: 22}]}/>
            </TouchableOpacity>
        )
    }

    /**
     * 左侧会员中心按钮
     * @Author   Menger
     * @DateTime 2018-01-17
     * @param    callBack
     * @return   {XML}
     */
    static getLeftUserButton = (callBack) => {
        return (
            <TouchableOpacity
                style = {{padding: 8}}
                onPress = {callBack}
            >
                <Image style={{width: 24, height: 24,}} source={Images.icon_header_user}/>
            </TouchableOpacity>
        )
    }
    
    /**
     * 右侧侧导航按钮
     * @Author   Menger
     * @DateTime 2018-01-17
     * @param    callBack
     * @return   {XML}
     */
    static getRightButton = (title, callBack) => {
        return (
            <TouchableOpacity
                style = {{alignItems: 'center',}}
                onPress = {callBack}
            >
                <View style={{marginRight: 10}}>
                    <Text style={{fontSize: 20, color: '#FFFFFF',}}>{title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    
    /**
     * 右侧侧导航按钮 - 白底黑色
     * @Author   aiqiu
     * @DateTime 2018-01-17
     * @param    more
     * @return   {XML}
     */
    static getRightBlackButton = (more) => {
        return (
            <TouchableOpacity
                style = {{padding: 8, paddingRight: 13}}
                onPress = {more}
            >
                <Image style={{width: 25.3, height: 4,}} source={Images.icon_more}/>
            </TouchableOpacity>
        )
    }
    
    /**
     * 右侧侧导航按钮 - 客服
     * @Author   aiqiu
     * @DateTime 2018-01-17
     * @param    more
     * @return   {XML}
     */
    static getRightKefuButton = (more) => {
        // return (
        //     <TouchableOpacity
        //         style = {{padding: 8, paddingRight: 13}}
        //         onPress = {more}
        //     >
        //         <Image source={Images.icon_kefu} style={GlobalStyles.iconsKefu} />
        //     </TouchableOpacity>
        // )
    }
    
    /**
     * 右侧侧导航按钮 - 客服黑色
     * @Author   aiqiu
     * @DateTime 2018-01-17
     * @param    more
     * @return   {XML}
     */
    static getRightKefuBlackButton = (more) => {
        // return (
            {/*<TouchableOpacity*/}
                {/*style = {{padding: 8, paddingRight: 13}}*/}
                {/*onPress = {more}*/}
            {/*>*/}
                {/*<Image source={Images.icon_kefu_black} style={[GlobalStyles.iconsKefu, {width: 22, height: 22}]} />*/}
            {/*</TouchableOpacity>*/}
        // )
    }
    
    /**
     * 右侧侧导航按钮 - 首页黑色
     * @Author   aiqiu
     * @DateTime 2018-01-17
     * @param    more
     * @return   {XML}
     */
    static getRightHomeBlackButton = (more) => {
        return (
            <TouchableOpacity
                style = {{padding: 8, paddingRight: 13}}
                onPress = {more}
            >
                <Text style={{fontSize: 15}}>首页</Text>
            </TouchableOpacity>
        )
    }

    /**
     * 获取更多按钮
     * @Author   Menger
     * @DateTime 2018-01-17
     * @param callBack
     * @returns {XML}
     */
    static getMoreButton = (callBack) => {
        return (
            <TouchableHighlight
                ref = "moreMenuButton"
                style = {{padding: 5}}
                onPress = {callBack}
                underlayColor = {'transparent'}
            >
                <View style={{paddingRight: 8}}>
                <Image style={styles.moreMenuBtnIcon} source={Images.icon_angle_left_white} />
                </View>
            </TouchableHighlight>
        )
    }

    /**
     * 获取分享按钮
     * @Author   Menger
     * @DateTime 2018-01-17
     * @param callBack
     * @returns {XML}
     */
    static getShareButton = (callBack) => {
        return (
            <TouchableHighlight
                onPress = {callBack}
                underlayColor = {'transparent'}
            >
                <Image style={styles.shareBtnIcon} source={Images.icon_angle_left_white}/>
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
    setting_item_container: {
        height: 60,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        justifyContent: 'space-between',
    },
    settingBtnIcon: {
        marginRight: 10,
        height: 22,
        width: 22,
        alignSelf: 'center',
        opacity: 1
    },
    moreMenuBtnIcon: {
        width: 24,
        height: 24,
    },
    shareBtnIcon: {
        width: 20,
        height: 20,
        opacity: 0.9,
        marginRight: 10,
        tintColor: 'white'
    },
})