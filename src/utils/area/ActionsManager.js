
import React from 'react';
import { View, Text } from 'react-native';
import { ActionSheet, Overlay, Label } from 'teaset';
import AreaContent from './AreaContent';


/**
 * 过滤掉false, null, 0, "", undefined, and NaN
 * @param {*} arr 
 */
const bouncer = (arr) => {
    return arr.filter((val) => {
        return !(!val || val === "");
    })
}

class ActionsManager {

    static pullViewRefs = []


    static showArea(func) {
        this.pullViewRefs = bouncer(this.pullViewRefs.slice()) // 过滤
        if (this.pullViewRefs.length === 0) {
            Overlay.show(
                <Overlay.PullView
                    ref={v => this.pullViewRefs.push(v)}
                    side={'bottom'}
                    modal={false}
                    rootTransform={'none'}
                    containerStyle={{}}
                    onCloseRequest={() => this.hide()}
                >
                    <AreaContent onPress={func} />
                </Overlay.PullView>
            )
        }
    }

    static hide() {
        this.pullViewRefs = bouncer(this.pullViewRefs.slice()) // 过滤
        if (this.pullViewRefs.length > 0) {
            const lastRef = this.pullViewRefs.pop()
            lastRef.close()
        }
    }
}


export default ActionsManager