/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'

import {SegmentedView, Label} from 'teaset';




import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

import Intocyh from './intocyh'
import Cyhnews from './cyhnews'
import Cyhcard from './cyhcard'

import ActionSheet from 'react-native-actionsheet'

// 弹窗
const CANCEL_INDEX = 0
const DESTRUCTIVE_INDEX = 0
const options = [ '取消', '一类','二类','三类']
const title = '产品精选'

export default class ProductInfo extends Component {

    constructor(props) {
        super(props);
        this.state =  {
            user: global.user.userData,
            loginState: global.user.loginState,

        }
    }

    componentDidMount(){
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    loadNetData = () => {
        
    }

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }

    render(){
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = {'产品内容精选'}
                    rightButton = {UtilsView.getRightKefuBlackButton(() => this.onPushNavigator('Kefu'))}
                />
                {/*<SegmentedView style={{flex: 1, }} type='carousel' barStyle={{height: 45, borderBottomColor: '#f2f2f2', borderBottomWidth: 1, }} indicatorPositionPadding={0}>*/}
                      {/*<SegmentedView.Sheet title='走进创元汇' titleStyle={{color: '#666', fontSize: 15, fontWeight: 'bold', }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1, }}>*/}
                          {/*<Intocyh {...this.props}/>*/}
                      {/*</SegmentedView.Sheet>*/}
                      {/*<SegmentedView.Sheet title='创元汇新闻' titleStyle={{color: '#666', fontSize: 15, fontWeight: 'bold', }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1, }}>*/}
                        {/*<Cyhnews {...this.props}/>*/}
                      {/*</SegmentedView.Sheet>*/}
                      {/*<SegmentedView.Sheet title='创元汇公告' titleStyle={{color: '#666', fontSize: 15, fontWeight: 'bold', }} activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold', }} style={{backgroundColor: '#fff', flex: 1, }}>*/}
                        {/*<Cyhcard {...this.props}/>*/}
                      {/*</SegmentedView.Sheet>*/}
                {/*</SegmentedView>*/}
                <TouchableOpacity onPress = {() => {this.showActionSheet()}} style={[GlobalStyles.userlist,styles.chooseitem]}>

                    <Text style={GlobalStyles.userlisttext}> 分类管理</Text>
                    {/*<View style={GlobalStyles.userlistleft}>*/}
                    {/*<Image source={Images.icon_user_zichan} style={GlobalStyles.usericon} />*/}
                    {/*</View>*/}
                    <Text>  |  </Text>
                    <View style={GlobalStyles.userlistright}>
                        <Text style={GlobalStyles.userlisttext}>请选择</Text>
                        <Text style={styles.userlistRightText}>{this.state.time}</Text>
                        <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                    </View>
                </TouchableOpacity>
                <Cyhnews {...this.props}/>

                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={title}
                    options={options}
                    cancelButtonIndex={CANCEL_INDEX}
                    destructiveButtonIndex={DESTRUCTIVE_INDEX}
                    onPress={this.handlePress}
                />
            </View>
        );
    }
    showActionSheet = () => {
        this.ActionSheet.show()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    chooseitem:{
    backgroundColor:"#ffffff"
}
});
