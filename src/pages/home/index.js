/**
 * 云技师 - Home
 * http://menger.me
 * @大梦
 */


'use strict';
import React, {Component} from 'react'
import {Image, Linking, RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View,} from 'react-native'

import {ListRow} from "teaset";
import NavigationBar from "../../components/common/NavigationBar";
import BannerComponent from "../../components/carousel/BannerComponent";
import NoticeComponent from "../../components/carousel/NoticeComponent";
import CardItem from "../../components/item/CardItem";
import ArticleItem from "../../components/item/ArticleItem";

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bannerData: [],
            activityData: [],
            noticeData: [],
            trainData: [],
            productsData: [],
            order_num: {
                new_number: '',
                all_number: '',
                working_number: '',
            },
            companyData: [],
            isRefreshing: false,
            ready: false,
            version_code: '1.0',
        };
    }

    componentDidMount() {
        this.loadNetData();

        this.timerReady = setTimeout(() => {
            this.setState({
                ready: true
            });
        }, 1000);
    };

    componentWillUnmount() {
        let timers = [this.timer, this.timerReady];
        ClearTimer(timers);
    };

    loadNetData = () => {
        // 查询版本
        this.getVersion();
        // 获取轮播图数据
        this.loadBannerData();
        // 获取首页数据
        this.getIndexDataSource();
        // 获取活动状态
        this.activityStatus();
    };

    onRefresh = () => {
        this.setState({
            isRefreshing: true,
        });
        this.loadNetData();
        this.timer = setTimeout(() => {
            this.setState({
                isRefreshing: false,
            });
        }, 1000);
    };

    activityStatus = async () => {
        let url = ServicesApi.systemSetting;
        let result = await Services.Get(url, true);
        if (result && result.code === 1) {
            global.showAd = result.data.show_ad;
        }
    };

    // 获取轮播图数据
    loadBannerData = async () => {
        let url = ServicesApi.getBannerList;
        let result = await Services.Get(url, true);
        if (result) {
            if (result.code == 1) {
                this.setState({
                    bannerData: result.data.ad_list,
                    activityData: result.data.activity,
                });
            }
        }
    };

    // 获取首页数据
    getIndexDataSource = async () => {
        let url = ServicesApi.getIndexDataSource;
        let result = await Services.Get(url, true);
        if (result) {
            if (result.code == 1) {
                this.setState({
                    order_num: result.data.order_num,
                    noticeData: result.data.note_list,
                    trainData: result.data.train_list,
                    productsData: result.data.pro_list,
                    companyData: result.data.about_us_list,
                });
            }
        }
    };

    getVersion = async () => {
        let url = ServicesApi.getVersion + '/version_code/' + Constant.VERSION_CODE;
        let result = await Services.Get(url, true);
        if (result && result.code === 1 && result.data.new_version === 1) {
            this.showVersionPopupView(result.data.version);
        }
    };

    showVersionPopupView = (data) => {
        let {title, version_code, content, link_ios, link_android} = data;
        // console.log(title, content, link_ios, link_android);
        let link = __IOS__ ? link_ios : link_android;
        let detail = content.map((item, index) => {
            return (
                <Text key={item.id} style={styles.alertItemStyle}>{item.value}</Text>
            );
        });
        let detailContent = <View style={styles.alertContent}>
            <View style={styles.versionContent}>
                <Text style={styles.versionStyle}>{version_code}</Text>
            </View>
            <Text style={styles.detailContentTitle}>{title}</Text>
            {detail}
        </View>;
        const params = {
            title: '发现新版本',
            style: {width: GlobalStyles.width - 30,},
            detail: detailContent,
            actions: [
                {
                    title: '暂不升级',
                    onPress: () => {
                    },
                },
                {
                    title: '立即升级',
                    actionStyle: {backgroundColor: GlobalStyles.alertActionColor},
                    titleStyle: {color: '#fff'},
                    onPress: () => this.submitUpdate(link),
                }
            ],
            actionContainerStyle: {width: GlobalStyles.width - 30},
        };
        AlertManager.show(params);
    };

    submitUpdate = (url) => {
        Linking.canOpenURL(url)
            .then(supported => {
                if (!supported) {
                    Toast.toastShort('无法打开该链接');
                    return;
                } else {
                    return Linking.openURL(url).catch(e => Toast.toastShort('打开链接出错，请稍后重试'));
                }
            })
            .catch(err => {
                Toast.toastShort('打开链接出错，请稍后重试');
            });
    };

    renderListItem = (data, type) => {
        if (!data || data.length < 1) {
            return null;
        }
        let listItems = data.map((item, index) => {
            if (type === 'card') {
                return (
                    <CardItem
                        key={'listItem_' + index}
                        item={item}
                        onCallBack={() => this.loadNetData()}
                        onPress={() => RouterHelper.navigate(item.title, 'NewsWebDetail', {link: item.article_url})}
                        {...this.props}
                    />
                );
            }
            return (
                <ArticleItem
                    key={'listItem_' + index}
                    item={item}
                    separator={index > 0}
                    onCallBack={() => this.loadNetData()}
                    onPress={() => RouterHelper.navigate(item.title, 'NewsWebDetail', {link: item.article_url})}
                    {...this.props}
                />
            );
        });
        return <View style={styles.contentItemCon}>{listItems}</View>;
    };


    render() {
        let {
            isRefreshing, bannerData, noticeData, trainData,
            productsData, order_num,
            companyData, ready, activityData
        } = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    leftButton={null}
                    style={{backgroundColor: 'transparent'}}
                />
                <ScrollView
                    style={styles.content}
                    refreshControl={
                        <RefreshControl
                            title='Loading...'
                            refreshing={isRefreshing}
                            onRefresh={this.onRefresh}
                            tintColor={GlobalStyles.themeColor}
                            colors={[GlobalStyles.themeColor]}
                            progressBackgroundColor="#fff"
                        />
                    }
                >
                    <View style={[styles.contentItem, {minHeight: SCREEN_WIDTH * 0.5,}]}>
                        {bannerData.length > 0 && <BannerComponent
                            bannerData={bannerData}
                            {...this.props}
                        />}
                        {noticeData.length > 0 && <NoticeComponent
                            noticeData={noticeData}
                            {...this.props}
                        />}
                    </View>
                    <View style={styles.contentItem}>
                        <ListRow
                            icon={<Image source={Images.icon_index_jijinjingxuan} style={styles.titleIconStyle}/>}
                            title={'工作台'}
                            detail={'更多'}
                            iconStyle={styles.titleIconStyle}
                            titleStyle={styles.contentItemTitle}
                            bottomSeparator={'full'}
                            onPress={() => RouterHelper.navigate('工作台', 'OrderManage', {page_flag: 'second'})}
                        />
                        <View style={[styles.contentItemCon, styles.workSpaceContent]}>
                            <TouchableOpacity
                                style={styles.workSpaceItem}
                                onPress={() => RouterHelper.navigate('新派遣', 'OrderManage', {
                                    activeIndex: 0,
                                    page_flag: 'second'
                                })}
                            >
                                <Text style={styles.workSpaceItemText}>新派遣</Text>
                                <Text style={styles.workSpaceItemText}>{order_num.new_number}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.workSpaceItem}
                                onPress={() => RouterHelper.navigate('进行中', 'OrderManage', {
                                    activeIndex: 1,
                                    page_flag: 'second'
                                })}
                            >
                                <Text style={styles.workSpaceItemText}>进行中</Text>
                                <Text style={styles.workSpaceItemText}>{order_num.working_number}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.workSpaceItem}
                                onPress={() => RouterHelper.navigate('总订单', 'OrderManage', {
                                    activeIndex: 2,
                                    page_flag: 'second'
                                })}
                            >
                                <Text style={styles.workSpaceItemText}>已完成</Text>
                                <Text style={styles.workSpaceItemText}>{order_num.all_number}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    {global.showAd > 0 && activityData.length > 0 && <BannerComponent
                        resizeMode={'contain'}
                        bannerData={activityData}
                        style={styles.activityBanner}
                    />}
                    <View style={styles.contentItem}>
                        <ListRow
                            icon={<Image source={Images.icon_peixun} style={styles.titleIconStyle}/>}
                            title={'培训内容精选'}
                            detail={'更多'}
                            iconStyle={styles.titleIconStyle}
                            titleStyle={styles.contentItemTitle}
                            bottomSeparator={'full'}
                            onPress={() => RouterHelper.navigate('培训内容精选', 'ArticleList', {cate_id: 1})}
                        />
                        {this.renderListItem(trainData, 'row')}
                    </View>
                    <View style={styles.contentItem}>
                        <ListRow
                            icon={<Image source={Images.icon_product} style={styles.titleIconStyle}/>}
                            title={'产品资料精选'}
                            detail={'更多'}
                            iconStyle={styles.titleIconStyle}
                            titleStyle={styles.contentItemTitle}
                            bottomSeparator={'full'}
                            onPress={() => RouterHelper.navigate('产品资料精选', 'ArticleList', {cate_id: 2})}
                        />
                        {this.renderListItem(productsData, 'row')}
                    </View>
                    <View style={styles.contentItem}>
                        <ListRow
                            icon={<Image source={Images.icon_goin} style={styles.titleIconStyle}/>}
                            title={'走进我们'}
                            detail={'更多'}
                            iconStyle={styles.titleIconStyle}
                            titleStyle={styles.contentItemTitle}
                            bottomSeparator={'full'}
                            onPress={() => RouterHelper.navigate('走进我们', 'ArticleList', {cate_id: 3})}
                        />
                        {this.renderListItem(companyData, 'row')}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    content: {
        flex: 1,
        marginTop: GlobalStyles.overNavigationBar + 20,
    },
    contentCon: {},
    contentConFlex: {
        flex: 1,
    },
    contentItem: {
        marginBottom: 10,
    },
    titleIconStyle: {
        // width: 30,
        height: 25,
        resizeMode: 'contain',
        // backgroundColor: '#123',
    },
    contentItemTitle: {
        color: '#333',
    },
    contentItemCon: {
        paddingVertical: 15,
        backgroundColor: '#fff',
    },

    workSpaceContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    workSpaceItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    workSpaceItemText: {
        fontSize: 14,
        color: '#444',
        lineHeight: 24,
    },

    alertContent: {
        paddingTop: 20,
        paddingBottom: 10,
        justifyContent: 'flex-start',
        width: GlobalStyles.width - 80,
        maxWidth: GlobalStyles.width - 80,
    },
    versionContent: {
        marginBottom: 15,
        paddingBottom: 15,
        borderColor: '#333',
        alignItems: 'center',
        borderBottomWidth: GlobalStyles.minPixel,
    },
    versionStyle: {
        color: '#fff',
        borderRadius: 14,
        paddingVertical: 5,
        overflow: 'hidden',
        paddingHorizontal: 15,
        backgroundColor: GlobalStyles.alertActionColor,
    },
    detailContentTitle: {
        fontSize: 14,
        color: '#333',
        lineHeight: 20,
        fontWeight: '600',
    },
    alertItemStyle: {
        color: '#333',
        lineHeight: 20,
    },

    activityBanner: {
        height: GlobalStyles.width / 3,
        marginBottom: 10,
    },
});