/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    Modal,
} from 'react-native'


import ScrollableTabView, {DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view';





import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'
import Video from 'react-native-video';
import BannerView from '../../components/common/Banner'
import OrderManage from "../invest/OrderManage";

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            homeData: [],
            about_us: [],
            activityBanner: [],
            fund: [],
            notice: [],
            article: [],
            banner: [],
            loginState: false,
            videoSource: '',
            isPlay: false,
            modalVisible: false,
            token: '',
            new_number:'',
            working_number:'',
            all_number:''
        }
    }

    async componentWillMount() {
        try {
            let result = await storage.load({
                key: 'loginState',
            });
            // console.log(result);
            // this.getInvestData(result.id, result.token);
            // this.loadNetData(result.id);
            this.setState({
                loginState: true,
                token: result.token,
            })
            //获取轮播图数据
            await this.loadBannerData(result.token);
            //获取培训内容
            await this.loadTrainData(result.token);
            //获取产品内容
            await this.loadProductArticleData(result.token);
            //获取工作台数据
            await this.loadWorkPlatformData(result.token);
            //获取走近我们数据
            await this.loadWalkinUs(result.token)


        } catch (error) {
            // console.log(error);
            this.setState({
                loginState: false
            })
        }

    }

    async componentDidMount() {
        // this.loadNetData();
        //获取轮播图数据
        // await  this.loadBannerData(this.state.token);
        // await this.loadProductArticleData(this.state.token);
        // await this.loadTrainData(result.token)
        setTimeout(() => {
            this.setState({
                ready: true,
            })
        }, 0);


    }

    //获取bannner图数据
    loadBannerData = (token) => {
        let url = ServicesApi.BannerList;
        let data = {
            token: token,
        };
        Services.Get(url, data)
            .then(result => {

                this.setState({
                    banner: result.data.ad_list,
                });
            })
            .catch(error => {
                // consoleLog('链接服务器出错，请稍后重试', error);
            })
    }
    //获取产品内容数据
    loadProductArticleData = (token) => {
        let url = ServicesApi.ArticProductlList;
        Services.Get(url)
            .then(result => {

                this.setState({
                    article: result.data.article_list,
                });
            })
            .catch(error => {
                // consoleLog('链接服务器出错，请稍后重试', error);
            })

    }

    //获取培训内容数据
    loadTrainData = (token) => {
        let url = ServicesApi.TrainList;
        Services.Get(url)
            .then(result => {

                this.setState({
                    fund: result.data.article_list,
                });
            })
            .catch(error => {
                // consoleLog('链接服务器出错，请稍后重试', error);
            })

    }

    loadWorkPlatformData=(token)=>{
        let url = ServicesApi.getOrderNumber;
        Services.Get(url)
            .then(result => {

                this.setState({
                    new_number: result.data.new_number,
                    working_number:result.data.working_number,
                    all_number:result.data.all_number
                });
            })
            .catch(error => {
                // consoleLog('链接服务器出错，请稍后重试', error);
            })
    }
    //获取走进我们数据
    loadWalkinUs=(token)=>{

        let url = ServicesApi.getUsChoose;
        Services.Get(url)
            .then(result => {

                this.setState({
                    about_us: result.data.article_list,
                });
            })
            .catch(error => {
                consoleLog('链接服务器出错，请稍后重试', error);
            })

    }

    onBack = () => {
        this.props.navigation.goBack();
    }


    componentWillUnmount() {
        this.setState({
            isPlay: false
        })
    }

    getUserInfo = () => {
        let url = ServicesApi.get_member_info;

        let data = {
            member_id: global.user.userData.id,
        };
        Services.Post(url, data)
            .then(result => {
                // console.log(result.code);
                if (result && result.code == 1) {
                    // console.log(result);
                    let user = result.data;

                    this.setState({
                        user: user
                    });

                    storage.save({
                        key: 'loginState',
                        data: {
                            id: global.user.userData.id,
                            account: user.account,
                            nickname: user.nickname,
                            head_img: user.head_img,
                            sex: user.sex,
                            integral: user.integral,
                            money: user.money,
                            email: user.email,
                            addr: user.addr,
                            bank_num: user.bank_num,
                            is_investor: user.is_investor,
                            investor_msg: user.investor_msg,
                            back_msg: user.back_msg,
                            special_type: user.special_type,
                            is_risk: user.is_risk,
                            is_bank_card: user.is_bank_card,
                            is_modify_truename: user.is_modify_truename,
                            is_special_investor: user.is_special_investor,
                            is_pay_auth: user.is_pay_auth,
                            is_pay_auth_msg: user.is_pay_auth_msg,
                            token: global.user.userData.token,
                        },
                    });

                    global.user = {
                        loginState: true,
                        userData: {
                            id: global.user.userData.id,
                            account: user.account,
                            nickname: user.nickname,
                            head_img: user.head_img,
                            sex: user.sex,
                            integral: user.integral,
                            money: user.money,
                            email: user.email,
                            addr: user.addr,
                            bank_num: user.bank_num,
                            is_investor: user.is_investor,
                            investor_msg: user.investor_msg,
                            back_msg: user.back_msg,
                            special_type: user.special_type,
                            is_risk: user.is_risk,
                            is_bank_card: user.is_bank_card,
                            is_modify_truename: user.is_modify_truename,
                            is_special_investor: user.is_special_investor,
                            is_pay_auth: user.is_pay_auth,
                            is_pay_auth_msg: user.is_pay_auth_msg,
                            token: global.user.userData.token,
                        }
                    };

                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    videoPlay = () => {
        this.setState({
            isPlay: !this.state.isPlay
        })
    }

    onPushNavigator = (compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            onCallBack: () => {
                this.loadBannerData(result.token);
                // this.loadProductArticleData(result.token);
            }
        })
    }

    onPushToFundDetail = (compent, fund_id, title, datum_rate, buy_end_day, buy_end_money, time_limit, start_buy_money, end_people, is_collection) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            fund_id: fund_id,
            title: title,
            datum_rate: datum_rate,
            buy_end_day: buy_end_day,
            buy_end_money: buy_end_money,
            time_limit: time_limit,
            start_buy_money: start_buy_money,
            end_people: end_people,
            is_collection: is_collection,
            onCallBack: () => {
                // this.loadNetData(global.user.userData.id);
                this.loadBannerData(result.token);
                // this.loadProductArticleData(result.token);
            }
        })
    }

    toWebview = (link,content_id,is_collection, compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            link: link,
            content_id:content_id,
            is_collection:is_collection

        })
    }

    setModalVisible = (visible) => {
        this.setState({modalVisible: visible});
    }

    render() {
        let {activityBanner} = this.state;
        return (
            <View style={styles.container}>

                <ScrollView>
                    <BannerView bannerData={this.state.banner} {...this.props} />

                    <TouchableOpacity onPress={() => {
                        // this.toWebview(this.state.notice.notice_url, 'NewsWebDetail')
                    }} style={[styles.gonggao, GlobalStyles.flexRowStart]}>
                        <Image source={Images.icon_gonggao}
                               style={styles.gonggaoimg}/>
                        <Text style={styles.gonggaotext}>新版本即将于5月31日上线</Text>
                    </TouchableOpacity>

                    <View style={GlobalStyles.whiteModule}>
                        <View style={[GlobalStyles.titleModule, GlobalStyles.flexRowBetween]}>
                            <View style={[GlobalStyles.titleLeft, GlobalStyles.flexRowStart]}>
                                <Image source={Images.icon_index_jijinjingxuan}
                                       style={GlobalStyles.titleIcon}/>
                                <Text style={GlobalStyles.titleText}>工作台</Text>
                            </View>
                            <TouchableOpacity onPress={() => {
                                this.onPushNavigator('OrderManage', {page_flag: 'second'})
                            }} style={[GlobalStyles.titleRight, GlobalStyles.flexRowStart]}>
                                <Text style={GlobalStyles.titleMoreText}>更多</Text>
                                <Image source={Images.icon_index_more}
                                       style={GlobalStyles.titleMoreIcon}/>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity style={GlobalStyles.firstMiddle}>
                            <View style={[GlobalStyles.fundBot, GlobalStyles.flexRowBetween]}>
                                <View style={[GlobalStyles.fundLeft, GlobalStyles.flexColumnCenter]}>
                                    <Text style={[GlobalStyles.fundLeftValue]}>新派遣</Text>
                                    <Text style={[GlobalStyles.fundLeftKey, styles.selffont]}>{this.state.new_number}</Text>
                                </View>
                                <View style={[GlobalStyles.fundCenter, GlobalStyles.flexColumnCenter]}>
                                    <Text style={GlobalStyles.fundCenterValue}>进行中</Text>
                                    <Text style={[GlobalStyles.fundCenterKey, styles.selffont]}>{this.state.working_number}</Text>
                                </View>
                                <View style={[GlobalStyles.fundRight, GlobalStyles.flexColumnCenter]}>
                                    <Text style={[GlobalStyles.fundCenterValue]}>总订单</Text>
                                    <Text style={[GlobalStyles.fundRightKey, styles.selffont]}>{this.state.all_number}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={GlobalStyles.whiteModule}>
                        <View style={[GlobalStyles.titleModule, GlobalStyles.flexRowBetween]}>
                            <View style={[GlobalStyles.titleLeft, GlobalStyles.flexRowStart]}>
                                <Image source={Images.icon_peixun}
                                       style={GlobalStyles.titleIcon}/>
                                <Text style={GlobalStyles.titleText}>培训内容精选</Text>
                            </View>
                            <TouchableOpacity onPress={() => {
                                this.onPushNavigator('Fund')
                            }} style={[GlobalStyles.titleRight, GlobalStyles.flexRowStart]}>
                                <Text style={GlobalStyles.titleMoreText}>更多</Text>
                                <Image source={Images.icon_index_more}
                                       style={GlobalStyles.titleMoreIcon}/>
                            </TouchableOpacity>
                        </View>
                        {this.homeFund(this.state.fund)}
                    </View>

                    <View style={GlobalStyles.whiteModule}>
                        <View style={[GlobalStyles.titleModule, GlobalStyles.flexRowBetween]}>
                            <View style={[GlobalStyles.titleLeft, GlobalStyles.flexRowStart]}>
                                <Image source={Images.icon_product}
                                       style={GlobalStyles.titleIcon}/>
                                <Text style={GlobalStyles.titleText}>产品资料精选</Text>
                            </View>
                            <TouchableOpacity onPress={() => {
                                this.onPushNavigator('Fund')
                            }} style={[GlobalStyles.titleRight, GlobalStyles.flexRowStart]}>
                                <Text style={GlobalStyles.titleMoreText}>更多</Text>
                                <Image source={Images.icon_index_more}
                                       style={GlobalStyles.titleMoreIcon}/>
                            </TouchableOpacity>
                        </View>
                        {this.homeInvest(this.state.article)}
                    </View>

                    <View style={GlobalStyles.whiteModule}>
                        <View style={[GlobalStyles.titleModule, GlobalStyles.flexRowBetween]}>
                            <View style={[GlobalStyles.titleLeft, GlobalStyles.flexRowStart]}>
                                <Image source={Images.icon_goin}
                                       style={GlobalStyles.titleIcon}/>
                                <Text style={GlobalStyles.titleText}>走进我们</Text>
                            </View>
                            <TouchableOpacity onPress={() => {
                                this.toWebview('http://yuntu.3todo.com/index/article/info/id/3', '', '', 'NewsWebDetail')
                            }} style={[GlobalStyles.titleRight, GlobalStyles.flexRowStart]}>
                                <Text style={GlobalStyles.titleMoreText}>更多</Text>
                                <Image source={Images.icon_index_more}
                                       style={GlobalStyles.titleMoreIcon}/>
                            </TouchableOpacity>
                        </View>
                        {this.homeAbout(this.state.about_us)}
                    </View>

                    {/*<Modal*/}
                    {/*animationType={"none"}*/}
                    {/*transparent={false}*/}
                    {/*visible={this.state.modalVisible}*/}
                    {/*>*/}
                    {/*<TouchableOpacity style={styles.modalCeng} onPress={() => {*/}
                    {/*this.setModalVisible(false)*/}
                    {/*}}>*/}
                    {/*<Image source={Images.modal} resizeMode="stretch"*/}
                    {/*style={styles.modalpic}/>*/}
                    {/*</TouchableOpacity>*/}
                    {/*</Modal>*/}


                </ScrollView>
            </View>
        );
    }


    homeFund(data) {
        // console.log(global.user.loginState);
        if (this.state.loginState) {
            // console.log(data);
            if (data.length > 0) {
                let fundList = [];
                for (let i = 0; i < data.length; i++) {
                    // let id = data[i].id;
                    // let title = data[i].title;
                    // let datum_rate = data[i].datum_rate;
                    // let buy_end_day = data[i].buy_end_day;
                    // let buy_end_money = data[i].buy_end_money;
                    // let time_limit = data[i].time_limit;
                    // let start_buy_money = data[i].start_buy_money;
                    // let end_people = data[i].end_people;
                    // let is_collection = data[i].is_collection;
                    // consoleLog('id', id);
                    let content_url = data[i].article_url;
                    let content_id=data[i].id;
                    let is_collection=data[i].is_collection;
                    let fundItem = (
                        <TouchableOpacity key={i} onPress={() => {
                            this.toWebview(content_url,content_id,is_collection, 'NewsWebDetail')
                        }} style={GlobalStyles.fundList}>
                            <View style={[GlobalStyles.fundTop, GlobalStyles.flexRowBetween]}>
                                <Text style={GlobalStyles.fundTitle}>{data[i].title}</Text>
                                {/*<Text style={GlobalStyles.fundDakuan}>截止打款{data[i].buy_end_day}天</Text>*/}
                            </View>
                            <View style={GlobalStyles.fundMid}>
                                <Image source={{uri: data[i].thumb}} style={GlobalStyles.fundThumb}/>
                            </View>
                            <View style={[GlobalStyles.fundBot, GlobalStyles.flexRowBetween]}>
                                <View style={GlobalStyles.userlistleft}>
                                    <Image source={Images.icon_shoucang_item} style={styles.icon} />
                                </View>
                                <View style={GlobalStyles.userlistright}>
                                    <Text style={GlobalStyles.userlisttext}>{data[i].views}</Text>
                                    <Text  style={styles.text} >{data[i].create_time}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                    fundList.push(fundItem);
                }
                return (
                    <View style={GlobalStyles.fundModule}>
                        {fundList}
                    </View>
                );
            } else {
                return <View style={[GlobalStyles.whiteModule, {height: 360}]}></View>
            }
        } else {
            if (data.length > 0) {
                let fundList = [];
                for (let i = 0; i < data.length; i++) {
                    let fundItem = (
                        <TouchableOpacity key={i} onPress={() => {
                            this.onPushToFundDetail('Login')
                        }} style={GlobalStyles.fundList}>
                            <View style={[GlobalStyles.fundTop, GlobalStyles.flexRowBetween]}>
                                <Text style={GlobalStyles.fundTitle}>{data[i].title}</Text>
                                <Image source={Images.image_index_mohu1}
                                       style={GlobalStyles.fundMohu1}/>
                            </View>
                            <View style={GlobalStyles.fundMid}>
                                <Image source={{uri: data[i].photo}} style={GlobalStyles.fundThumb}/>
                            </View>
                            <View style={[GlobalStyles.fundBot, GlobalStyles.flexRowBetween]}>
                                <Image source={Images.image_index_mohu}
                                       style={GlobalStyles.fundMohu}/>
                            </View>
                        </TouchableOpacity>
                    )
                    fundList.push(fundItem);
                }
                return (
                    <View style={GlobalStyles.fundModule}>
                        {fundList}
                    </View>
                );
            } else {
                return <View style={[GlobalStyles.whiteModule, {height: 360}]}></View>
            }
        }
    }

//{/*<Image source={Images.images_index_video} style={GlobalStyles.videoImg} />*/}
    homeAbout(data) {
        let aboutList = [];
        for (let i = 0; i < data.length; i++) {
            let content_url = data[i].article_url;
            let id=data[i].id;
            let isCollection=data[i].is_collection;
            let aboutItem = (
                <TouchableOpacity key={i} onPress={() => {
                    this.toWebview(content_url,id,isCollection, 'NewsWebDetail')
                }} style={[GlobalStyles.newsList, GlobalStyles.flexRowStart]}>
                    <View style={GlobalStyles.newsLeft}>
                        <Image source={{uri: data[i].thumb}} style={GlobalStyles.newsThumb}/>
                    </View>
                    <View style={[GlobalStyles.newsRight, GlobalStyles.flexColumnBetweenStart]}>
                        <Text style={GlobalStyles.newsTitle}>{data[i].title}</Text>
                        <Text style={GlobalStyles.newsDesc}>{data[i].remark}</Text>
                        <View style={[GlobalStyles.newsInfo, GlobalStyles.flexRowBetween]}>
                            <Text style={GlobalStyles.newsDate}>{data[i].create_time}</Text>
                            <View style={[GlobalStyles.newsClick, GlobalStyles.flexRowStart]}>
                                <Text style={GlobalStyles.newsClickNum}>{data[i].views}</Text>
                                <Text style={GlobalStyles.newsClickText}>阅读</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            )
            aboutList.push(aboutItem);
        }
        return (
            <View style={GlobalStyles.newsModule}>
                {aboutList}
            </View>
        )

    }


    homeInvest(data) {
        let investList = [];
        for (let i = 0; i < data.length; i++) {
            let content_url = data[i].article_url;
            let content_id=data[i].id;
            let is_collection=data[i].is_collection
            let investItem = (
                <TouchableOpacity key={i} onPress={() => {
                    this.toWebview(content_url,content_id,is_collection, 'NewsWebDetail')
                }} style={[GlobalStyles.newsList, GlobalStyles.flexRowStart,]}>
                    <View style={GlobalStyles.newsLeft}>
                        <Image source={{uri: data[i].thumb}} style={GlobalStyles.newsThumb}/>
                    </View>
                    <View style={[GlobalStyles.newsRight, GlobalStyles.flexColumnBetweenStart]}>
                        <Text style={GlobalStyles.newsTitle}>{data[i].title}</Text>
                        <Text style={GlobalStyles.newsDesc}>{data[i].remark}</Text>
                        <View style={[GlobalStyles.newsInfo, GlobalStyles.flexRowBetween]}>
                            <Text style={GlobalStyles.newsDate}>{data[i].create_time}</Text>
                            <View style={[GlobalStyles.newsClick, GlobalStyles.flexRowStart]}>
                                <Text style={GlobalStyles.newsClickText}>阅读</Text>
                                <Text style={GlobalStyles.newsClickNum}>{data[i].views}</Text>

                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            )
            investList.push(investItem);
        }
        return (
            <View style={GlobalStyles.newsModule}>
                {investList}
            </View>
        )

    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    gonggao: {
        backgroundColor: '#fff',
        height: 45,
        paddingLeft: 15,
        paddingRight: 15,
    },
    gonggaoimg: {
        width: 19,
        height: 19 * 60 / 64,
        marginRight: 10
    },
    gonggaotext: {
        flex: 1,
        color: '#888888',
        fontSize: 15
    },
    fundLogin: {
        width: GlobalStyles.width,
        flex: 1,
        backgroundColor: '#fff'
    },
    fundLoginImg: {
        width: GlobalStyles.width,
        height: GlobalStyles.width * 860 / 660,
    },
    submitbot: {
        position: 'absolute',
        bottom: 0,
        width: GlobalStyles.width - 30,
        borderRadius: 0,
        height: 42
    },
    backgroundVideo: {
        width: GlobalStyles.width - 30,
        height: (GlobalStyles.width - 30) * 460 / 800,
        backgroundColor: '#000',
        borderRadius: 3
    },
    play: {
        position: 'absolute',
        top: (GlobalStyles.width - 30) * 460 / 800 / 2,
        left: (GlobalStyles.width - 30) / 2,
        marginLeft: -35,
        marginTop: -35,
        width: 70,
        height: 70
    },
    playico: {
        width: 70,
        height: 70,
    },
    video: {
        margin: 15,
        marginBottom: 5,
        borderRadius: 3
    },
    modalpic: {
        width: GlobalStyles.width,
        height: GlobalStyles.height
    },
    modalCeng: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: GlobalStyles.width,
        height: GlobalStyles.height,
    },
    selffont: {
        fontSize: 20,
        color: "#597290"
    },
    selfimage :{
        width:15,
        height:15,
        margin:5
    },
    text :{
        width:80,
    },
    icon:{
        width:18,
        height:18
    }
});