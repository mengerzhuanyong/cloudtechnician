/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'
import ActionSheet from 'react-native-actionsheet'

// 弹窗
const CANCEL_INDEX = 0
const DESTRUCTIVE_INDEX = 4
const options = ['取消', '确认退出']
const title = '您确认要退出吗？'
export default class Huiyuan extends Component {

    onBack = () => {
        this.props.navigation.state.params.onCallBack();
        this.props.navigation.goBack();
    }
    componentWillUnmount() {
        this.onBack();
    }
    updateState = (state) => {
        if (!this) {
            return
        }
        this.setState(state);
    }
    loadNetData = () => {
        let url = ServicesApi.getSkill;

        Services.Get(url)
            .then(result => {
                // console.log(result.code);
                if (result) {
                    this.setState({
                        skillstate: result.data.status,
                    })
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }
    onPushNavigator = (compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            is_investor: this.state.is_investor,
            investor_msg: this.state.investor_msg,
            back_msg: this.state.back_msg,
            is_risk: this.state.is_risk,
            is_bank_card: this.state.is_bank_card,
            is_modify_truename: this.state.is_modify_truename,
            is_special_investor: this.state.is_special_investor,
            is_pay_auth: this.state.is_pay_auth,
            is_pay_auth_msg: this.state.is_pay_auth_msg,
            onCallBack: () => {
                this.loadNetData();
            }
        })
    }
    showActionSheet = () => {
        this.ActionSheet.show()
    }
    handlePress = (i) => {
        if (i == 1) {
            this.doLogOut();
        }
    }
    doLogOut = () => {
        this.cleanUserInfo();
        this.timer = setTimeout(() => {
            RouterHelper.reset('', 'Login');
        }, 1000);
    }

    cleanUserInfo = () => {
        global.token = '';
        StorageManager.remove(Constant.USER_INFO_KEY);
    }
    yinhangka = () => {
        if (this.state.is_pay_auth == -1) {
            if (this.state.is_investor == 0) {
                return (
                    <TouchableOpacity onPress={() => {
                        this.onPushNavigator('Yinhangka')
                    }} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_member_yinhangka}
                                   style={GlobalStyles.usericon}/>
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>银行卡管理</Text>
                            {/*<Text style={styles.userlistRightText}>{this.state.is_bank_card == 1 ? '已绑定' : '未绑定'}</Text>*/}
                            <Image source={Images.icon_user_arrow}
                                   style={GlobalStyles.userlistmore}/>
                        </View>
                    </TouchableOpacity>
                )
            } else {
                return (
                    <TouchableOpacity onPress={() => {
                        toastShort(this.state.is_pay_auth_msg);
                        this.onPushNavigator('Yinhangka')
                    }} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_member_yinhangka}
                                   style={GlobalStyles.usericon}/>
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>银行卡管理</Text>
                            {/*<Text style={styles.userlistRightText}>{this.state.is_bank_card == 1 ? '已绑定' : '未绑定'}</Text>*/}
                            <Image source={Images.icon_user_arrow}
                                   style={GlobalStyles.userlistmore}/>
                        </View>
                    </TouchableOpacity>
                )
            }
        } else {
            if (this.state.is_bank_card == 1) {
                return (
                    <TouchableOpacity onPress={() => {
                        this.onPushNavigator('Yinhangka')
                    }} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_member_yinhangka}
                                   style={GlobalStyles.usericon}/>
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>银行卡管理</Text>
                            {/*<Text style={styles.userlistRightText}>{this.state.is_bank_card == 1 ? '已绑定' : '未绑定'}</Text>*/}
                            <Image source={Images.icon_user_arrow}
                                   style={GlobalStyles.userlistmore}/>
                        </View>
                    </TouchableOpacity>
                )
            } else {
                return (
                    <TouchableOpacity onPress={() => {
                        this.onPushNavigator('Yinhangka')
                    }} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_member_yinhangka}
                                   style={GlobalStyles.usericon}/>
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>银行卡管理</Text>
                            {/*<Text style={styles.userlistRightText}>{this.state.is_bank_card == 1 ? '已绑定' : '未绑定'}</Text>*/}
                            <Image source={Images.icon_user_arrow}
                                   style={GlobalStyles.userlistmore}/>
                        </View>
                    </TouchableOpacity>
                )
            }

        }
    }
    pinggu = () => {
        return (
            <View>

                <TouchableOpacity onPress={() => {
                    this.onPushNavigator('Pinggu')
                }} style={GlobalStyles.userlist}>
                    <View style={GlobalStyles.userlistleft}>
                        <Image source={Images.icon_member_fengxianpinggu}
                               style={GlobalStyles.usericon}/>
                    </View>
                    <View style={GlobalStyles.userlistright}>
                        <Text style={GlobalStyles.userlisttext}>技能认证</Text>
                        <Image source={Images.icon_user_arrow}
                               style={GlobalStyles.userlistmore}/>
                    </View>
                </TouchableOpacity>

            </View>
        )
    }

    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        this.state = {
            user: global.user.userData,
            is_investor: params.is_investor,
            investor_msg: params.investor_msg,
            back_msg: params.back_msg,
            is_risk: params.is_risk,
            is_bank_card: params.is_bank_card,
            is_modify_truename: params.is_modify_truename,
            is_special_investor: params.is_special_investor,
            is_pay_auth: params.is_pay_auth,
            is_pay_auth_msg: params.is_pay_auth_msg,
            skillstate: ''
        }
    }

    componentDidMount() {
        this.loadNetData();
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'会员中心'}
                />

                <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                    <TouchableOpacity onPress={() => {
                        this.onPushNavigator('Gerenxinxi')
                    }} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_member_gerenxinxi}
                                   style={GlobalStyles.usericon}/>
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>个人信息</Text>
                            <Image source={Images.icon_user_arrow}
                                   style={GlobalStyles.userlistmore}/>
                        </View>
                    </TouchableOpacity>
                    {this.yinhangka()}

                    <TouchableOpacity onPress={() => {
                        this.onPushNavigator('SetMima')
                    }} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_member_mimaguanli}
                                   style={GlobalStyles.usericon}/>
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>修改密码</Text>
                            <Image source={Images.icon_user_arrow}
                                   style={GlobalStyles.userlistmore}/>
                        </View>
                    </TouchableOpacity>
                    {this.pinggu()}

                </View>

                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={title}
                    options={options}
                    cancelButtonIndex={CANCEL_INDEX}
                    destructiveButtonIndex={DESTRUCTIVE_INDEX}
                    onPress={this.handlePress}
                />
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    mtop: {
        backgroundColor: GlobalStyles.themeColor,
        height: 160,
        padding: 15,
    },
    minfo: {
        height: 30,
    },
    mtouxiang: {
        width: 32,
        height: 32,
        marginRight: 6,
        borderRadius: 16,
    },
    mnicheng: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },
    mshenfen: {
        backgroundColor: '#fff',
        marginTop: 15,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        height: 100,
        width: GlobalStyles.width - 30,
    },
    msfico: {
        height: (GlobalStyles.width - 80) * 96 / 1251,
        width: GlobalStyles.width - 80,
        marginTop: 15
    },
    msftext: {
        fontSize: 16,
        color: GlobalStyles.themeColor,
        fontWeight: 'bold',
        marginTop: 13
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
});
