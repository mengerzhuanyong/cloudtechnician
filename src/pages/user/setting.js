/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    Alert,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'
import JPushModule from 'jpush-react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import ActionSheet from 'react-native-actionsheet'

// 弹窗
const CANCEL_INDEX = 0
const DESTRUCTIVE_INDEX = 4
const options = [ '取消', '确认退出']
const title = '您确认要退出吗？'
export default class Setting extends Component {

    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state =  {
            user: global.user.userData,
            is_investor: params.is_investor,
            investor_msg: params.investor_msg,
            back_msg: params.back_msg,
            is_risk: params.is_risk,
            is_bank_card: params.is_bank_card,
            is_modify_truename: params.is_modify_truename,
            is_special_investor: params.is_special_investor,
            is_pay_auth: params.is_pay_auth,
            is_pay_auth_msg: params.is_pay_auth_msg,
        }
    }

    componentDidMount(){
        // this.loadNetData();
    }

    onBack = () => {
        const {goBack, state} = this.props.navigation;
        state.params && state.params.onCallBack && state.params.onCallBack();
        goBack();
    }

    componentWillUnmount() {
        let timers = [this.timer, this.timer1];
        ClearTimer(timers);
    }

    loadNetData = () => {
        let url = ServicesApi.get_member_info;
    }

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            is_investor: this.state.is_investor,
            investor_msg: this.state.investor_msg,
            back_msg: this.state.back_msg,
            is_risk: this.state.is_risk,
            is_bank_card: this.state.is_bank_card,
            is_modify_truename: this.state.is_modify_truename,
            is_special_investor: this.state.is_special_investor,
            is_pay_auth: this.state.is_pay_auth,
            is_pay_auth_msg: this.state.is_pay_auth_msg,
            onCallBack:()=>{
                this.loadNetData();
            }
        })
    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    handlePress = (i) => {
        if(i == 1){
            this.doLogOut();
        }
    }

    doLogOut = () => {
        this.cleanUserInfo();
        this.deleteAlias();
        this.timer = setTimeout(() => {
            RouterHelper.reset('', 'Login');
        }, 1000);
    }

    cleanUserInfo = () => {
        global.token = '';
        StorageManager.remove(Constant.USER_INFO_KEY);
    }

    deleteAlias = () => {
        JPushModule.deleteAlias(map => {
            if (map.errorCode === 0) {
                // console.log('delete alias succeed')
            } else {
                // console.log('delete alias failed, errorCode: ', map.errorCode)
            }
        })
    }

    toWebview = (pageTitle,link,content_id,is_collection, compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            pageTitle: pageTitle,
            link: link,
            content_id:content_id,
            is_collection:is_collection,
            type: 2,
        })
    }

    render(){
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = {'设置'}
                />

                <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                    <TouchableOpacity
                        onPress={() => RouterHelper.navigate('关于我们', 'WebViewPage', {url: ServicesApi.about})}
                        style={GlobalStyles.userlist}
                    >

                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>关于我们</Text>
                            <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => RouterHelper.navigate('联系客服', 'WebViewPage', {url: ServicesApi.customer_services})}
                        style={GlobalStyles.userlist}
                    >

                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>联系客服</Text>
                            <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={GlobalStyles.userlist}
                        onPress={() => {
                            this.timer1 = setTimeout(() => {
                                Alert.alert('', '清除成功', [{text: '确定', onPress: () => {}}]);
                            }, 1000);
                        }}
                    >
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>清除缓存</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>当前版本</Text>
                            <Text style={[GlobalStyles.userlisttext, {fontSize: 13, color: '#999'}]}>已是最新版本</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                    <TouchableOpacity onPress = {() => {this.showActionSheet()}} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>安全退出</Text>
                            <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                        </View>
                    </TouchableOpacity>
                </View>

                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={title}
                    options={options}
                    cancelButtonIndex={CANCEL_INDEX}
                    destructiveButtonIndex={DESTRUCTIVE_INDEX}
                    onPress={this.handlePress}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    mtop: {
        backgroundColor: GlobalStyles.themeColor,
        height: 160,
        padding: 15,
    },
    minfo: {
        height: 30,
    },
    mtouxiang: {
        width: 32,
        height: 32,
        marginRight: 6,
        borderRadius: 16,
    },
    mnicheng: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },
    mshenfen: {
        backgroundColor: '#fff',
        marginTop: 15,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        height: 100,
        width: GlobalStyles.width - 30,
    },
    msfico: {
        height: (GlobalStyles.width-80)*96/1251,
        width: GlobalStyles.width-80,
        marginTop: 15
    },
    msftext: {
        fontSize: 16,
        color: GlobalStyles.themeColor,
        fontWeight: 'bold',
        marginTop: 13
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
});
