/**
 * 云技师 - 日期管理
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'
import ActionSheet from 'react-native-actionsheet'
import CheckboxCustom from 'react-native-checkboxcustom'

// 弹窗
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 4;
options = ['上午',
    '下午',
    '全天',];
const title = '您确认要退出吗？';

export default class DateManage extends Component {

    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        this.state = {
            user: global.user.userData,
            dateone: 0,
            datetwo: 0,
            datethree: 0,
            datefour: 0,
            datefive: 0,
            datesix: 0,
            dateseven: 0
        }
    }

    componentDidMount() {
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.state.params.onCallBack();
        this.props.navigation.goBack();
    }

    componentWillUnmount() {
        let timers = [this.timer];
        ClearTimer(timers);
    }

    loadNetData = () => {
        let url = ServicesApi.getMyworkTime;
        Services.Get(url)
            .then(result => {
                if (result.code == 1) {
                    this.updateDate(result.data.list);
                }
            })
            .catch(error => {
                // toastShort('服务器请求失败，请稍后重试！');
            })
    }

    updateDate = (data) => {
        for (let i = 0; i < data.length; i++) {
            let arr = data[i].split(',');
            switch(arr[0]) {
                case 'A1':
                    this.setState({dateone: arr[1]});
                    break;
                case 'A2':
                    this.setState({datetwo: arr[1]});
                    break;
                case 'A3':
                    this.setState({datethree: arr[1]});
                    break;
                case 'A4':
                    this.setState({datefour: arr[1]});
                    break;
                case 'A5':
                    this.setState({datefive: arr[1]});
                    break;
                case 'A6':
                    this.setState({datesix: arr[1]});
                    break;
                case 'A0':
                    this.setState({dateseven: arr[1]});
                    break;
                default:
                    this.setState({dateone: arr[1]});
                    break;
            }
        }
    }

    handlePress = (i) => {
        if (i == 1) {
            this.doLogOut();
        }
    }

    handleDateOne = (option) => {
        let {dateone} = this.state;

        if (option.length < 1) {
            this.setState({
                dateone: 0,
            })
        }
        if (option[0] === "上午") {
            this.setState({
                dateone: 1,
            })
        } else if (option[0] === "下午") {
            this.setState({
                dateone: 2,
            })
        } else if (option[0] === "全天") {
            this.setState({
                dateone: 3,
            })
        }
    }

    handleDateTwo = (option) => {
        let {datetwo} = this.state;

        if (option.length < 1) {
            this.setState({
                datetwo: 0,
            })
        }
        if (option[0] === "上午") {
            this.setState({
                datetwo: 1,
            })
        } else if (option[0] === "下午") {
            this.setState({
                datetwo: 2,
            })
        } else if (option[0] === "全天") {
            this.setState({
                datetwo: 3,
            })
        }
    }
    handleDateThree = (option) => {
        let {datethree} = this.state;

        if (option.length < 1) {
            this.setState({
                datethree: 0,
            })
        }
        if (option[0] === "上午") {
            this.setState({
                datethree: 1,
            })
        } else if (option[0] === "下午") {
            this.setState({
                datethree: 2,
            })
        } else if (option[0] === "全天") {
            this.setState({
                datethree: 3,
            })
        }
    }
    handleDateFour = (option) => {
        let {datefour} = this.state;

        if (option.length < 1) {
            this.setState({
                datefour: 0,
            })
        }
        if (option[0] === "上午") {
            this.setState({
                datefour: 1,
            })
        } else if (option[0] === "下午") {
            this.setState({
                datefour: 2,
            })
        } else if (option[0] === "全天") {
            this.setState({
                datefour: 3,
            })
        }
    }
    handleDateFive = (option) => {
        let {datefive} = this.state;

        if (option.length < 1) {
            this.setState({
                datefive: 0,
            })
        }
        if (option[0] === "上午") {
            this.setState({
                datefive: 1,
            })
        } else if (option[0] === "下午") {
            this.setState({
                datefive: 2,
            })
        } else if (option[0] === "全天") {
            this.setState({
                datefive: 3,
            })
        }
    }
    handleDateSix = (option) => {
        let {datesix} = this.state;

        if (option.length < 1) {
            this.setState({
                datesix: 0,
            })
        }
        if (option[0] === "上午") {
            this.setState({
                datesix: 1,
            })
        } else if (option[0] === "下午") {
            this.setState({
                datesix: 2,
            })
        } else if (option[0] === "全天") {
            this.setState({
                datesix: 3,
            })
        }
    }
    handledateSeven = (option) => {
        let {dateseven} = this.state;

        if (option.length < 1) {
            this.setState({
                dateseven: 0,
            })
        }
        if (option[0] === "上午") {
            this.setState({
                dateseven: 1,
            })
        } else if (option[0] === "下午") {
            this.setState({
                dateseven: 2,
            })
        } else if (option[0] === "全天") {
            this.setState({
                dateseven: 3,
            })
        }
    }
    save = () => {
        let {dateone, datetwo, datethree, datefour, datefive, datesix, dateseven} = this.state;
        let url = ServicesApi.updateMyWorkeTime;
        let worktimedata = '';

        let data1 = ['A1', dateone];
        let data2 = ['A2', datetwo];
        let data3 = ['A3', datethree];
        let data4 = ['A4', datefour];
        let data5 = ['A5', datefive];
        let data6 = ['A6', datesix];
        let data7 = ['A0', dateseven];

        if (dateone != 0) {
            worktimedata += ([data1] + ";")
        }
        if (datetwo != 0) {
            worktimedata += ([data2] + ";")
        }
        if (datethree != 0) {
            worktimedata += ([data3] + ";")
        }
        if (datefour != 0) {
            worktimedata += ([data4] + ";")
        }
        if (datefive != 0) {
            worktimedata += ([data5] + ";")
        }
        if (datesix != 0) {
            worktimedata += ([data6] + ";")
        }
        if (dateseven != 0) {
            worktimedata += ([data7] + ";")
        }

        let data = {
            worktime: worktimedata
        }
        Services.Post(url, data)
            .then(result => {
                if (result.code == 1) {
                    toastShort(result.msg);
                    this.timer = setTimeout(() => {
                        RouterHelper.goBack();
                    }, 1000);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    render() {
        let {dateone, datetwo, datethree, datefour, datefive, datesix, dateseven} = this.state;
        // console.log(dateone, datetwo, datethree, datefour, datefive, datesix, dateseven);
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'日期管理'}
                />

                <View style={styles.content}>
                    <View style={styles.dateItemView}>
                        <Text style={styles.dateItemTitle}>周一</Text>
                        <CheckboxCustom
                            options={options}
                            optionStyle={styles.optionStyle}
                            selectedOptions={[options[dateone - 1]]}
                            maxSelectedOptions={1}
                            onSelection={(option) => this.handleDateOne(option)}
                        />
                    </View>
                    <View style={styles.dateItemView}>
                        <Text style={styles.dateItemTitle}>周二</Text>
                        <CheckboxCustom
                            options={options}
                            optionStyle={styles.optionStyle}
                            selectedOptions={[options[datetwo - 1]]}
                            maxSelectedOptions={1}
                            onSelection={(option) => this.handleDateTwo(option)}
                        />
                    </View>
                    <View style={styles.dateItemView}>
                        <Text style={styles.dateItemTitle}>周三</Text>
                        <CheckboxCustom
                            options={options}
                            optionStyle={styles.optionStyle}
                            selectedOptions={[options[datethree - 1]]}
                            maxSelectedOptions={1}
                            onSelection={(option) => this.handleDateThree(option)}
                        />
                    </View>
                    <View style={styles.dateItemView}>
                        <Text style={styles.dateItemTitle}>周四</Text>
                        <CheckboxCustom
                            options={options}
                            optionStyle={styles.optionStyle}
                            selectedOptions={[options[datefour - 1]]}
                            maxSelectedOptions={1}
                            onSelection={(option) => this.handleDateFour(option)}
                        />
                    </View>
                    <View style={styles.dateItemView}>
                        <Text style={styles.dateItemTitle}>周五</Text>
                        <CheckboxCustom
                            options={options}
                            optionStyle={styles.optionStyle}
                            selectedOptions={[options[datefive - 1]]}
                            maxSelectedOptions={1}
                            onSelection={(option) => this.handleDateFive(option)}
                        />
                    </View>
                    <View style={styles.dateItemView}>
                        <Text style={styles.dateItemTitle}>周六</Text>
                        <CheckboxCustom
                            options={options}
                            optionStyle={styles.optionStyle}
                            selectedOptions={[options[datesix - 1]]}
                            maxSelectedOptions={1}
                            onSelection={(option) => this.handleDateSix(option)}
                        />
                    </View>
                    <View style={styles.dateItemView}>
                        <Text style={styles.dateItemTitle}>周天</Text>
                        <CheckboxCustom
                            options={options}
                            optionStyle={styles.optionStyle}
                            selectedOptions={[options[dateseven - 1]]}
                            maxSelectedOptions={1}
                            onSelection={(option) => this.handledateSeven(option)}
                        />
                    </View>
                </View>
                <TouchableOpacity
                    style={styles.btnView}
                    onPress={this.save}
                >
                    <Text style={styles.btnName}> 保存</Text>

                </TouchableOpacity>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    content: {
        marginTop: 10,
        backgroundColor: '#fff',
    },
    mtop: {
        backgroundColor: GlobalStyles.themeColor,
        height: 160,
        padding: 15,
    },
    minfo: {
        height: 30,
    },
    mtouxiang: {
        width: 32,
        height: 32,
        marginRight: 6,
        borderRadius: 16,
    },
    mnicheng: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },
    mshenfen: {
        backgroundColor: '#fff',
        marginTop: 15,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        height: 100,
        width: GlobalStyles.width - 30,
    },
    msfico: {
        height: (GlobalStyles.width - 80) * 96 / 1251,
        width: GlobalStyles.width - 80,
        marginTop: 15
    },
    msftext: {
        fontSize: 16,
        color: GlobalStyles.themeColor,
        fontWeight: 'bold',
        marginTop: 13
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
    dateItemView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 30,
        paddingTop: 5,
        paddingBottom: 5,
        // backgroundColor: '#123',
        borderTopWidth: 1,
        borderColor: '#ddd'
    },
    dateItemTitle: {
        marginRight: 20,
    },
    optionStyle: {
        flex: 1,
        height: 45,
        justifyContent: 'center',
        borderColor: "#ddd",
        // backgroundColor: '#f60'
    },
    btnView: {
        height: 45,
        margin: 10,
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
    },
    btnName: {
        fontSize: 14,
        color: '#333',
    }
});
