/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    DeviceEventEmitter, RefreshControl
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'
import Setting from "./setting";
import ActionSheet from 'react-native-actionsheet'

// 弹窗
const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 0;
const options = ['取消', '上午', '下午', '全天'];
const title = '日期管理';
export default class User extends Component {

    constructor(props) {
        super(props);
        this.state = {
            money_total: 0,
            clear_total: 0,
            new_profit: 0,
            fund_list: [],
            is_investor: -10,//为下一个页面传值
            investor_msg: '',
            back_msg: '',
            is_risk: -10,
            is_bank_card: -10,
            is_modify_truename: -10,
            is_special_investor: -10,
            is_pay_auth: -10,
            is_pay_auth_msg: '',
            time: '',
            head_img: '',
            nickname: '',
            user_level: '',
            isRefreshing: false,
        }
    }

    componentDidMount() {
        this.requestDataSource();
        DeviceEventEmitter.addListener('customName', this.callBack);
    }

    componentWillUnmount() {
        let timers = [this.timer];
        ClearTimer(timers);
        DeviceEventEmitter.removeListener('customName');
    }

    requestDataSource = async () => {
        let url = ServicesApi.get_member_info;
        let result = await Services.Get(url);
        if (result) {
            if (result.code == 1) {
                this.setState({
                    nickname: result.data.nickname || '',
                    head_img: result.data.img_url || '',
                    user_level: result.data.user_level || '',
                });
            }
        }
    };

    onRefresh = () => {
        this.setState({
            isRefreshing: true,
        });
        this.requestDataSource();
        this.timer = setTimeout(() => {
            this.setState({
                isRefreshing: false,
            });
        }, 1000);
    }

    callBack = (params) => {
        this.forceUpdate()
    }


    onBack = () => {
        this.props.navigation.goBack();
    }


    toWebview = (title, link, compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            title: title,
            link: link,
        })
    }

    onPushNavigator = (compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            money_total: this.state.money_total,
            clear_total: this.state.clear_total,
            new_profit: this.state.new_profit,
            fund_list: this.state.fund_list,
            is_investor: this.state.is_investor,
            investor_msg: this.state.investor_msg,
            back_msg: this.state.back_msg,
            is_risk: this.state.is_risk,
            is_bank_card: this.state.is_bank_card,
            is_modify_truename: this.state.is_modify_truename,
            is_special_investor: this.state.is_special_investor,
            is_pay_auth: this.state.is_pay_auth,
            is_pay_auth_msg: this.state.is_pay_auth_msg,
            onCallBack: () => {
                // this.loadNetData();
            }
        })
    }

    render() {
        let {head_img, nickname, user_level, isRefreshing} = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    style={{backgroundColor: GlobalStyles.themeColor}}
                    leftButton={UtilsView.getLeftSetButton(() => this.onPushNavigator("Setting"))}
                    rightButton={UtilsView.getRightXiaoxiButton(() => this.onPushNavigator('Xiaoxi'))}
                />
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            title='Loading...'
                            refreshing={isRefreshing}
                            onRefresh={this.onRefresh}
                            tintColor={GlobalStyles.themeColor}
                            colors={[GlobalStyles.themeColor]}
                            progressBackgroundColor="#fff"
                        />
                    }
                >
                    <View style={styles.userbg}>


                        <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 20, marginTop: 10}}>
                            {head_img ?
                                <Image source={{uri: head_img}} style={styles.userPhotoIcon}/>
                                :
                                <Image source={Images.icon_user_touxiang}
                                       style={styles.userPhotoIcon}/>
                            }
                            <View>
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={{marginLeft: 20, color: 'white'}}>{nickname}</Text>
                                </View>
                                <View style={{flexDirection: 'row', marginTop: 15}}>
                                    <Text style={{marginLeft: 20, color: 'white'}}>等级:</Text>
                                    <Text style={{marginLeft: 10, color: 'white'}}>{user_level}</Text>
                                </View>
                            </View>
                        </View>

                    </View>

                    <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                        <TouchableOpacity onPress={() => {
                            this.onPushNavigator('Huiyuan')
                        }} style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistleft}>
                                <Image source={Images.icon_user_huiyuan}
                                       style={GlobalStyles.usericon}/>
                            </View>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>会员中心</Text>
                                <Image source={Images.icon_user_arrow}
                                       style={GlobalStyles.userlistmore}/>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            this.onPushNavigator('Zichan')
                        }} style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistleft}>
                                <Image source={Images.icon_user_zichan}
                                       style={GlobalStyles.usericon}/>
                            </View>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>我的资产</Text>
                                <Image source={Images.icon_user_arrow}
                                       style={GlobalStyles.userlistmore}/>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {
                            this.onPushNavigator('OrderManage', {page_flag: 'second'})
                        }} style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistleft}>
                                <Image source={Images.icon_user_order}
                                       style={GlobalStyles.usericon}/>
                            </View>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>订单管理</Text>
                                <Image source={Images.icon_user_arrow}
                                       style={GlobalStyles.userlistmore}/>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {
                            this.onPushNavigator("DateManage")
                        }} style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistleft}>
                                <Image source={Images.icon_user_zichan}
                                       style={GlobalStyles.usericon}/>
                            </View>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>日期管理</Text>
                                <Text style={styles.userlistRightText}>{this.state.time}</Text>
                                <Image source={Images.icon_user_arrow}
                                       style={GlobalStyles.userlistmore}/>
                            </View>
                        </TouchableOpacity>

                        <ActionSheet
                            ref={o => this.ActionSheet = o}
                            title={title}
                            options={options}
                            cancelButtonIndex={CANCEL_INDEX}
                            destructiveButtonIndex={DESTRUCTIVE_INDEX}
                            onPress={this.handlePress}
                        />
                    </View>

                    <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                        <TouchableOpacity onPress={() => {
                            this.onPushNavigator('Shoucang')
                        }} style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistleft}>
                                <Image source={Images.icon_user_shoucang}
                                       style={GlobalStyles.usericon}/>
                            </View>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>我的收藏</Text>
                                <Image source={Images.icon_user_arrow}
                                       style={GlobalStyles.userlistmore}/>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            this.onPushNavigator('Setting')
                        }} style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistleft}>
                                <Image source={Images.icon_user_guanyu}
                                       style={GlobalStyles.usericon}/>
                            </View>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>设置</Text>
                                <Image source={Images.icon_user_arrow}
                                       style={GlobalStyles.userlistmore}/>
                            </View>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </View>
        );
    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    handlePress = (i) => {
        if (i == 1) {
            this.setState({
                time: '上午'
            })
            // toastShort("第一个")
        } else if (i == 2) {
            this.setState({
                time: '下午'
            })
            // toastShort("第二个")
        } else if (i == 3) {
            this.setState({
                time: '全天'
            })
            // toastShort("第三个")
        }

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    userbg: {
        backgroundColor: GlobalStyles.themeColor,
        height: 100,
    },
    userInfo: {
        height: 60,
        position: 'absolute',
        bottom: 30,
        left: 15,

    },
    userPhotoIcon: {
        width: 66,
        height: 66,
        borderRadius: 33
    },
    userMessage: {
        marginLeft: 10,
        height: 60
    },
    username: {
        color: '#fff',
        fontSize: 15,
        fontWeight: 'bold',
        lineHeight: 22,
        marginTop: 4
    },
    usertouxian: {
        backgroundColor: '#fff',
        height: 20,
        borderRadius: 10,
        paddingLeft: 5,
        paddingRight: 5,
        marginTop: 5
    },
    userv: {
        textAlign: 'center',
        borderRadius: 8,
        height: 16,
        position: 'relative',
        marginRight: 2,
    },
    uservicon: {
        width: 10,
        height: 10,
        position: 'absolute',
        top: 3,
        left: 3,
    },
    uservtext: {
        color: GlobalStyles.themeColor,
        fontSize: 12
    },
    userBigIcon: {
        width: 45,
        height: 45,
    },
    userBigText: {
        color: '#666',
        fontSize: 13,
        lineHeight: 30,
        textAlign: 'center',

    },
    userMid: {
        backgroundColor: '#fff',
        padding: 15,
        paddingBottom: 5,
        paddingTop: 12
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
});
