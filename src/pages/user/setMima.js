/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class SetMima extends Component {

    constructor(props) {
        super(props);
        this.state =  {
            user: '',
            oldpassword: '',
            password: '',
            rePassword: '',
        }
    }

    componentDidMount(){
        // this.getUserData();
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    loadNetData = () => {
        
    }

    submit = () => {
        
        let { oldpassword, password, rePassword } = this.state;

        if (!oldpassword) {
            toastShort('请输入原密码');
            return;
        }
        if (!password) {
            toastShort('请输入新密码');
            return;
        }
        if (!rePassword) {
            toastShort('请确认新密码');
            return;
        }
        if (password !== rePassword) {
            toastShort('两次密码输入不一致，请重新输入！');
            return;
        }

        let url = ServicesApi.changepass;
        let data = {
            password: password,
            old_password: oldpassword,
            repassword:rePassword
        }
        
        Services.Post(url, data)
            .then( result => {
                // console.log(result);
                if (result && result.code == 1) {
                    this.doLogOut();
                }else{
                    toastShort(result.msg)
                }
            })
            .catch( error => {
                // console.log(error)
            })
    }

    doLogOut = () => {
        this.cleanUserInfo();
        this.timer = setTimeout(() => {
            RouterHelper.reset('', 'Login');
        }, 1000);
    }

    cleanUserInfo = () => {
        global.token = '';
        StorageManager.remove(Constant.USER_INFO_KEY);
    }

    render(){
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = {'修改密码'}
                />
                <KeyboardAwareScrollView>
                    <View style={[GlobalStyles.mcell, {marginTop: 0, borderTopWidth: 10, borderTopColor: GlobalStyles.bgColor}]}>
                        <View style={GlobalStyles.cellItem}>
                            <Text style={[GlobalStyles.cellLeft, {width: 100}]}>原密码</Text>
                            <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
                                <TextInput
                                    placeholder={'请输入原密码'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            oldpassword: text
                                        })
                                    }}
                                    style={[GlobalStyles.cellInput,]} 
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.cellItem}>
                            <Text style={[GlobalStyles.cellLeft, {width: 100}]}>新密码</Text>
                            <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
                                <TextInput
                                    placeholder={'请输入新密码'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            password: text
                                        })
                                    }}
                                    style={[GlobalStyles.cellInput,]} 
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.cellItem}>
                            <Text style={[GlobalStyles.cellLeft, {width: 100}]}>确认新密码</Text>
                            <View style={[GlobalStyles.cellRight,GlobalStyles.textRight]}>
                                <TextInput
                                    placeholder={'请确认新密码'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            rePassword: text
                                        })
                                    }}
                                    style={[GlobalStyles.cellInput,]} 
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                    </View>

                    <TouchableOpacity onPress={()=>this.submit()} style={[GlobalStyles.submit, {marginBottom: 15, marginTop: 10}]}>
                        <View style={GlobalStyles.btn}>
                            <Text style={GlobalStyles.btna}>提交修改</Text>   
                        </View>
                    </TouchableOpacity>

                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    scrollView: {

    }
});
