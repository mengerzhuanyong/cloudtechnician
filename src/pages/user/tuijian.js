/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class Tuijian extends Component {

    constructor(props) {
        super(props);
        this.state =  {
            reg_count: '',
            bank_count: '',
            first_money: '',
            total_person_price: '',
            person_price: '',
        }
    }

    async componentWillMount(){
        try {
            await this.loadNetData();
        } catch (error) {
            // // console.log(error);
        }

    }

    componentDidMount(){
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    loadNetData = () => {
        let url = ServicesApi.get_team_info;
        let data = {
            member_id: global.user.userData.id,
            token: global.user.userData.token
        }

        Services.Post(url, data)
            .then( result => {
                if (result && result.code == 1) {
                    // console.log(result.data);
                    this.setState({
                        reg_count: result.data.reg_count,
                        bank_count: result.data.bank_count,
                        first_money: result.data.first_money,
                        total_person_price: result.data.total_person_price,
                        person_price: result.data.person_price,
                    })
                }else{
                    toastShort(result.msg);
                }
            })
            .catch( error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }

    render(){
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = {'推荐'}
                />
                <View style={GlobalStyles.userNeiTop}>
                    <Text style={GlobalStyles.userNeiTopline1}>累计人脉价值</Text>
                    <Text style={GlobalStyles.userNeiTopline2}>{this.state.total_person_price}</Text>
                    <Text style={GlobalStyles.userNeiTopline3}>本月人脉价值：{this.state.person_price}</Text>
                </View>

                <View style={[GlobalStyles.whiteModule, {marginTop: 0}]}>
                    <TouchableOpacity onPress={() => {this.onPushNavigator('TuijianZhuce')}} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_user_huiyuan} style={GlobalStyles.usericon} />
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>推荐注册人数</Text>
                            <Text style={styles.userlistRightText}>{this.state.reg_count}</Text>
                            <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {this.onPushNavigator('TuijianBangka')}} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_user_zichan} style={GlobalStyles.usericon} />
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>银行卡绑定人数</Text>
                            <Text style={styles.userlistRightText}>{this.state.bank_count}</Text>
                            <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {this.onPushNavigator('TuijianShoutou')}} style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_user_shangcheng} style={GlobalStyles.usericon} />
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>被邀请人首投金额</Text>
                            <Text style={styles.userlistRightText}>{this.state.first_money}</Text>
                            <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                        </View>
                    </TouchableOpacity>    
                </View>

                <View style={[GlobalStyles.whiteModule, {marginTop: 0, paddingBottom: 10, paddingTop: 10, backgroundColor: GlobalStyles.bgColor}]}>
                    <TouchableOpacity onPress={() => {this.onPushNavigator('Erweima')}} style={[GlobalStyles.userlist, {backgroundColor: '#fff'}]}>
                        <View style={GlobalStyles.userlistleft}>
                            <Image source={Images.icon_user_huiyuan} style={GlobalStyles.usericon} />
                        </View>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>推荐给好友</Text>
                            <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: GlobalStyles.themeColor,
    },
});
