/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class Tuijian extends Component {

    constructor(props) {
        super(props);
        this.state =  {
            info_img: '',
            qrcode: ''
        }
    }

    async componentWillMount(){
        try {
            await this.loadNetData();
        } catch (error) {
            // // console.log(error);
        }

    }

    componentDidMount(){
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    loadNetData = () => {
        let url = ServicesApi.tuiguang;
        let data = {
            member_id: global.user.userData.id
        }
        Services.Post(url, data)
            .then( result => {
                // console.log(result);
                this.setState({
                    info_img: result.data.info_img,
                    qrcode: result.data.qrcode
                })
            })
            .catch( error => {
                // console.log('退出失败，请重试！', error);
            })
    }

    render(){
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = {'推荐好友'}
                />
                <ScrollView>
                    <View style={styles.tjTop}>
                        {this.state.info_img ? <Image source={{uri: this.state.info_img}} style={styles.info_img} /> : null }
                        {this.state.qrcode ? <Image source={{uri: this.state.qrcode}} style={styles.qrcode} /> : null }
                    </View>

                    <View style={[styles.tjBot, GlobalStyles.hide]}>
                        <TouchableOpacity onPress={() => {}} style={[GlobalStyles.submit, {marginTop: 0}]}>
                            <Text style={GlobalStyles.btna}>已推荐列表</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {}} style={[GlobalStyles.submit, {marginTop: 0}]}>
                            <Text style={GlobalStyles.btna}>推荐给好友</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                    

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    tjTop: {

    },
    info_img: {
        width: GlobalStyles.width,
        height: GlobalStyles.width*1057/779
    },
    qrcode: {
        width: GlobalStyles.width/1.5,
        height: GlobalStyles.width/1.5,
        marginTop: 30,
        marginLeft: (GlobalStyles.width - GlobalStyles.width/1.5 - 30)/2
    },
    tjBot: {

    },
});
