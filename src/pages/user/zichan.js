/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar, RefreshControl
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'

import JifenQuanbu from './jifenQuanbu'
import RouterHelper from "../../routers/RouterHelper";

export default class Zichan extends Component {

    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        this.state = {
            money_total: params.money_total,
            clear_total: params.clear_total,
            new_profit: params.new_profit,
            integral_total: '',
            integral_new: '',
            list: [],
            all_income: '',
            cash_income: '',
            now_income: '',
            all_page: '',
            detailstate: 1,
            withdrawlist: [],
            refreshing: false,
        }
    }

    componentDidMount() {
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.state.params.onCallBack();
        this.props.navigation.goBack();
    };

    componentWillUnmount() {
        this.onBack();
    }

    onRefresh = (updateState = true) => {
        updateState && this.setState({refreshing: true});
        this.loadNetData();
        this.loadWithData();
        this.timer1 = setTimeout(() => {
            updateState && this.setState({refreshing: false});
        }, 1000);
    };

    loadNetData = () => {
        let url = ServicesApi.getRecentmemberIncome;
        Services.Get(url, true)
            .then(result => {
                if (result && result.code == 1) {
                    this.setState({
                        list: result.data.list,
                        all_income: result.data.all_income,
                        cash_income: result.data.cash_income,
                        now_income: result.data.now_income,
                        all_page: result.data.all_page,
                    });
                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })

    };

    loadWithData = () => {

        let url = ServicesApi.getRecentWithdrawlist;
        Services.Get(url, true)
            .then(result => {
                if (result && result.code == 1) {
                    // console.log(result);
                    this.setState({
                        withdrawlist: result.data.list,
                    });
                    // toastShort(result.msg);
                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })

    };

    onPushNavigator = (pageTitle, component, params = {}) => {
        params = {
            onCallBack: () => this.onRefresh(false),
            ...params,
        }
        RouterHelper.navigate(pageTitle, component, params);
    };

    onPushToJijin = (component, fund_id, fund_name) => {
        const {navigate} = this.props.navigation;
        navigate(component, {
            fund_id: fund_id,
            fund_name: fund_name,
        })
    };


    zichanlist = (data) => {
        let investList = [];
        if (data.length < 1) {
            let empty = <View style={GlobalStyles.emptyComponentView} key={'emptyEntry'}>
                <Text style={GlobalStyles.emptyText}>暂无相关信息</Text>
            </View>
            investList.push(empty);
        }
        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            let investItem = (
                <TouchableOpacity
                    key={'item_' + i}
                    style={styles.accountDetailItemView}
                    onPress={() => this.onPushNavigator('订单详情', 'OrderDetail', {
                        id: item.id,
                        page_flag: 'property',
                        status: item.status,
                    })}
                >
                    <Text
                        style={[styles.accountDetailItemCon, styles.accountDetailItemConCur]}>{item.order_sn}</Text>
                    <Text style={[styles.accountDetailItemCon,]}>¥ {item.money}</Text>
                    <Text style={[styles.accountDetailItemCon,]}>{item.create_time}</Text>
                </TouchableOpacity>
            );
            investList.push(investItem);
        }
        return investList;

    };

    tixianliebiao = (data) => {
        let investList = [];
        if (data.length < 1) {
            let empty = <View style={GlobalStyles.emptyComponentView} key={'emptyWithDraw'}>
                <Text style={GlobalStyles.emptyText}>暂无相关信息</Text>
            </View>
            investList.push(empty);
        }
        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            let investItem = (
                <View
                    key={'item_' + i}
                    style={styles.accountDetailItemView}
                >
                    <Text style={[styles.accountDetailItemCon, styles.accountDetailItemConCur]}>¥ {item.money}</Text>
                    <Text style={[styles.accountDetailItemCon,]}>{item.create_time}</Text>
                    <Text style={[styles.accountDetailItemCon,]}>{item.status_name}</Text>
                </View>
            );
            investList.push(investItem);
        }
        return investList;

    };

    render() {
        let {
            all_income,
            cash_income,
            now_income,
            detailstate,
            refreshing,
        } = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'我的资产'}
                />
                <ScrollView
                    style={styles.content}
                    refreshControl={
                        <RefreshControl
                            title='Loading...'
                            refreshing={refreshing}
                            onRefresh={this.onRefresh}
                            tintColor="#0398ff"
                            colors={['#0398ff']}
                            progressBackgroundColor="#fff"
                        />
                    }
                >
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: '#617995',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                        <View style={styles.accountContent}>
                            <View style={styles.accountItemView}>
                                <Text style={[styles.accountItemTitle]}>总收入:</Text>
                                <Text style={[styles.accountItemCon]}>¥ {all_income}</Text>
                            </View>
                            <View style={styles.accountItemView}>
                                <Text style={styles.accountItemTitle}>已提现:</Text>
                                <Text style={styles.accountItemCon}>¥ {now_income}</Text>
                            </View>
                            <View style={styles.accountItemView}>
                                <Text style={styles.accountItemTitle}>余&nbsp;&nbsp;&nbsp;&nbsp;额:</Text>
                                <Text style={styles.accountItemCon}>¥ {cash_income}</Text>
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => {
                                this.onPushNavigator('提现', 'Tixian');
                            }}
                        >
                            <View style={{marginRight: 15}}>
                                <Text style={{
                                    color: '#fff',
                                    textAlign: 'center',
                                    paddingVertical: 8,
                                    paddingHorizontal: 16,
                                    borderRadius: 5,
                                    backgroundColor: '#fccd55'
                                }}>提现</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.middle, styles.btnViewStyle]}>
                        <TouchableOpacity
                            style={styles.btnItemView}
                            onPress={() => {
                                this.setState({detailstate: 1});
                                this.loadNetData();
                            }}
                        >
                            <Text style={[styles.btnItemName, detailstate === 1 && styles.btnItemNameCur]}>收入明细</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.btnItemView}
                            onPress={() => {
                                this.setState({detailstate: 2});
                                this.loadWithData();
                            }}
                        >
                            <Text style={[styles.btnItemName, detailstate === 2 && styles.btnItemNameCur]}>提现明细</Text>
                        </TouchableOpacity>
                    </View>
                    {this.state.detailstate === 1 ?
                        <View style={{flexDirection: "column"}}>
                            <View style={styles.accountDetailItemView}>
                                <Text style={[styles.accountDetailItemCon, styles.accountDetailItemConCur]}>订单</Text>
                                <Text style={[styles.accountDetailItemCon,]}>收入</Text>
                                <Text style={[styles.accountDetailItemCon,]}>时间</Text>
                            </View>
                            <View style={styles.accountListContent}>
                                {this.zichanlist(this.state.list)}
                            </View>
                        </View>
                        :
                        <View style={{flexDirection: "column"}}>
                            <View style={styles.accountDetailItemView}>
                                <Text style={[styles.accountDetailItemCon, styles.accountDetailItemConCur]}>金额</Text>
                                <Text style={[styles.accountDetailItemCon,]}>时间</Text>
                                <Text style={[styles.accountDetailItemCon,]}>状态</Text>
                            </View>
                            <View style={styles.accountListContent}>
                                {this.tixianliebiao(this.state.withdrawlist)}
                            </View>
                        </View>
                    }

                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    content: {
        flex: 1,
    },
    titleIcon: {
        width: 27,
        height: 27,
        marginRight: 10
    },
    myFundItem: {
        borderColor: '#d9d9d9',
        borderWidth: 1,
        borderRadius: 8,
        height: 45,
        marginBottom: 10,
        paddingLeft: 15,
        paddingRight: 15,
    },
    myFundText: {
        color: GlobalStyles.themeColor,
        fontSize: 16
    },
    lookIco: {
        width: 15,
        height: 15,
    },
    userlistRightText: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        position: 'absolute',
        right: 20,
        color: '#ffffff',
        backgroundColor: '#ffcc52',
        borderRadius: 2
    },
    top: {
        backgroundColor: '#617995',
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
    },
    toptext: {
        color: '#ffffff',
        fontSize: 15,
    },
    topbigText: {
        color: '#ffffff',
        fontSize: 35,
        textAlign: 'center'
    },
    middle: {},

    accountContent: {
        paddingVertical: 20,
        paddingHorizontal: 15,
    },
    accountItemView: {
        marginVertical: 8,
        flexDirection: 'row',
        alignItems: 'center',
    },
    accountItemTitle: {
        fontSize: 16,
        color: '#fff',
        marginRight: 10,
        fontWeight: '700',
    },
    accountItemTitleCur: {
        fontSize: 18,
        fontWeight: '700',
    },
    accountItemCon: {
        fontSize: 15,
        color: '#fff',
        fontWeight: '700',
    },
    accountItemConCur: {
        fontSize: 18,
        fontWeight: '700',
    },

    btnViewStyle: {
        height: 45,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        justifyContent: 'space-between',
    },
    btnItemView: {
        height: 45,
        justifyContent: 'center',
    },
    btnItemName: {
        fontSize: 15,
        color: '#666',
    },
    btnItemNameCur: {
        color: '#000',
    },
    accountListContent: {},
    accountDetailItemView: {
        height: 45,
        flexDirection: 'row',
        alignItems: "center",
        paddingHorizontal: 10,
        borderColor: '#dbdbdb',
        backgroundColor: '#fff',
        justifyContent: 'space-around',
        borderTopWidth: GlobalStyles.minPixel,
    },
    accountDetailItemCon: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    accountDetailItemConCur: {
        flex: 2,
    },
});
