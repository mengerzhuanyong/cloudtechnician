/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    DeviceEventEmitter
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'
import SpinnerLoading from '../../components/loading/SpinnerLoading'

import SYImagePicker from 'react-native-syan-image-picker'
import ActionsManager from '../../utils/area/ActionsManager'

const options = {
    imageCount: 1,             // 最大选择图片数目，默认6
    isCamera: true,            // 是否允许用户在内部拍照，默认true
    isCrop: false,             // 是否允许裁剪，默认false
    CropW: ~~(GlobalStyles.width * 0.6),    // 裁剪宽度，默认屏幕宽度60%
    CropH: ~~(GlobalStyles.width * 0.6),    // 裁剪高度，默认屏幕宽度60%
    isGif: false,              // 是否允许选择GIF，默认false，暂无回调GIF数据
    showCropCircle: false,     // 是否显示圆形裁剪区域，默认false
    circleCropRadius: GlobalStyles.width / 2,  // 圆形裁剪半径，默认屏幕宽度一半
    showCropFrame: true,       // 是否显示裁剪区域，默认true
    showCropGrid: false,       // 是否隐藏裁剪区域网格，默认false
    quality: 50,                // 压缩质量
    enableBase64: true
};

export default class Gerenxinxi extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nickname: '',
            img_url: '',
            telephone: '',
            wx_number: '',
            true_name: '',
            province: '',
            city: '',
            district: '',
            address: '',
            uploading: false,
        }
    }

    componentDidMount() {
        this.loadNetData();
    }

    onBack = () => {
        const {goBack, state} = this.props.navigation;
        state.params && state.params.onCallBack && state.params.onCallBack();
        goBack();
        DeviceEventEmitter.emit('customName', {})
    }

    componentWillUnmount() {
        let timers = [this.timer];
        ClearTimer(timers);
    }

    loadNetData = () => {
        let url = ServicesApi.get_member_info;
        Services.Get(url)
            .then(result => {
                if (result && result.code == 1) {
                    this.setState({
                        nickname: result.data.nickname,
                        img_url: result.data.img_url,
                        telephone: result.data.telephone,
                        wx_number: result.data.wx_number,
                        true_name: result.data.true_name,
                        province: result.data.province,
                        city: result.data.city,
                        district: result.data.district,
                        address: result.data.province + '-' + result.data.city + '-' + result.data.district,
                    })
                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    onPushNavigator = (compent, back_msg) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            back_msg: back_msg,
        })
    }

    submit = () => {
        let {nickname, img_url, telephone, wx_number, true_name, province, city, district, address} = this.state;
        let url = ServicesApi.put_member_info;
        let data = {
            nickname,
            img_url,
            wx_number,
            address,
            true_name,
        };

        Services.Post(url, data, true)
            .then(result => {
                if (result && result.code == 1) {
                    this.timer = setTimeout(() => {
                        RouterHelper.goBack();
                    }, 1000);
                    toastShort(result.msg);
                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    setPhoto = () => {
        SYImagePicker.showImagePicker(options, (err, selectedPhotos) => {
            if (err) {
                // 取消选择
                // console.log('您已取消选择');
                return;
            }
            // 选择成功
            // console.log('您已选择成功');
            // console.log(selectedPhotos);
            this.photoResult(selectedPhotos);
        })
    }

    photoResult = (datas) => {
        let photoList = [],
            selectedPhotos = datas;

        // 图片base64转码
        let url = ServicesApi.base64,
            thisSelectedPhoto = selectedPhotos[0].base64;
        let data = {
            base64: thisSelectedPhoto
        };
        this.setState({uploading: true});
        Services.Post(url, data)
            .then(result => {
                if (result && result.code == 1) {
                    this.setState({
                        img_url: result.data.img_url
                    })
                } else {
                    toastShort(result.msg);
                }
                this.setState({uploading: false});
            })
            .catch(error => {
                this.setState({uploading: false});
                toastShort('服务器请求失败，请稍后重试！');
            })

    }

    chooseArea = () => {
        ActionsManager.showArea((info) => {
            this.setState({
                address: info
            })
        })
    }

    renderUserAvatar = (uploading, avatar) => {
        if (uploading) {
            return (
                <View style={styles.userPhoto}>
                    <SpinnerLoading size={20} />
                </View>
            );
        } else {
            return (
                <Image
                    style={styles.userPhoto}
                    source={avatar ? {uri: avatar} : Images.icon_user_touxiang}
                />
            )
        }
    }

    render() {
        let {uploading, nickname, img_url, telephone, wx_number, true_name, province, city, district, address} = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'个人信息'}
                />
                <ScrollView keyboardShouldPersistTaps={'handled'}>
                    <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setPhoto()
                            }}
                            style={GlobalStyles.userlist}
                        >
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>头像</Text>
                                {this.renderUserAvatar(uploading, img_url)}
                                <Image source={Images.icon_user_arrow} style={GlobalStyles.userlistmore}/>
                            </View>
                        </TouchableOpacity>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>昵称</Text>
                                <TextInput
                                    defaultValue={nickname}
                                    placeholder={'请输入昵称'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            nickname: text,
                                        })
                                    }}
                                    style={[GlobalStyles.userlistRightText, styles.rightTextCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>手机号</Text>
                                <Text
                                    style={[GlobalStyles.userlistRightText, styles.rightTextCon, {lineHeight: 45}]}>{telephone}</Text>
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>微信号</Text>
                                <TextInput
                                    defaultValue={wx_number}
                                    placeholder={'请输入微信号'}
                                    placeholderTextColor={'#999'}
                                    // value={wx_number}
                                    onChangeText={(text) => {
                                        this.setState({
                                            wx_number: text,
                                        })
                                    }}
                                    style={[GlobalStyles.userlistRightText, styles.rightTextCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>真实姓名</Text>
                                <TextInput
                                    defaultValue={true_name}
                                    placeholder={'请输入真实姓名'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            true_name: text,
                                        })
                                    }}
                                    style={[GlobalStyles.userlistRightText, styles.rightTextCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>

                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>所在区域</Text>
                                <TouchableOpacity
                                    onPress={() => this.chooseArea()}
                                    style={[styles.rightTextView,]}
                                >
                                    <Text
                                        style={[GlobalStyles.userlistRightText, styles.rightTextCon, {lineHeight: 45}]}>{address && address !== '--' ? address : "请选择业务区域"}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => {
                        this.submit()
                    }} style={GlobalStyles.submit}>
                        <Text style={GlobalStyles.btna}>保存信息</Text>
                    </TouchableOpacity>
                </ScrollView>

            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
    rightTextView: {
        flex: 1,
        height: 45,
        justifyContent: 'center',
    },
    rightTextCon: {
        flex: 1,
        height: 45,
        textAlign: 'right',
        // backgroundColor: '#123'
    },
    userPhoto: {
        width: 32,
        height: 32,
        position: 'absolute',
        right: 20,
        borderRadius: 16,
    }
});
