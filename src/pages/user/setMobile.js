/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'

export default class SetMobile extends Component {

    constructor(props) {
        super(props);
        this.state =  {}
    }

    componentDidMount(){
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    loadNetData = () => {
        
    }

    render(){
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = {'修改手机号'}
                />
                <Text style={{padding: 15, }}>即将上线，敬请期待！</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
});
