/**
 * 云技师 - 提现
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    DeviceEventEmitter
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'

import SYImagePicker from 'react-native-syan-image-picker'
import Yinhangka from "../fund/yinhangka";

const options = {
    imageCount: 1,             // 最大选择图片数目，默认6
    isCamera: true,            // 是否允许用户在内部拍照，默认true
    isCrop: false,             // 是否允许裁剪，默认false
    CropW: ~~(GlobalStyles.width * 0.6),    // 裁剪宽度，默认屏幕宽度60%
    CropH: ~~(GlobalStyles.width * 0.6),    // 裁剪高度，默认屏幕宽度60%
    isGif: false,              // 是否允许选择GIF，默认false，暂无回调GIF数据
    showCropCircle: false,     // 是否显示圆形裁剪区域，默认false
    circleCropRadius: GlobalStyles.width / 2,  // 圆形裁剪半径，默认屏幕宽度一半
    showCropFrame: true,       // 是否显示裁剪区域，默认true
    showCropGrid: false,       // 是否隐藏裁剪区域网格，默认false
    quality: 50,                // 压缩质量
    enableBase64: true
};

export default class Tixian extends Component {

    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        this.state = {
            withdrawlist: [],
            money: '',
            cardnumber: '',
            id: ''
        }
    }

    componentDidMount() {
        this.loadNetData();
        // this.requestDataSource()
        DeviceEventEmitter.addListener('tixianka', this.callBack)
    }

    callBack = (params) => {
        this.setState({
            cardnumber: params.num,
            id: params.id,
        })
    }

    onBack = () => {
        let {state, goBack} = this.props.navigation;
        state.params && state.params.onCallBack && state.params.onCallBack();
        goBack && goBack();
    }

    componentWillUnmount() {
        this.onBack();
        DeviceEventEmitter.removeListener('tixianka')
    }


    onPushNavigator = (compent, back_msg) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            fromtixianpage: back_msg,
        })
    }


    submit = () => {
        let url = ServicesApi.addWithdraw;
        let data = {
            money: this.state.money,
            bank_number: this.state.id,
        };

        Services.Post(url, data)
            .then(result => {
                if (result && result.code == 1) {
                    // console.log(result);
                    this.timer = setTimeout(() => {
                        this.onBack();
                    }, 1000);
                    toastShort(result.msg);
                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    loadNetData = () => {
        let url = ServicesApi.getRecentWithdrawlist;
        Services.Get(url)
            .then(result => {
                if (result && result.code == 1) {
                    // console.log(result);
                    this.setState({
                        withdrawlist: result.data.list,
                    })
                    if (result.data.list.length == 0) {
                        toastShort("暂无提现信息");
                    }
                    // toastShort(result.msg);
                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })

    };

    render() {
        let {cardnumber} = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'我的提现'}
                />

                <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                    <View style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>提现金额</Text>
                            <TextInput
                                placeholder={'请输入提现金额'}
                                onChangeText={(text) => {
                                    this.state.money = text;
                                }}
                                style={[GlobalStyles.cellInput, {textAlign: 'right'}]}
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                    </View>
                    <View style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>提现银行卡</Text>
                            <TouchableOpacity
                                style={{flex: 1, alignItems: 'flex-end'}}
                                onPress={() => {
                                    this.onPushNavigator('Yinhangka', '1')
                                }}
                            >
                                <Text style={GlobalStyles.userlistRightText}>{cardnumber ? cardnumber : '请选择'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <TouchableOpacity
                    onPress={() => {
                        this.submit()
                    }} style={GlobalStyles.submit}>
                    <Text style={GlobalStyles.btna}>提现</Text>
                </TouchableOpacity>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
    userPhoto: {
        width: 32,
        height: 32,
        position: 'absolute',
        right: 20,
        borderRadius: 16,
    },
    middle: {
        height: 30,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
    },
    backcolor: {
        backgroundColor: '#ffffff'
    }
});
