/**
 * 云技师 - 技能重新认证
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'

import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'

import SYImagePicker from "react-native-syan-image-picker";
import ActionSheet from 'react-native-actionsheet'
import {Button} from "teaset";
import SpinnerLoading from "../../components/loading/SpinnerLoading";

const options = {
    imageCount: 1,             // 最大选择图片数目，默认6
    isCamera: true,            // 是否允许用户在内部拍照，默认true
    isCrop: true,             // 是否允许裁剪，默认false
    CropW: ~~(GlobalStyles.width * 0.9),    // 裁剪宽度，默认屏幕宽度60%
    CropH: ~~(GlobalStyles.height * 0.6),    // 裁剪高度，默认屏幕宽度60%
    isGif: false,              // 是否允许选择GIF，默认false，暂无回调GIF数据
    showCropCircle: false,     // 是否显示圆形裁剪区域，默认false
    circleCropRadius: GlobalStyles.width / 2,  // 圆形裁剪半径，默认屏幕宽度一半
    showCropFrame: true,       // 是否显示裁剪区域，默认true
    showCropGrid: false,       // 是否隐藏裁剪区域网格，默认false
    quality: 50,                // 压缩质量
    enableBase64: true
};
// 弹窗
const DESTRUCTIVE_INDEX = 0;
const title = '技能分类';

export default class UserRetrial extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            skill_arr: [],
            selectedArr: [],
            imgurl1: '',
            imgurl2: '',
            status: -100,
            uploading: false,
            uploading2: false,
            photo: [],
            reason: '',
        }
    }

    componentDidMount() {
        this.loadNetData();
        // this.loadMySkill();
    }

    onBack = () => {
        const {goBack, state} = this.props.navigation;
        state.params && state.params.onCallBack && state.params.onCallBack();
        goBack();
    }
    componentWillUnmount() {
        let timers = [this.timer];
        ClearTimer(timers);
        this.onBack();
    }
    loadNetData = () => {
        let url = ServicesApi.getAllSkill;
        Services.Get(url)
            .then(result => {
                if (result && result.code == 1) {
                    this.setState({
                        ready: true,
                        skill_arr: result.data.list,
                    });
                }
            })
            .catch(error => {
                // Toast.toastShort('');
            })
    };
    loadMySkill = () => {
        let url = ServicesApi.getMySkill;
        Services.Get(url, true)
            .then(result => {
                if (result && result.code == 1) {
                    this.setState({
                        ready: true,
                        selectedArr: result.data.skill_list || [],
                        photo: result.data.album_list || [],
                        status: result.data.status,
                        reason: result.data.reason || '',
                    });
                }
            })
            .catch(error => {
                // Toast.toastShort('');
            })
    };
    onPushNavigator = (component) => {
        const {navigate} = this.props.navigation;
        navigate(component, {})
    };
    submit = () => {
        let {photo, selectedArr} = this.state;
        let url = ServicesApi.addSkill;
        let data = {
            album_list: photo,
            skill_arr: selectedArr,
        };
        Services.Post(url, data, true)
            .then(result => {
                // console.log(result.code);
                Toast.toastShort(result.msg);
                if (result && result.code == 1) {
                    this.timer = setTimeout(() => {
                        this.onBack();
                    }, 500);
                }
            })
            .catch(error => {
                Toast.toastShort('error');
            })

    }
    showActionSheet = () => {
        this.ActionSheet.show()
    }
    selectedSkill = (item) => {
        let {selectedArr} = this.state;
        let selectedItem = '[' + item.id + ']';
        item.selected = item.selected === 0 ? 1 : 0;

        if (item.selected === 1) {
            selectedArr.push(selectedItem);
        } else {
            selectedArr.splice(selectedArr.findIndex(i => i === selectedItem), 1);
        }
        // console.log(selectedArr);
        this.setState({
            selectedArr
        });
    };
    renderSkillItem = (data) => {
        let {selectedArr, status} = this.state;
        if (!data || data.length < 1) {
            return null;
        }
        let skillItems = data.map((item, index) => {
            let item_id = '[' + item.id + ']';
            let cur = selectedArr.indexOf(`${item_id}`);
            return (
                <Button
                    key={'skill_' + item.id}
                    title={item.name}
                    style={[styles.skillItemStyle, cur >= 0 && styles.skillItemStyleCur]}
                    titleStyle={[styles.skillItemTitleStyle, cur >= 0 && styles.skillItemTitleStyleCur]}
                    onPress={() => {
                        status === -100 && this.selectedSkill(item)
                    }}
                />
            );
        });
        return <View style={styles.skillItemView}>{skillItems}</View>;
    };

    renderBtnView = (status) => {
        return (
            <TouchableOpacity
                onPress={this.submit}
                style={GlobalStyles.submit}
            >
                <Text style={GlobalStyles.btna}>立即提交</Text>
            </TouchableOpacity>
        );
    };


    handleImage = () => {
        SYImagePicker.showImagePicker(options, (err, selectedPhotos) => {
            if (err) {
                // 取消选择
                // console.log('您已取消选择');
                return;
            }
            // 选择成功
            // console.log('您已选择成功');
            // console.log(selectedPhotos);
            let data = selectedPhotos[0].base64;
            this.uploadImage(data);
        })
    };

    uploadImage = (image) => {
        let url = ServicesApi.base64;
        let data = {
            base64: image,
        };
        this.setState({uploading: true});
        Services.Post(url, data)
            .then(result => {
                if (result && result.code === 1) {
                    let images = this.state.photo.concat(result.data.img_url);
                    this.setState({
                        photo: images,
                    });
                }
                this.setState({uploading: false});
            })
            .catch(error => {
                // console.log(error);
                // console.log(error);
                this.setState({uploading: false});
                Toast.toastShort('error');
            })
    };

    renderImagesView = (data) => {
        if (!data || data.length < 1) {
            return;
        }
        let images = data.map((obj, index) => {
            let imgUrl = obj.img_url ? obj.img_url : obj;
            return <Image key={index} source={{uri: imgUrl}} style={styles.articleBanner}/>;
        });
        return images;
    };

    renderUploadContent = (status, photo) => {
        return null
    };

    renderUploadBtnContent = (status) => {
        if (status > -100) {
            return null;
        }
        return (
            <TouchableOpacity
                style={[styles.articleBannerPlusBtnView]}
                onPress={() => {
                    this.handleImage(1, 360, 240)
                }}
            >
                <Image source={Images.picAdd} style={styles.articleBannerPlusIcon}/>
                <Text style={styles.plusBtnTips}>添加配图</Text>
            </TouchableOpacity>
        );
    };

    render() {
        let {skill_arr, selectedArr, imgurl1, imgurl2, status, uploading, uploading2, photo, reason, ready} = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'技能认证'}
                />
                {ready ?
                    <ScrollView style={[{marginTop: 10, backgroundColor: '#f5f5f5'}]}>
                        {this.renderSkillItem(skill_arr)}

                        <Text style={styles.contentTitle}>请上传证书</Text>
                        <View style={styles.photoView}>
                            <View style={[styles.photoViewContent,]}>
                                {photo.length > 0 && this.renderImagesView(photo)}
                                {uploading && <View style={styles.articleBannerPlusBtnView}>
                                    <SpinnerLoading />
                                </View>}
                                {this.renderUploadBtnContent(status, photo)}
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={this.submit}
                            style={GlobalStyles.submit}
                        >
                            <Text style={GlobalStyles.btna}>立即提交</Text>
                        </TouchableOpacity>
                    </ScrollView>
                    : <SpinnerLoading isVisible={!ready}/>
                }
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    pingguModule: {
        borderWidth: 1,
        borderColor: '#ececec',
        margin: 15,
        marginBottom: 10,
        borderRadius: 5,
        padding: 15

    },
    pingguItem: {
        display: 'none',

    },
    pingguItemActive: {
        display: 'flex'
    },
    pingguTitle: {
        color: GlobalStyles.themeColor,
        fontSize: 16,
        lineHeight: 22,
    },
    numItem: {},
    numNow: {},
    numXian: {},
    numAll: {},
    numSame: {
        color: GlobalStyles.themeColor,
        fontSize: 14,

    },
    picAdd: {
        display: 'flex',
        flexDirection: 'column',
        width: GlobalStyles.width - 60,
        marginRight: 30,
        marginLeft: 30,
        backgroundColor: '#ffffff',
        margin: 10,
        borderRadius: 5,
        minHeight: GlobalStyles.width / 4,
        borderWidth: 2,
        borderColor: "#eeeeee",
        alignItems: 'center',
        justifyContent: 'center',
    },
    chooseitem: {
        backgroundColor: '#fff'
    },

    skillItemView: {
        flex: 1,
        padding: 15,
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: '#fff',
    },
    skillItemStyle: {
        height: 30,
        borderRadius: 3,
        borderColor: '#ddd',
        marginHorizontal: 5,
        paddingHorizontal: 8,
        borderWidth: GlobalStyles.minPixel,
    },
    skillItemStyleCur: {
        backgroundColor: GlobalStyles.themeColor,
    },
    skillItemTitleStyle: {
        fontSize: 14,
        color: '#666',
    },
    skillItemTitleStyleCur: {
        color: '#fff',
    },
    contentTitle: {
        fontSize: 15,
        color: '#666',
        marginTop: 15,
        padding: 15,
        backgroundColor: '#fff',
    },
    userPhotoTips: {
        fontSize: 13,
        color: '#666',
        marginTop: 5,
    },
    uploadView: {
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    uploadImg: {
        flex: 1,
        minHeight: 150,
        resizeMode: 'contain',
        width: GlobalStyles.width - 60,
    },

    photoView: {
        paddingBottom: 10,
        backgroundColor: '#fff',
    },
    photoViewContent: {
        padding: 15,
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'space-between'
    },
    articleBannerPlusView: {
        padding: 15,
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    articleBannerPlusBtnView: {
        width: GlobalStyles.width / 4,
        height: GlobalStyles.width / 4,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#999',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
    },
    articleBanner: {
        width: GlobalStyles.width / 4,
        height: GlobalStyles.width / 4,
        borderWidth: 1,
        margin: 5,
        borderRadius: 8,
        borderColor: '#999',
        // backgroundColor: '#f60',
    },
    articleBannerPlusIcon: {
        width: 30,
        height: 30,
        tintColor: '#999',
    },
    plusBtnTips: {
        fontSize: 14,
        color: '#999',
        marginTop: 10,
    },
    reasonContent: {
        padding: 15,
    },
    reasonTitle: {
        fontSize: 15,
        color: '#f00',
    },
    reasonContext: {
        fontSize: 14,
        color: '#999',
        lineHeight: 20,
    },
});
