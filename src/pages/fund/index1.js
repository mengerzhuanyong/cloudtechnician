/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    FlatList,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'

import {PullPicker} from 'teaset'

import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'
import ActivityIndicatorItem from '../../components/common/ActivityIndicatorItem'
import FooterComponent from '../../components/common/footerComponent'
import EmptyComponent from '../../components/common/emptyComponent'
import CardItem from "../../components/item/CardItem";


export default class Fund extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            showFoot: 0,
            error: false,
            errorInfo: "",
            loadMore: false,
            refreshing: false,
            fundListData: [],
            order_type: 0,
            fund_type: 0,
            risk_level: 0,
            rate_start: '',
            rate_end: '',
            paixuShow: false,
            shaixuanShow: false,
            yinyingShow: false,
            desc: 'asc'
        }
    }

    /**
     * 初始化状态
     * @type {Boolean}
     */
    page = 0;
    totalPage = 1;
    loadMore = false;
    refreshing = false;

    async componentDidMount() {
        await this.dropLoadMore();
        setTimeout(() => {
            this.setState({
                ready: true,
                showFoot: 0 // 控制foot， 0：隐藏footer  1：已加载完成,没有更多数据   2 ：显示加载中
            })
        }, 0)
    }

    loadNetData = (page, num, desc) => {
        let url = ServicesApi.ArticleList + "/cate_id/" + num + "/page/" + page + "/desc/" + desc;
        // let data = {
        //     cate_id: num,
        //     page: page,
        //     desc:'asc'
        // }
        return Services.Get(url)
            .then(result => {
                // console.log(result);
                return result;
            })
            .catch(error => {
                // consoleLog('链接服务器出错，请稍后重试', error);
                this.setState({
                    ready: true,
                    error: true,
                    errorInfo: error
                })
            })
    }

    dropLoadMore = async () => {
        //如果是正在加载中或没有更多数据了，则返回
        if (this.state.showFoot != 0) {
            return;
        }
        if ((this.page != 1) && (this.page >= this.totalPage)) {
            return;
        } else {
            this.page++;
        }
        this.setState({
            showFoot: 2
        })
        let result = await this.loadNetData(this.page, this.state.order_type, this.state.desc);
        // // console.log(this.totalPage);
        this.totalPage = result.data.count;
        // // console.log(result);
        let foot = 0;
        if (this.page >= this.totalPage) {
            // // console.log(this.totalPage);
            foot = 1; //listView底部显示没有更多数据了
        }
        this.setState({
            showFoot: foot,
            fundListData: this.state.fundListData.concat(result.data.article_list)
        })
    }

    freshNetData = async (num) => {
        if (!num) {
            num = this.state.order_type;
        }
        let result = await this.loadNetData(1, num, this.state.desc);
        if (result && result.code == 1) {
            this.page = 1;
            this.setState({
                showFoot: 0
            })
            this.setState({
                fundListData: result.data.article_list
            })
        }
    }

    onPushNavigator = (compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {})
    }

    onPushToFundDetail = (compent, fund_id, title, datum_rate, buy_end_day, buy_end_money, time_limit, start_buy_money, end_people, is_collection) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            fund_id: fund_id,
            title: title,
            datum_rate: datum_rate,
            buy_end_day: buy_end_day,
            buy_end_money: buy_end_money,
            time_limit: time_limit,
            start_buy_money: start_buy_money,
            end_people: end_people,
            is_collection: is_collection,
            onCallBack: () => {
                this.freshNetData(this.state.order_type);
            }
        })
    }

    renderCompanyItem = ({item}) => {
        let content_url = item.article_url;
        let content_id = item.article_id;
        let is_collection = item.is_collection;
        return (
            <CardItem
                item={item}
                onCallBack={() => this.freshNetData(this.state.order_type)}
                onPress={() => RouterHelper.navigate(item.title, 'NewsWebDetail', {link: item.article_url})}
                {...this.props}
            />
        );
    }

    renderHeaderView = () => {
        return (
            <View style={styles.shopListViewTitle}/>
        )
    }

    renderFooterView = () => {
        return <FooterComponent status={this.state.showFoot} {...this.props} />;
    }

    renderEmptyView = () => {
        return this.state.showFoot == 0 && <EmptyComponent emptyTips={'抱歉，没有符合条件的结果显示'}/>;
    }

    renderSeparator = () => {
        return <View style={GlobalStyles.horLine}/>;
    }

    //发送网络请求
    paixu = () => {
        // //发送网络请求
        // if (this.state.desc == "desc") {
        //     this.setState({
        //         desc: 'asc'
        //     })
        // } else {
        //     this.setState({
        //         desc: 'desc'
        //     })
        // }
        // this.freshNetData(this.state.order_type)
        let items = [
            {a: '按时间正序', b: 1},
            {a: '按时间倒序', b: 1},
            {a: '按阅读量正序', b: 1},
            {a: '按阅读量倒序', b: 1},
        ];
        // PullPicker.show(
        //     '排序',
        //     items,
        //     this.state.selectedIndex,
        //     (item, index) => {
        //         // console.log(item)
        //     },
        //     getItemText = (item, index) => {
        //         // console.log(item)
        //     }
        // );
        // return (
        //     <PullPicker.PullPickerView
        //         title={'排序'}
        //         items={items}
        //         selectedIndex={0}
        //         getItemText={(item, index) => {item.a}}
        //     />
        // )
    };

    toWebview = (link, content_id, is_collection, compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            link: link,
            content_id: content_id,
            is_collection: is_collection

        })
    }

    shaixuan = () => {
        let items = [
            '最新',
            '最热',
        ];
        PullPicker.show(
            '筛选',
            items,
            this.state.selectedIndex,
            (item, index) => this.setState({selectedIndex: index})
        );
    };

    yinying = () => {
        this.setState({
            paixuShow: false,
            shaixuanShow: false,
            yinyingShow: false,
        })
    }

    setPaixu = (num) => {
        this.setState({
            order_type: num,
            paixuShow: false,
            shaixuanShow: false,
            yinyingShow: false,
        })
        this.freshNetData(num);
    }


    submit = () => {
        this.setState({
            paixuShow: false,
            shaixuanShow: false,
            yinyingShow: false,
        })
        this.freshNetData(this.state.order_type);
    }

    render() {
        const {ready, error, refreshing, fundListData} = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'云社区'}
                    leftButton={null}
                />
                <View style={styles.topBar}>
                    <TouchableOpacity
                        onPress={() => {
                            this.paixu()
                        }}
                        style={styles.topLeft}
                    >
                        <Image source={Images.icon_paixu}
                               style={styles.topLeftIco}/>
                        <Text style={styles.topLeftText}>排序</Text>
                    </TouchableOpacity>
                    <View style={styles.shuxian}/>
                    <TouchableOpacity
                        onPress={() => {
                            this.shaixuan()
                        }}
                        style={styles.topLeft}
                    >
                        <Image source={Images.icon_shaixuan}
                               style={styles.topLeftIco}/>
                        <Text style={styles.topLeftText}>筛选</Text>
                    </TouchableOpacity>
                </View>

                {this.state.yinyingShow ?
                    <TouchableOpacity
                        onPress={() => {
                            this.yinying()
                        }}
                        style={styles.yinying}
                    /> : null}


                {this.funList(ready, error, refreshing, fundListData)}


            </View>
        );
    }

    funList = (ready, error, refreshing, fundListData) => {
        let data = [["C", "Java", "JavaScript"], ["Python", "Ruby"], ["Swift", "Objective-C"]];
        if (ready) {
            return (
                <View style={[GlobalStyles.whiteModule, {marginTop: 0, zIndex: 1}]}>
                    <View style={[GlobalStyles.fundModule, {paddingLeft: 0, paddingRight: 0, marginBottom: 110}]}>
                        <FlatList
                            style={styles.shopListView}
                            keyExtractor={item => `${item.id}`}
                            data={fundListData}
                            extraData={this.state}
                            renderItem={(item) => this.renderCompanyItem(item)}
                            onEndReachedThreshold={0.1}
                            // onEndReached={(info) => this.dropLoadMore(info)}
                            onRefresh={this.freshNetData}
                            refreshing={refreshing}
                            ItemSeparatorComponent={this.renderSeparator}
                            ListHeaderComponent={this.renderHeaderView}
                            ListFooterComponent={this.renderFooterView}
                            ListEmptyComponent={this.renderEmptyView}
                        />
                    </View>
                </View>
            )
        } else {
            return (
                <ActivityIndicatorItem/>
            )
        }
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    topBar: {
        height: 55,
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        zIndex: 999,
        borderBottomWidth: 10,
        borderBottomColor: GlobalStyles.bgColor
    },
    topLeft: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    topLeftIco: {
        height: 24,
        width: 24,
        marginRight: 5
    },
    topLeftText: {
        fontSize: 15,
        color: '#666',
        height: 20,
        lineHeight: 20,
    },
    shuxian: {
        position: 'absolute',
        height: 24,
        width: 1.5,
        backgroundColor: '#ececec',
        top: 10
    },
    fundLogin: {
        width: GlobalStyles.width,
        flex: 1,
        paddingBottom: 30,
        backgroundColor: '#fff'
    },
    fundLoginImg: {
        width: GlobalStyles.width,
        height: GlobalStyles.width * 860 / 660,
    },
    submitbot: {
        position: 'absolute',
        bottom: 0,
        width: GlobalStyles.width - 30,
        borderRadius: 0,
        height: 42
    },
    paixuList: {
        width: GlobalStyles.width,
        backgroundColor: '#fff',
        paddingLeft: 15,
        paddingRight: 15,
        position: 'absolute',
        top: 40,
        left: 0,
        marginTop: 10,
        // borderTopWidth: 1,
        // borderTopColor: '#f2f2f2',
    },
    paixuItem: {
        height: 45,
        borderTopWidth: 1,
        borderTopColor: '#f2f2f2',
    },
    paixuText: {
        color: '#666'
    },
    paixuItemActive: {},
    paixuTextActive: {
        color: GlobalStyles.themeColor
    },
    gouxuanIco: {
        width: 20,
        height: 20,
    },
    yinying: {
        backgroundColor: 'rgba(0,0,0,.3)',
        position: 'absolute',
        width: GlobalStyles.width,
        height: GlobalStyles.height - 64 - 55 - 10,
        bottom: 0,
        top: 64 + 55 + 10,
        zIndex: 99,
    },
    shaixuanList: {
        width: GlobalStyles.width,
        backgroundColor: '#fff',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 15,
        position: 'absolute',
        top: 40,
        left: 0,
        marginTop: 10,

    },
    sxItem: {
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: 1,
        paddingBottom: 12,
        marginBottom: 12,
    },
    sxItembot: {
        marginBottom: 12,
    },
    sxTitle: {
        fontSize: 15,
        color: '#666',
        marginBottom: 10,
        // marginTop: 5,
    },
    sxCon: {
        // backgroundColor: '#000'
    },
    sxKuang: {
        borderRadius: 5,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 7,
        paddingRight: 7,
        borderWidth: 1,
        borderColor: '#f2f2f2',
        marginRight: 10
    },
    sxKuangText: {
        color: '#333',
        fontSize: 15
    },
    sxKuangActive: {
        borderColor: GlobalStyles.themeColor,
        backgroundColor: GlobalStyles.themeColor,
    },
    sxKuangTextActive: {
        color: '#fff',
    },
    cellInput: {
        borderRadius: 5,
        padding: 5,
        borderWidth: 1,
        borderColor: '#f2f2f2',
        color: '#666',
        width: 50,
        textAlign: 'center'
    },
    jizhunxian: {
        marginLeft: 10,
        marginRight: 10,
    },
    baifenhao: {
        marginLeft: 10,
        color: GlobalStyles.themeColor,
        fontWeight: 'bold'
    },
    sxbtn: {
        width: (GlobalStyles.width - 30) / 2 - 10,
        height: 40,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#f2f2f2',
    },
    sxbtna: {
        color: GlobalStyles.themeColor,
        fontSize: 16
    },
    sxbtnn: {
        width: (GlobalStyles.width - 30) / 2 - 10,
        height: 40,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#f2f2f2',
        backgroundColor: GlobalStyles.themeColor,
    },
    sxbtnna: {
        color: '#fff',
        fontSize: 16
    },
    text: {
        width: 80,
    },
    icon: {
        width: 18,
        height: 18
    },
    shopListView: {
        // paddingHorizontal: 15,
        marginBottom: 15,
    },
});
