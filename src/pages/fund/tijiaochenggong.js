/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
export default class Tijiaochenggong extends Component {

    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state =  {
            user: global.user.userData,
            loginState: global.user.loginState,
            money: params.money,
            fene:  params.fene,
            rate_money: params.rate_money,
            total_money: params.total_money,
            order_id: params.order_id,
            fund_id: params.fund_id,
            payment_day: '',
            isDateTimePickerVisible: false,
            fundDetailBankTitle: params.fundDetailBankTitle,
            fundDetailBankName: params.fundDetailBankName,
            fundDetailBankNum: params.fundDetailBankNum,
            open_day: params.open_day,
        }
    }

    componentDidMount(){
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    loadNetData = () => {
        
    }

    onPushNavigator = (compent) => {
        const { navigate } = this.props.navigation;
        navigate( compent , {
            
        })
    }

    hasCompleted = (DateFormat) => {
        let url = ServicesApi.confirm_payment;
        let data = {
            order_id: this.state.order_id,
            payment_day: DateFormat,
            member_id: global.user.userData.id,
            token: global.user.userData.token,
        }
        Services.Post(url, data)
            .then( result => {
                if (result && result.code == 1) {
                    // console.log(result);
                    toastShort(result.msg);
                }else{
                    toastShort(result.msg);
                }
            })
            .catch( error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    sendSms = () => {
        let url = ServicesApi.send_bank_msg;
        let data = {
            fund_id: this.state.fund_id,
            member_id: global.user.userData.id,
            token: global.user.userData.token,
        }
        Services.Post(url, data)
            .then( result => {
                if (result && result.code == 1) {
                    // console.log(result);
                    toastShort(result.msg);
                }else{
                    toastShort(result.msg);
                }
            })
            .catch( error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    _showDateTimePicker = () => {
        this.setState({ 
            isDateTimePickerVisible: true 
        })
    }

    _hideDateTimePicker = () => {
        this.setState({ 
            isDateTimePickerVisible: false 
        });
    }

    _handleDatePicked = (date) => {
        // console.log(date);
        let DateFormat =  moment(date).format("YYYY-MM-DD");
        this.setState({
            payment_day:DateFormat
        });
        // console.log(DateFormat);
        this._hideDateTimePicker();
        this.hasCompleted(DateFormat);
    };

    render(){
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = {'提交订单成功'}
                    rightButton = {UtilsView.getRightHomeBlackButton(() => this.onPushNavigator('TabNavScreen'))}
                />
                <ScrollView>
                    <View style={[styles.tijiaoStatus, GlobalStyles.whiteModule, GlobalStyles.mt10, GlobalStyles.flexColumnCenter]}>
                        <Image source={Images.icon_chenggong} style={styles.statusIco} />
                        <Text style={styles.statusText}>订单提交成功</Text>
                    </View>
                    <View style={[styles.tijiaoInfo, GlobalStyles.whiteModule, GlobalStyles.mt10, GlobalStyles.pt0]}>
                        <View style={[styles.infoWrap, GlobalStyles.flexRowStart, {marginTop: 0}]}><Text style={[styles.infoText, styles.infoTextThemeColor]}>订单详情</Text></View>
                        <View style={[styles.infoWrap, GlobalStyles.flexRowStart, ]}><Text style={styles.infoText}>申购份额：{this.state.money}</Text></View>
                        <View style={[styles.infoWrap, GlobalStyles.flexRowStart, ]}><Text style={styles.infoText}>申购金额：{this.state.money}</Text></View>
                        <View style={[styles.infoWrap, GlobalStyles.flexRowStart, ]}><Text style={styles.infoText}>申购费：{this.state.rate_money}</Text></View>
                        <View style={[styles.infoWrap, GlobalStyles.flexRowStart, ]}><Text style={styles.infoText}>合计金额：{this.state.total_money}</Text></View>
                        <TouchableOpacity onPress={() => {this.sendSms()}} style={[GlobalStyles.submit, styles.submit, GlobalStyles.mt20]}>
                            <Text style={GlobalStyles.btna}>汇款信息发送到手机</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {this._showDateTimePicker()}} style={[GlobalStyles.submit, styles.submit]}>
                            <Text style={GlobalStyles.btna}>已完成汇款？</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.zhanghu, GlobalStyles.whiteModule]}>
                        <Text style={[styles.orderText, {marginBottom: 8}]}>订单提交后，请于{this.state.open_day}下午15点前，使用绑定的银行卡（尾号{global.user.userData.bank_num}）将合计金额汇款至募集账户——</Text>
                        <Text style={[styles.orderText, styles.textWeight, {color: GlobalStyles.themeColor}]}>银行户名：{this.state.fundDetailBankTitle}</Text>
                        <Text style={[styles.orderText, styles.textWeight, {color: GlobalStyles.themeColor}]}>开户行：{this.state.fundDetailBankName}</Text>
                        <Text style={[styles.orderText, styles.textWeight, {color: GlobalStyles.themeColor}]}>银行账号：{this.state.fundDetailBankNum}</Text>
                    </View>

                    <DateTimePicker
                        titleIOS={'选择时间'}
                        confirmTextIOS='确认'
                        cancelTextIOS='取消'
                        datePickerModeAndroid='calendar'
                        mode='date'
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    tijiaoStatus: {
        padding: 35,
        marginTop: 10
    },
    statusIco: {
        width: 75,
        height: 75,

    },
    statusText: {
        marginTop: 13,
        fontSize: 17,
        fontWeight: 'bold',
        color: GlobalStyles.themeColor,
    },
    tijiaoInfo: {
        padding: 15,
    },
    infoTextThemeColor: {
        color: GlobalStyles.themeColor,
    },
    infoWrap: {
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: 1,
        height: 40,
    },
    infoText: {
        color: '#999',
        fontSize: 14,
    },
    submit: {
        marginTop: 15,
        marginBottom: 0,
        height: 46
    },
    zhanghu: {
        marginTop: 20,
        padding: 15
    },
});
