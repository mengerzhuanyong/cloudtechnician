/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar,
    FlatList,
    DeviceEventEmitter
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'
import ActivityIndicatorItem from '../../components/common/ActivityIndicatorItem'

import EmptyComponent from '../../components/common/emptyComponent'
import FooterComponent from "../../components/common/footerComponent";

export default class Yinhangka extends Component {

    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        this.state = {
            ready: false,
            loadMore: false,
            refreshing: false,
            companyListData: [],
            token: '',
            fromtixianpage: params.fromtixianpage,
        }
    }

    async componentWillMount() {
        this.loadNetData();
        setTimeout(() => {
            this.setState({
                ready: true
            })
        }, 0)
    }

    onBack = () => {
        const {goBack, state} = this.props.navigation;
        state.params && state.params.onCallBack && state.params.onCallBack();
        goBack();
    }
    componentWillUnmount() {
        this.onBack();

    }

    onPushNavigator = (component) => {
        const {navigate} = this.props.navigation;
        navigate(component, {
            onCallBack: () => {
                this.loadNetData();
            }
        })
    }

    loadNetData = () => {

        let url = ServicesApi.get_bank_card;

        Services.Get(url)
            .then(result => {
                if (result && result.code == 1) {
                    this.setState({
                        companyListData: result.data.bank_list,
                    })
                } else {
                    // toastShort(result.msg);
                }
            })
            .catch(error => {
                // console.log(error);
                // toastShort('服务器请求失败，请稍后重试！');
            })
    }
    dropLoadMore = () => {
        this.loadNetData();
    }
    freshNetData = () => {
        this.loadNetData();
    }
    renderCompanyItem = ({item}) => {
        let bank_number = item.bank_number;
        let id = item.id;
        if (this.state.fromtixianpage == 1) {
            return (
                <TouchableOpacity onPress={() => {
                    DeviceEventEmitter.emit('tixianka', {num: bank_number, id: id});
                    this.onBack()
                }}>
                    <View style={styles.bankItem}>
                        <View style={[styles.bankTop, GlobalStyles.flexRowBetween]}>
                            <Text style={styles.bankTit}>{item.bank_name}</Text>
                        </View>
                        <View style={[styles.bankMid, GlobalStyles.flexRowStart]}>
                            <Text style={styles.bankNumber}>{bank_number}</Text>
                        </View>
                        <Text style={styles.bankBot}>{item.bank_addr}</Text>
                    </View>
                </TouchableOpacity>
            );
        }
        return (
            <View style={styles.bankItem}>
                <View style={[styles.bankTop, GlobalStyles.flexRowBetween]}>
                    <Text style={styles.bankTit}>{item.bank_name}</Text>
                </View>
                <View style={[styles.bankMid, GlobalStyles.flexRowStart]}>
                    <Text style={styles.bankNumber}>{bank_number}</Text>
                </View>
                <Text style={styles.bankBot}>{item.bank_addr}</Text>
            </View>
        )
    };
    renderHeaderView = () => {
        return (
            <View style={styles.shopListViewTitle}>

            </View>
        )
    }
    renderFooterView = () => {
        return <FooterComponent/>;
    }
    renderEmptyView = () => {
        return <EmptyComponent/>;
    }
    renderSeparator = () => {
        return <View style={GlobalStyles.horLine}/>;
    }

    render() {
        const {ready, refreshing, companyListData} = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'绑定银行卡'}

                />
                <View style={styles.bankList}>
                    {ready ?
                        <FlatList
                            style={styles.shopListView}
                            keyExtractor={item => `${item.id}`}
                            data={companyListData}
                            extraData={this.state}
                            renderItem={(item) => this.renderCompanyItem(item)}
                            onEndReachedThreshold={0.1}
                            onEndReached={(info) => this.dropLoadMore(info)}
                            onRefresh={this.freshNetData}
                            refreshing={refreshing}
                            ItemSeparatorComponent={this.renderSeparator}
                            ListHeaderComponent={this.renderHeaderView}
                            ListFooterComponent={this.renderFooterView}
                            ListEmptyComponent={this.renderEmptyView}
                        />
                        : <ActivityIndicatorItem/>
                    }
                </View>
                <TouchableOpacity onPress={() => {
                    toastShort('温馨提示：暂不支持绑定信用卡，请绑定您常用的银行借记卡!', 5000);
                    this.onPushNavigator('AddBank')
                }} style={[styles.addBank, GlobalStyles.flexRowCenter]}>
                    <Text style={styles.addText}>添加银行卡</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    bankList: {
        paddingTop: 10,
        padding: 15,
        flex: 1,
    },
    bankItem: {
        padding: 15,
        borderRadius: 6,
        backgroundColor: GlobalStyles.themeColor,
        marginBottom: 15,
        paddingTop: 5
    },
    bankTop: {
        marginTop: 2,
        // marginBottom: 10
    },
    bankTit: {
        color: '#fff',
        fontSize: 16,
    },
    bankDefault: {
        // backgroundColor: '#000',
        padding: 10,
        paddingRight: 0
    },
    bankRadio: {
        width: 18,
        height: 18,
        borderWidth: 2,
        borderColor: '#fff',
        borderRadius: 10,
        marginRight: 5,
        position: 'relative',
    },
    bankRadioActive: {
        position: 'absolute',
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#fff',
        top: 2,
        left: 2,

    },
    bankDefaultText: {
        color: '#fff',
        fontSize: 16,
    },
    bankMid: {
        marginBottom: 10
    },
    bankNumber: {
        color: '#fff',
        fontSize: 28,
        marginRight: 10,
    },
    bankBot: {
        color: '#fff',
        fontSize: 16
    },
    addBank: {
        backgroundColor: '#fff',
        width: GlobalStyles.width,
        borderRadius: 6,
        height: 60,
        marginTop: -10
    },
    addIco: {
        width: 26,
        height: 26,
        marginRight: 6
    },
    addText: {
        color: '#666',
        fontSize: 17,
        color: GlobalStyles.themeColor,
    },
});
