/**
 * 云技师 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'

export default class AddBank extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user_name: '',
            bank_number: '',
            bank_telephone: '',
            bank_name: '',
        }
    }

    componentDidMount() {
        this.loadNetData();
    }


    onBack = () => {
        this.props.navigation.state.params.onCallBack();
        this.props.navigation.goBack();
    }

    componentWillUnmount() {
        let timers = [this.timer];
        ClearTimer(timers);
    }

    loadNetData = () => {

    }
    getUserInfo = () => {
        let url = ServicesApi.get_member_info;

        let data = {
            member_id: global.user.userData.id,
        };
        Services.Post(url, data)
            .then(result => {
                // console.log(result.code);
                if (result && result.code == 1) {
                    // console.log(result);
                    let user = result.data;

                    this.setState({
                        user: user
                    });


                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                toastShort('服务器请求失败，请稍后重试！');
            })
    }
    onPushNavigator = (compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {})
    }
    Submit = () => {
        let url = ServicesApi.insert_bank_card;
        let data = {
            user_name: this.state.user_name,
            bank_number: this.state.bank_number,
            bank_telephone: this.state.bank_telephone,
            bank_name: this.state.bank_name,
        };

        Services.Post(url, data)
            .then(result => {
                if (result && result.code == 1) {
                    this.timer = setTimeout(() => {
                        this.onBack();
                    }, 1000);
                    toastShort(result.msg);
                } else {
                    toastShort(result.msg);
                }
            })
            .catch(error => {
                // console.log(error);
                toastShort('服务器请求失败，请稍后重试！');
            })
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'添加银行卡'}
                />

                <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>
                    <View style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>真实姓名</Text>
                            <TextInput
                                placeholder={'请填写真实姓名'}
                                onChangeText={(text) => {
                                    this.setState({
                                        user_name: text
                                    })
                                }}
                                style={[GlobalStyles.cellInput, styles.rightTextCon]}
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                    </View>
                    <View style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>银行预留手机号</Text>
                            <TextInput
                                placeholder={'请填写银行预留手机号'}
                                keyboardType="numeric"
                                maxLength={11}
                                onChangeText={(text) => {
                                    this.setState({
                                        bank_telephone: text
                                    })
                                }}
                                style={[GlobalStyles.cellInput, styles.rightTextCon]}
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                    </View>
                    <View style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>银行卡号</Text>
                            <TextInput
                                placeholder={'请填写银行卡号'}
                                keyboardType="numeric"
                                maxLength={18}
                                onChangeText={(text) => {
                                    this.setState({
                                        bank_number: text
                                    })
                                }}
                                style={[GlobalStyles.cellInput, styles.rightTextCon]}
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                    </View>
                    <View style={GlobalStyles.userlist}>
                        <View style={GlobalStyles.userlistright}>
                            <Text style={GlobalStyles.userlisttext}>开户行名称</Text>
                            <TextInput
                                placeholder={'请填写开户行名称(精确到分支行)'}
                                onChangeText={(text) => {
                                    this.setState({
                                        bank_name: text
                                    })
                                }}
                                style={[GlobalStyles.cellInput, styles.rightTextCon]}
                                underlineColorAndroid={'transparent'}
                                // multiline={true}
                            />
                        </View>
                    </View>
                </View>

                <TouchableOpacity onPress={() => this.Submit()} style={[GlobalStyles.submit, {marginBottom: 15}]}>
                    <View style={GlobalStyles.btn}>
                        <Text style={GlobalStyles.btna}>提交</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    rightTextView: {
        flex: 1,
        height: 45,
        justifyContent: 'center',
    },
    rightTextCon: {
        flex: 1,
        height: 45,
        // lineHeight: 45,
        textAlign: 'right',
        // backgroundColor: '#123'
    },
});
