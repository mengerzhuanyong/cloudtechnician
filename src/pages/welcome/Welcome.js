/**
 * 灵工云 - 欢迎页
 * http://menger.me
 * @大梦
 */


'use strict';

import React from 'react'
import {StyleSheet, View,} from 'react-native'
import NavigationBar from "../../components/common/NavigationBar";
import BannerComponent from "../../components/carousel/BannerComponent";
import {Button} from "teaset";

export default class Welcome extends React.Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            bannerData: this.params.bannerData || [],
        };
    }

    componentDidMount() {
    }

    onPressToResetPage = () => {
        let {onCallBack, component} = this.params;
        onCallBack && onCallBack();
        component = component || 'Login';
        RouterHelper.reset('', component);
    };

    render() {
        let {bannerData} = this.state;
        return (
            <View style={[styles.container]}>
                <NavigationBar
                    showLeftBtn={false}
                    leftButton={null}
                    statusBar={{
                        barStyle: 'light-content'
                    }}
                    style={GlobalStyles.transparentNavBar}
                />
                <View style={styles.content}>
                    <BannerComponent
                        cycle={false}
                        interval={4000}
                        showControl={false}
                        bannerData={bannerData}
                        style={styles.advertisementContent}
                    />
                    <Button
                        title={'跳过'}
                        style={styles.btnStyle}
                        titleStyle={styles.btnTitleStyle}
                        onPress={this.onPressToResetPage}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        zIndex: 9999,
        backgroundColor: '#fff',
        marginTop: GlobalStyles.overNavigationBar,
    },
    advertisementContent: {
        height: SCREEN_HEIGHT,
        backgroundColor: '#fff',
    },
    btnStyle: {
        top: 50,
        right: 10,
        width: 50,
        height: 25,
        zIndex: 9999,
        borderWidth: 0,
        borderRadius: 25,
        paddingVertical: 0,
        position: 'absolute',
        backgroundColor: '#ffffff40'
    },
    btnTitleStyle: {
        fontSize: 12,
        color: '#fff',
    },
});