/**
 * 云技师 - 订单管理
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {StyleSheet, View} from 'react-native'

import NavigationBar from '../../components/common/NavigationBar'
import SegmentedView from "../../components/segmented/SegmentedView";
import ArticleTabList from "../../components/forum/ArticleTabList";

const maxCount = 5;

export default class OrderManage extends Component {

    constructor(props) {
        super(props);
        let {params} = this.props.navigation.state;
        this.state = {
            activeIndex: params && params.activeIndex ? params.activeIndex : 0,
            sort_value: 1,
            sort_arr: [],
            navArray: [],
        };
    }

    componentDidMount() {
        this.requestNavArray();
    }


    requestNavArray = () => {
        let url = ServicesApi.ArticleFilter;
        Services.Get(url)
            .then(result => {
                console.log(result);
                if (result && result.code === 1) {
                    this.setState({
                        sort_arr: result.data.sort_arr,
                        navArray: result.data.cate_arr,
                    })
                }
            })
            .catch(error => {
                console.log(error);
                Toast.toastShort('error');
            })
    };

    renderSegmentedView = () => {
        let {activeIndex, navArray, sort_arr, sort_value} = this.state;
        let length = navArray.length;
        if (length < 1) {
            return null;
        }
        let content = navArray.map((item, index) => {
            return (
                <View
                    key={index}
                    title={item.name}
                    style={styles.navBarItemView}
                    titleStyle={styles.sheetTitle}
                    activeTitleStyle={styles.sheetActiveTitle}
                    itemStyle={{width: GlobalStyles.width / (length > maxCount ? maxCount : length)}}
                >
                    <ArticleTabList
                        cate_id={item.value}
                        sort_value={sort_value}
                        {...this.props}
                    />
                </View>
            );
        });
        return (
            <SegmentedView
                initialPage={activeIndex}
                ref={v => this.segmentedView = v}
                style={styles.segmentedView}
                barStyle={styles.segmentedBar}
                indicatorType={'customWidth'}
                indicatorLineColor={GlobalStyles.themeColor}
                indicatorLineWidth={4}
                indicatorWidth={GlobalStyles.width / (length > maxCount ? maxCount : length)}
                indicatorPositionPadding={ScaleSize(-4)}
                scrollEnabled={true}
                justifyItem={length > maxCount ? 'scrollable' : 'fixed'}
                keyboardShouldPersistTaps={'always'}
            >{content}</SegmentedView>
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'云社区'}
                    leftButton={null}
                />
                {this.renderSegmentedView()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    segmentedView: {
        backgroundColor: '#fff',
    },
    segmentedBar: {
        height: 45,
        borderBottomWidth: 1,
        borderBottomColor: '#dbdbdb',
    },
    sheetActiveTitle: {
        color: GlobalStyles.themeColor,
        fontSize: FontSize(13),
    },
    sheetTitle: {
        color: '#999',
        fontSize: FontSize(13),
    },
    navBarItemView: {
        flex: 1,
        width: GlobalStyles.width,
    },
});
