/**
 * 云技师 - ArticleList
 * http://menger.me
 * @大梦
 */


'use strict';

import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import NavigationBar from "../../components/common/NavigationBar";
import ArticleTabList from "../../components/forum/ArticleTabList";

export default class ArticleList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            loading: false,
            dataSource: [],
            type: this.props.type,
            status: this.props.status,
        };
        this.page = 1;
        this.pageSize = 10;
    }

    render() {
        let {ready, dataSource} = this.state;
        let {params} = this.props.navigation.state;
        let pageTitle = params.pageTitle || '云社区';
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={pageTitle}
                />
                <View style={styles.content}>
                    <ArticleTabList
                        cate_id={params.cate_id || 0}
                        sort_value={1}
                        {...this.props}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    // 内容区
    content: {
        flex: 1,
        marginTop: 10,
        backgroundColor: GlobalStyles.bgColor,
    },
    headerComponentView: {},
    // 列表区
    listContent: {
        flex: 1,
        width: SCREEN_WIDTH,
    },
    horLine: {
        backgroundColor: '#dbdbdb',
    },
});