/**
 * 速芽物流 - 公共详情页
 * http://menger.me
 * @Meng
 */

import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    WebView,
    Dimensions,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
} from 'react-native';



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import HTMLView from 'react-native-htmlview';
export default class XiaoxiDetail extends Component {

    constructor(props){
        super(props);
        const { params } = this.props.navigation.state;
        this.state={
            content: params.content
        }
    }

    componentDidMount(){
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.state.params.onCallBack();
        this.props.navigation.goBack();
    }

    componentWillUnmount() {
        this.onBack();
    }

    loadNetData = () => {
        
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationBar
                    title = '消息详情'
                />
                <View style={{ padding: 15, }}>
                    <Text style={{ color: '#666', fontSize: 14, lineHeight: 22 }}>{this.state.content}</Text>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    webContainer: {
        flex: 1,
        backgroundColor: '#f1f2f3',
    },
});