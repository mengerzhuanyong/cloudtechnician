/**
 * 云技师 - WebPage
 * http://menger.me
 * @大梦
 */

'use strict';

import React, {PureComponent} from 'react'
import {StyleSheet, View, WebView,} from 'react-native'
import NavigationBar from "../../components/common/NavigationBar";
import SpinnerLoading from "../../components/loading/SpinnerLoading";


const patchPostMessageFunction = () => {
    const originalPostMessage = window.postMessage;
    const patchedPostMessage = (message, targetOrigin, transfer) => {
        originalPostMessage(message, targetOrigin, transfer);
    };
    patchedPostMessage.toString = () => String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
    window.postMessage = patchedPostMessage;
};

const patchPostMessageJsCode = `(${String(patchPostMessageFunction)})();`;

export default class WebPage extends PureComponent {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            ready: false,
        };
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    onLoadEnd = () => {
        this.setState({ready: true});
    };

    onMessage = (event) => {
        console.log('event.nativeEvent.data---->', event.nativeEvent.data);
        let {data} = event.nativeEvent;
        if (data === 'goBack') {
            RouterHelper.goBack();
        }
    }

    render() {
        let {ready} = this.state;
        let uri = this.params.url || '';
        let style = this.params.style || '';
        let pageTitle = this.params.pageTitle || '详情页';
        console.log('WebPageAddress---->', uri);
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={pageTitle}
                />
                <WebView
                    source={{uri}}
                    onLoadEnd={this.onLoadEnd}
                    startInLoadingState={false}
                    style={[styles.webContainer, style]}
                    injectedJavaScript={patchPostMessageJsCode} // 注入js代码
                    onMessage={this.onMessage}
                />
                <SpinnerLoading isVisible={!ready}/>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f2f3',
    },
    webContainer: {
        flex: 1,
        backgroundColor: '#f1f2f3',
    },
});