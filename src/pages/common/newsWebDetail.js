/**
 * 速芽物流 - 公共详情页
 * http://menger.me
 * @Meng
 */

import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    WebView,
    Dimensions,
    StyleSheet,
    StatusBar,
    TouchableOpacity
} from 'react-native';



import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import { toastShort, consoleLog } from '../../utils/utilsToast'
import ShareUtils from '../../components/common/shareUtils'
import ActivityIndicatorItem from "../../components/common/ActivityIndicatorItem";
import SpinnerLoading from "../../components/loading/SpinnerLoading";

const WEBVIEW_REF = 'webview';

export default class NewsWebDetail extends Component {

    constructor(props){
        super(props);
        this.params = this.props.navigation.state.params;
        this.state={
            ready: false,
            url: this.params.link || '',
            type: this.params.type || 1,
            title: this.params.title || '',
            content_id: this.params.content_id || '',
            is_collection: this.params.is_collection || '',
        }
    }

    componentDidMount() {
        this.timer = setTimeout(() => {
            this.setState({
                ready: true,
            });
        }, 500);
    }

    componentWillUnmount(){
        let {params} = this.props.navigation.state;
        params && params.onCallBack && params.onCallBack();
        this.timer&&clearTimeout(this.timer);
    }

    onBack = () => {
        this.props.navigation.goBack();
    };

    onSharePress = () => {
        this.setState({
            showSharePop: !this.state.showSharePop
        })
    };

    loadNetData = () => {

    };

    onCollect =() =>{
        let url= ServicesApi.collectArticle+"/article_id/"+this.state.content_id;
        Services.Get(url,true)
            .then( result => {
                // console.log(result);
                if (result && result.code == 1) {
                    toastShort(result.msg);
                    this.setState({
                        is_collection: 1,
                    })
                }else{
                    this.setState({
                        is_collection: 0,
                    });
                    toastShort(result.msg);
                }
            })
            .catch( error => {
                toastShort('服务器请求失败，请稍后重试！');
            })

    }


    renderRightButton = (status) => {
        // console.log(status);
        if (this.state.type === 2) {
            return null;
        }
        if (status == 0) {
            return (
                <TouchableOpacity onPress={()=>{this.onCollect()}} style={styles.botLeft}>
                    <Image source={Images.icon_shoucang} style={[styles.botLeftIco, {width: 24}]} />
                </TouchableOpacity>
            )
        }
        return (
            <TouchableOpacity onPress={()=>{this.onCollect()}} style={styles.botLeft}>
                <Image source={Images.icon_shoucang_cur} style={[styles.botLeftIco, {width: 24}]} />
            </TouchableOpacity>
        )
    }

    onLoadEnd = () => {
        this.setState({ready: true});
    };

    render() {
        let {ready, is_collection, url} = this.state;
        let style = this.params.style || '';
        let pageTitle = this.params.pageTitle || '详情页';
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={pageTitle}
                    rightButton={this.renderRightButton(is_collection)}
                />
                <WebView
                    source={{url}}
                    onLoadEnd={this.onLoadEnd}
                    startInLoadingState={false}
                    style={[styles.webContainer, style]}
                />
                <SpinnerLoading isVisible={!ready}/>
                {this.state.showSharePop?
                    <View>
                        <ShareUtils
                            style={{position:'absolute'}}
                            show={this.state.showSharePop}
                            closeModal={(show) => {
                                this.setState({
                                    showSharePop: show
                                })
                            }}
                            title = {this.state.title}
                            description = {this.state.description}
                            thumbImage = {this.state.logo}
                            webpageUrl = {this.state.webpageUrl}
                            id = {this.state.id}
                            {...this.props}
                        />
                    </View>
                    : null
                }
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1f2f3',
    },    
    webContainer: {
        flex: 1,
        backgroundColor: '#f1f2f3',
    },
    botLeft: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
    },
    botLeftIco: {
        height: 24,

    },
});