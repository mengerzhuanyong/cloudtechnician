/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'

import SpinnerLoading from '../../components/loading/SpinnerLoading'
import SYImagePicker from 'react-native-syan-image-picker'
import Yinhangka from "../fund/yinhangka";
import RouterHelper from "../../routers/RouterHelper";
import LargePicture from "../../components/common/LargePicture";

const options = {
    imageCount: 1,             // 最大选择图片数目，默认6
    isCamera: true,            // 是否允许用户在内部拍照，默认true
    isCrop: false,             // 是否允许裁剪，默认false
    CropW: ~~(GlobalStyles.width * 0.6),    // 裁剪宽度，默认屏幕宽度60%
    CropH: ~~(GlobalStyles.width * 0.6),    // 裁剪高度，默认屏幕宽度60%
    isGif: false,              // 是否允许选择GIF，默认false，暂无回调GIF数据
    showCropCircle: false,     // 是否显示圆形裁剪区域，默认false
    circleCropRadius: GlobalStyles.width / 2,  // 圆形裁剪半径，默认屏幕宽度一半
    showCropFrame: true,       // 是否显示裁剪区域，默认true
    showCropGrid: false,       // 是否隐藏裁剪区域网格，默认false
    quality: 50,                // 压缩质量
    enableBase64: true
};
/**
 * 订单详情页面
 */
export default class OrderDetail extends Component {

    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        this.state = {
            id: params.id,
            status: params.status,
            page_flag: params && params.page_flag ? params.page_flag : '',
            sn: '',
            type_name: '',
            server_time: '',
            server_address: '',
            goods_list: [],
            photo_list: [],
            uploadings: [false, false, false, false, false, false,],
            telephone: "",
            consignee: "",
            is_zhibao: 0,
            confirm_code: '',
        }
    }

    componentDidMount() {
        this.loadNetData()
    }

    onBack = () => {
        const {goBack, state} = this.props.navigation;
        state.params && state.params.onCallBack && state.params.onCallBack();
        goBack();
    };

    componentWillUnmount() {
        this.onBack();
    }

    loadNetData = () => {
        let {id} = this.state;
        let url = ServicesApi.getOrderInfo;
        let data = {
            id,
        };
        Services.Post(url, data, true)
            .then(result => {
                // console.log(result.code);
                if (result.code == 1) {
                    this.setState({
                        sn: result.data.sn,
                        status: result.data.status,
                        type_name: result.data.type_name,
                        server_time: result.data.server_time,
                        server_address: result.data.server_address,
                        goods_list: result.data.goods_list,
                        photo_list: result.data.server_albums,
                        telephone: result.data.telephone,
                        consignee: result.data.consignee,
                        goods_name: result.data.goods_name,
                        attr_name: result.data.attr_name,
                        num: result.data.num,
                        is_zhibao: result.data.is_zhibao,

                    })
                }
            })
            .catch(error => {
                // toastShort('');
            })
    }


    onPushNavigator = (compent, back_msg) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            back_msg: back_msg,
        })
    }

    goodlist = (data) => {
        data = [
            {name: 1, id: 1},
            {name: 2, id: 2}
        ];
        let list = [];
        for (let i = 0; i < data.length; i++) {
            let item = (
                <TouchableOpacity
                    key={'item' + i}
                    style={styles.goodsListItemView}
                >
                    <View style={GlobalStyles.newsLeft}>
                        <Image source={{uri: data[i].thumb}}
                               style={GlobalStyles.newsThumb}/>
                    </View>
                    <View style={[GlobalStyles.newsRight, GlobalStyles.flexColumnBetweenStart]}>
                        <Text style={GlobalStyles.newsTitle}>商品名称</Text>
                        <Text style={GlobalStyles.newsDesc}>data[i].name</Text>
                        <View style={[GlobalStyles.newsInfo, GlobalStyles.flexRowBetween]}>
                            <Text style={GlobalStyles.newsDate}>2018-5-30</Text>
                            <View style={[GlobalStyles.newsClick, GlobalStyles.flexRowStart]}>
                                <Text style={GlobalStyles.newsClickText}>数量</Text>
                                <Text style={GlobalStyles.newsClickNum}>1234</Text>

                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            )
            list.push(item)
        }
        return <View>{list}</View>

    }

    setPhoto = (index) => {
        SYImagePicker.showImagePicker(options, (err, selectedPhotos) => {
            if (err) {
                // 取消选择
                // console.log('您已取消选择');
                return;
            }
            // 选择成功
            // console.log('您已选择成功');
            // console.log(selectedPhotos);
            this.photoResult(selectedPhotos, index);
        })
    }

    /**
     * 第一张图片
     * @param datas
     */
    photoResult = (datas, index) => {
        let photoList = [],
            selectedPhotos = datas,
            uploadingStatus = this.state.uploadings,
            newlist = this.state.photo_list;

        // 图片base64转码
        let url = ServicesApi.addServerAlbum;
        let thisSelectedPhoto = selectedPhotos[0].base64;
        let img_id = newlist[index] ? newlist[index].id : '';
        let data = {
            base64: thisSelectedPhoto,
            order_id: this.state.id,
            id: img_id,
        };
        uploadingStatus[index] = true;
        this.setState({uploadings: uploadingStatus});
        Services.Post(url, data, true)
            .then(result => {
                if (result && result.code == 1) {
                    newlist[index] = {
                        id: result.data.id,
                        img_url: result.data.img_url,
                    };
                    this.setState({
                        photo_list: newlist,
                    })
                } else {
                    toastShort(result.msg);
                }
                uploadingStatus[index] = false;
                this.setState({uploadings: uploadingStatus});
            })
            .catch(error => {
                console.log(error);
                uploadingStatus[index] = false;
                this.setState({uploadings: uploadingStatus});
                toastShort('服务器请求失败，请稍后重试！');
            })

    };


    postRequest = (url, data) => {
        let {onRefresh} = this.props;
        Services.Post(url, data, true)
            .then(result => {
                if (result.code == 1) {
                    this.onBack();
                } else {
                    toastShort(result.msg, 'center');
                }
            })
            .catch(error => {
                console.log(error);
                toastShort('error');
            })
    };

    pickUpOrderItem = () => {
        let {id} = this.state;
        let url = ServicesApi.changeOrderStatus;
        let data = {
            id: id,
            status: 1,
        };
        let params = {
            title: '温馨提示',
            detail: '您是否要接受该订单？',
            actions: [
                {
                    title: '取消',
                    onPress: () => {
                    },
                },
                {
                    title: '确定',
                    onPress: () => this.postRequest(url, data),
                }
            ]
        };
        AlertManager.show(params);
    };

    refuseOrderItem = () => {
        let {id} = this.state;
        let url = ServicesApi.changeOrderStatus;
        let data = {
            id: id,
            status: -1,
        };

        let params = {
            title: '温馨提示',
            detail: '您是否要拒绝该订单？',
            actions: [
                {
                    title: '取消',
                    onPress: () => {
                    },
                },
                {
                    title: '确定',
                    onPress: () => this.postRequest(url, data),
                }
            ]
        };
        AlertManager.show(params);
    };

    completeOrderItem = () => {
        let {id, confirm_code} = this.state;
        let url = ServicesApi.changeOrderStatus;
        console.log('confirm_code---->', confirm_code);
        let data = {
            id: id,
            code: confirm_code,
            status: 2,
        };
        if (!confirm_code) {
            toastShort('请输入短信确认码');
            return;
        }

        let params = {
            title: '温馨提示',
            detail: '您是否已完成该订单？',
            actions: [
                {
                    title: '取消',
                    onPress: () => {
                    },
                },
                {
                    title: '确定',
                    onPress: () => this.postRequest(url, data),
                }
            ]
        };
        AlertManager.show(params);
    };

    submitOrderItem = (is_zhibao) => {
        let {id} = this.state;
        let orderInfo = {
            id,
            is_zhibao,
        };
        let pageTitle = orderInfo.is_zhibao > 0 ? '查看质保单' : '录入质保单';
        RouterHelper.navigate(pageTitle, 'InputQualityBill', {
            orderInfo,
            onCallBack: () => this.loadNetData(),
        });
    };

    renderOrderBtnContent = () => {
        let {status, is_zhibao, page_flag} = this.state;
        if (page_flag === 'property') {
            return null;
        }
        if (status === 0) {
            return (
                <View style={styles.orderBtnView}>
                    <TouchableOpacity
                        style={[styles.orderBtnItemView]}
                        onPress={this.refuseOrderItem}>
                        <Text style={[styles.orderBtnItemName,]}>拒&nbsp;&nbsp;绝</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.orderBtnItemView, styles.orderBtnItemViewCur]}
                        onPress={this.pickUpOrderItem}
                    >
                        <Text style={[styles.orderBtnItemName, styles.orderBtnItemNameCur]}>接&nbsp;&nbsp;单</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        if (status === 1) {
            return (
                <View style={styles.orderBtnView}>

                    <TouchableOpacity
                        style={[styles.orderBtnItemView, styles.orderBtnItemViewCur]}
                        onPress={this.completeOrderItem}>
                        <Text style={[styles.orderBtnItemName, styles.orderBtnItemNameCur]}>完&nbsp;&nbsp;成</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        if (status === 2) {
            return (
                <View style={styles.orderBtnView}>
                    <TouchableOpacity
                        style={[styles.orderBtnItemView, is_zhibao < 1 && styles.orderBtnItemViewCur, styles.customWidth]}
                        onPress={() => this.submitOrderItem(is_zhibao)}
                    >
                        <Text
                            style={[styles.orderBtnItemName, is_zhibao < 1 && styles.orderBtnItemNameCur]}>{is_zhibao > 0 ? '查看质保单' : '录入质保单'}</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        return null;
    };

    render() {
        let {
            status, photo_list, sn, consignee, telephone, type_name, uploadings,
            server_time, server_address, goods_list, goods_name, attr_name, num, page_flag
        } = this.state;
        let photoArray = photo_list.map((item, index) => item.img_url);
        console.log(photoArray);
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'订单详情页面'}
                />
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                >
                    <View style={styles.itemContentView}>

                        <Text style={{margin: 5}}>服务单号</Text>
                        <Text style={{margin: 5}}>{sn}</Text>

                    </View>
                    <View style={styles.itemContentView}>

                        <View style={{flexDirection: 'row'}}>
                            <Text style={{margin: 5}}>客户名称</Text>
                            <Text style={{margin: 5}}>{consignee}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{margin: 5}}>客户电话</Text>
                            <Text style={{margin: 5}}>{telephone}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{margin: 5}}>服务内容</Text>
                            <Text style={{margin: 5}}>{type_name}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{margin: 5}}>服务时间</Text>
                            <Text style={{margin: 5}}>{server_time}</Text>
                        </View>

                        <View style={{}}>
                            <Text style={{margin: 5}}>服务地点</Text>
                            <Text style={{margin: 5}}>{server_address}</Text>
                        </View>

                    </View>

                    <View style={styles.itemContentView}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{margin: 5}}>商品名称</Text>
                            <Text style={{margin: 5}}>{goods_name}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{margin: 5}}>商品属性</Text>
                            <Text style={{margin: 5}}>{attr_name}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={{margin: 5}}>商品数量</Text>
                            <Text style={{margin: 5}}>{num}</Text>
                        </View>
                    </View>

                    {status > 0 ?
                        <View style={styles.itemContentView}>
                            <Text style={{margin: 5}}>添加施工过程</Text>

                            <View style={styles.imagesContent}>
                                {status === 1 || photo_list[0] ?
                                    <TouchableOpacity
                                        style={[styles.imageItemView, styles.imageItemUploadView]}
                                        onPress={() => status === 1 && this.setPhoto(0)}
                                    >
                                        {uploadings[0] ?
                                            <View style={[styles.imageItemView]}>
                                                <SpinnerLoading />
                                            </View>
                                            :
                                            <View>
                                                {photo_list[0] ?
                                                    <View>
                                                        {status === 1 ?
                                                            <Image
                                                                source={{uri: photo_list[0].img_url}}
                                                                style={[styles.uploadIcon, styles.uploadImage]}
                                                            />
                                                            :
                                                            <LargePicture
                                                                maxScale={3.0}
                                                                resizeMode={'contain'}
                                                                imagesArray={photoArray}
                                                                activeIndex={0}
                                                                source={{uri: photo_list[0].img_url}}
                                                                style={[styles.uploadImageView]}
                                                                imageStyle={[styles.uploadImageView]}
                                                            />
                                                        }
                                                    </View>
                                                    :
                                                    <Image source={Images.picAdd} style={[styles.uploadIcon]}/>
                                                }
                                            </View>
                                        }
                                    </TouchableOpacity>
                                    : <View style={[styles.imageItemView, styles.imageItemViewNoBor]} />
                                }
                                {status === 1 || photo_list[1] ?
                                    <TouchableOpacity
                                        style={[styles.imageItemView, styles.imageItemUploadView]}
                                        onPress={() => status === 1 && this.setPhoto(1)}
                                    >
                                        {uploadings[1] ?
                                            <View style={[styles.imageItemView]}>
                                                <SpinnerLoading />
                                            </View>
                                            :
                                            <View>
                                                {photo_list[1] ?
                                                    <View>
                                                        {status === 1 ?
                                                            <Image
                                                                source={{uri: photo_list[1].img_url}}
                                                                style={[styles.uploadIcon, styles.uploadImage]}
                                                            />
                                                            :
                                                            <LargePicture
                                                                maxScale={3.0}
                                                                resizeMode={'contain'}
                                                                imagesArray={photoArray}
                                                                activeIndex={1}
                                                                source={{uri: photo_list[1].img_url}}
                                                                style={[styles.uploadImageView]}
                                                                imageStyle={[styles.uploadImageView]}
                                                            />
                                                        }
                                                    </View>
                                                    :
                                                    <Image source={Images.picAdd} style={[styles.uploadIcon]}/>
                                                }
                                            </View>
                                        }
                                    </TouchableOpacity>
                                    : <View style={[styles.imageItemView, styles.imageItemViewNoBor]} />
                                }
                                {status === 1 || photo_list[2] ?
                                    <TouchableOpacity
                                        style={[styles.imageItemView, styles.imageItemUploadView]}
                                        onPress={() => status === 1 && this.setPhoto(2)}
                                    >
                                        {uploadings[2] ?
                                            <View style={[styles.imageItemView]}>
                                                <SpinnerLoading />
                                            </View>
                                            :
                                            <View>
                                                {photo_list[2] ?
                                                    <View>
                                                        {status === 1 ?
                                                            <Image
                                                                source={{uri: photo_list[2].img_url}}
                                                                style={[styles.uploadIcon, styles.uploadImage]}
                                                            />
                                                            :
                                                            <LargePicture
                                                                maxScale={3.0}
                                                                resizeMode={'contain'}
                                                                imagesArray={photoArray}
                                                                activeIndex={2}
                                                                source={{uri: photo_list[2].img_url}}
                                                                style={[styles.uploadImageView]}
                                                                imageStyle={[styles.uploadImageView]}
                                                            />
                                                        }
                                                    </View>
                                                    :
                                                    <Image source={Images.picAdd} style={[styles.uploadIcon]}/>
                                                }
                                            </View>
                                        }
                                    </TouchableOpacity>
                                    : <View style={[styles.imageItemView, styles.imageItemViewNoBor]} />
                                }
                            </View>

                            <View style={styles.imagesContent}>
                                {status === 1 || photo_list[3] ?
                                    <TouchableOpacity
                                        style={[styles.imageItemView, styles.imageItemUploadView]}
                                        onPress={() => status === 1 && this.setPhoto(3)}
                                    >
                                        {uploadings[3] ?
                                            <View style={[styles.imageItemView]}>
                                                <SpinnerLoading />
                                            </View>
                                            :
                                            <View>
                                                {photo_list[3] ?
                                                    <View>
                                                        {status === 1 ?
                                                            <Image
                                                                source={{uri: photo_list[3].img_url}}
                                                                style={[styles.uploadIcon, styles.uploadImage]}
                                                            />
                                                            :
                                                            <LargePicture
                                                                maxScale={3.0}
                                                                resizeMode={'contain'}
                                                                imagesArray={photoArray}
                                                                activeIndex={3}
                                                                source={{uri: photo_list[3].img_url}}
                                                                style={[styles.uploadImageView]}
                                                                imageStyle={[styles.uploadImageView]}
                                                            />
                                                        }
                                                    </View>
                                                    :
                                                    <Image source={Images.picAdd} style={[styles.uploadIcon]}/>
                                                }
                                            </View>
                                        }
                                    </TouchableOpacity>
                                    : <View style={[styles.imageItemView, styles.imageItemViewNoBor]} />
                                }
                                {status === 1 || photo_list[4] ?
                                    <TouchableOpacity
                                        style={[styles.imageItemView, styles.imageItemUploadView]}
                                        onPress={() => status === 1 && this.setPhoto(4)}
                                    >
                                        {uploadings[4] ?
                                            <View style={[styles.imageItemView]}>
                                                <SpinnerLoading />
                                            </View>
                                            :
                                            <View>
                                                {photo_list[4] ?
                                                    <View>
                                                        {status === 1 ?
                                                            <Image
                                                                source={{uri: photo_list[4].img_url}}
                                                                style={[styles.uploadIcon, styles.uploadImage]}
                                                            />
                                                            :
                                                            <LargePicture
                                                                maxScale={3.0}
                                                                resizeMode={'contain'}
                                                                imagesArray={photoArray}
                                                                activeIndex={4}
                                                                source={{uri: photo_list[4].img_url}}
                                                                style={[styles.uploadImageView]}
                                                                imageStyle={[styles.uploadImageView]}
                                                            />
                                                        }
                                                    </View>
                                                    :
                                                    <Image source={Images.picAdd} style={[styles.uploadIcon]}/>
                                                }
                                            </View>
                                        }
                                    </TouchableOpacity>
                                    : <View style={[styles.imageItemView, styles.imageItemViewNoBor]} />
                                }
                                {status === 1 || photo_list[5] ?
                                    <TouchableOpacity
                                        style={[styles.imageItemView, styles.imageItemUploadView]}
                                        onPress={() => status === 1 && this.setPhoto(5)}
                                    >
                                        {uploadings[5] ?
                                            <View style={[styles.imageItemView]}>
                                                <SpinnerLoading />
                                            </View>
                                            :
                                            <View>
                                                {photo_list[5] ?
                                                    <View>
                                                        {status === 1 ?
                                                            <Image
                                                                source={{uri: photo_list[5].img_url}}
                                                                style={[styles.uploadIcon, styles.uploadImage]}
                                                            />
                                                            :
                                                            <LargePicture
                                                                maxScale={3.0}
                                                                resizeMode={'contain'}
                                                                imagesArray={photoArray}
                                                                activeIndex={5}
                                                                source={{uri: photo_list[5].img_url}}
                                                                style={[styles.uploadImageView]}
                                                                imageStyle={[styles.uploadImageView]}
                                                            />
                                                        }
                                                    </View>
                                                    :
                                                    <Image source={Images.picAdd} style={[styles.uploadIcon]}/>
                                                }
                                            </View>
                                        }
                                    </TouchableOpacity>
                                    : <View style={[styles.imageItemView, styles.imageItemViewNoBor]} />
                                }
                            </View>
                        </View>
                        : null
                    }

                    {status === 1 && page_flag !== 'property' ?
                        <View style={styles.itemContentView}>
                            <View style={{flexDirection: 'row', alignItems: 'center',}}>
                                <Text style={{margin: 5}}>短信确认码</Text>
                                <TextInput
                                    maxLength={6}
                                    keyboardType="numeric"
                                    placeholder={'请输入收到的确认码'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            confirm_code: text
                                        });
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        : null
                    }
                    {this.renderOrderBtnContent()}
                </ScrollView>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
    userPhoto: {
        width: 32,
        height: 32,
        position: 'absolute',
        right: 20,
        borderRadius: 16,
    },
    middle: {
        height: 30,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
    },
    backcolor: {
        backgroundColor: '#ffffff'
    },

    itemContentView: {
        padding: 10,
        marginTop: 10,
        minHeight: 45,
        borderRadius: 5,
        marginHorizontal: 10,
        backgroundColor: '#fff',
    },

    imagesContent: {
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: "center",
        backgroundColor: '#fff'
    },
    imageItemView: {
        width: 80,
        height: 80,
        borderWidth: 1,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#dbdbdb',
    },
    imageItemViewNoBor: {
        borderColor: '#fff',
    },
    imageItemUploadView: {},
    uploadIcon: {
        width: 40,
        height: 40,
        borderRadius: 5,
    },
    uploadImage: {
        width: 80,
        height: 80,
        borderRadius: 5,
        resizeMode: 'contain',
    },
    uploadImageView: {
        width: 80,
        height: 80,
        borderRadius: 5,
    },
    goodsListItemView: {
        marginTop: 5,
        marginRight: 10,
        marginLeft: 10,
        marginBottom: 5,
        backgroundColor: '#fff',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        padding: 5,
        borderRadius: 5
    },
    inputItemCon: {
        flex: 1,
        height: 45,
    },

    orderBtnView: {
        marginTop: 30,
        marginBottom: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    orderBtnItemView: {
        flex: 1,
        height: 45,
        marginHorizontal: 10,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: GlobalStyles.minPixel,
        backgroundColor: '#fff',
        borderColor: '#fff',
    },
    orderBtnItemViewCur: {
        borderColor: '#5c7091',
        backgroundColor: '#5c7091',
    },
    orderBtnItemViewNoBor: {
        borderColor: '#fff',
    },
    customWidth: {
        width: 90,
    },
    orderBtnItemName: {
        fontSize: 16,
        color: '#333',
    },
    orderBtnItemNameCur: {
        color: '#fff',
    }
});
