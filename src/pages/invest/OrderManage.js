/**
 * 云技师 - 订单管理
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'

import NavigationBar from '../../components/common/NavigationBar'
import SegmentedView from "../../components/segmented/SegmentedView";
import OrderList from "./OrderList";
import NewSend from "./newsend";
import OnProcess from "./onprocess";
import Finish from "./finish";


export default class OrderManage extends Component {

    constructor(props) {
        super(props);
        let {params} = this.props.navigation.state;
        this.state = {
            activeIndex: params && params.activeIndex ? params.activeIndex : 0,
        };
    }

    componentDidMount() {

    }

    render() {
        let {activeIndex} = this.state;
        let {params} = this.props.navigation.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'订单管理'}
                    leftButton={null}
                />
                <SegmentedView
                    initialPage={activeIndex}
                    ref={v => this.segmentedView = v}
                    style={styles.segmentedView}
                    barStyle={styles.segmentedBar}
                    indicatorLineColor={GlobalStyles.themeColor}
                    indicatorLineWidth={4}
                    indicatorPositionPadding={ScaleSize(-4)}
                    scrollEnabled={true}
                    // lazy={false}
                    keyboardShouldPersistTaps={'always'}
                >
                    <View
                        title={'新派遣'}
                        style={styles.navBarItemView}
                        titleStyle={styles.sheetTitle}
                        activeTitleStyle={styles.sheetActiveTitle}
                    >
                        <OrderList
                            status={0}
                            {...this.props}
                        />
                    </View>
                    <View
                        title={'进行中'}
                        style={styles.navBarItemView}
                        titleStyle={styles.sheetTitle}
                        activeTitleStyle={styles.sheetActiveTitle}
                    >
                        <OrderList
                            status={1}
                            {...this.props}
                        />
                    </View>
                    <View
                        title={'已完成'}
                        style={styles.navBarItemView}
                        titleStyle={styles.sheetTitle}
                        activeTitleStyle={styles.sheetActiveTitle}
                    >
                        <OrderList
                            status={2}
                            {...this.props}
                        />
                    </View>
                </SegmentedView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    segmentedView: {
        backgroundColor: '#fff',
    },
    segmentedBar: {
        height: 45,
        borderBottomWidth: 1,
        borderBottomColor: '#dbdbdb',
    },
    sheetActiveTitle: {
        color: GlobalStyles.themeColor,
        fontSize: FontSize(13),
    },
    sheetTitle: {
        color: '#999',
        fontSize: FontSize(13),
    },
    navBarItemView: {
        flex: 1,
        width: GlobalStyles.width,
    },
});
