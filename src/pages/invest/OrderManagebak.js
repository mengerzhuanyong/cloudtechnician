/**
 * 云技师 - 订单管理
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'

import {SegmentedView, Label} from 'teaset';


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'

import NewSend from './newsend'
import OnProcess from './onprocess'
import Finish from './finish'
import Judge from "./Judge";


export default class OrderManage extends Component {

    constructor(props) {
        super(props);
        let {params} = this.props.navigation.state;
        this.state = {
            activeIndex: params && params.activeIndex ? params.activeIndex : 0,
        };
    }

    componentDidMount() {
        this.loadNetData();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    loadNetData = () => {

    }

    onPushNavigator = (compent) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {})
    }

    render() {
        let {activeIndex} = this.state;
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={'订单管理'}
                    rightButton={UtilsView.getRightKefuBlackButton(() => this.onPushNavigator('Kefu'))}
                />
                <SegmentedView
                    style={{flex: 1,}}
                    type={'carousel'}
                    activeIndex={activeIndex}
                    indicatorPositionPadding={0}
                    barStyle={{height: 45, borderBottomColor: '#f2f2f2', borderBottomWidth: 1,}}
                    onChange={(index) => {
                        this.setState({
                            activeIndex: index,
                        })
                    }}
                >
                    <SegmentedView.Sheet
                        title='新派遣'
                        style={{backgroundColor: '#fff', flex: 1,}}
                        titleStyle={{color: '#666', fontSize: 15, fontWeight: 'bold',}}
                        activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold',}}
                    >
                        <NewSend {...this.props}/>
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet
                        title='进行中'
                        style={{backgroundColor: '#fff', flex: 1,}}
                        titleStyle={{color: '#666', fontSize: 15, fontWeight: 'bold',}}
                        activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold',}}
                    >
                        <OnProcess {...this.props}/>
                    </SegmentedView.Sheet>
                    <SegmentedView.Sheet
                        title='已完成'
                        style={{backgroundColor: '#fff', flex: 1,}}
                        titleStyle={{color: '#666', fontSize: 15, fontWeight: 'bold',}}
                        activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold',}}
                    >
                        <Finish {...this.props}/>
                    </SegmentedView.Sheet>
                    {/*<SegmentedView.Sheet*/}
                        {/*title='已评价'*/}
                        {/*style={{backgroundColor: '#fff', flex: 1,}}*/}
                        {/*titleStyle={{color: '#666', fontSize: 15, fontWeight: 'bold',}}*/}
                        {/*activeTitleStyle={{color: GlobalStyles.themeColor, fontSize: 15, fontWeight: 'bold',}}*/}
                    {/*>*/}
                        {/*<Judge {...this.props}/>*/}
                    {/*</SegmentedView.Sheet>*/}
                </SegmentedView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
});
