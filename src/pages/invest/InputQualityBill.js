/**
 * 速芽物流 - WebName
 * http://menger.me
 * @大梦
 */

import React, {Component} from 'react'
import {
    Text,
    View,
    Image,
    TextInput,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    StatusBar
} from 'react-native'


import NavigationBar from '../../components/common/NavigationBar'
import UtilsView from '../../utils/utilsView'
import {toastShort, consoleLog} from '../../utils/utilsToast'

import SYImagePicker from 'react-native-syan-image-picker'
import Yinhangka from "../fund/yinhangka";

const options = {
    imageCount: 1,             // 最大选择图片数目，默认6
    isCamera: true,            // 是否允许用户在内部拍照，默认true
    isCrop: false,             // 是否允许裁剪，默认false
    CropW: ~~(GlobalStyles.width * 0.6),    // 裁剪宽度，默认屏幕宽度60%
    CropH: ~~(GlobalStyles.width * 0.6),    // 裁剪高度，默认屏幕宽度60%
    isGif: false,              // 是否允许选择GIF，默认false，暂无回调GIF数据
    showCropCircle: false,     // 是否显示圆形裁剪区域，默认false
    circleCropRadius: GlobalStyles.width / 2,  // 圆形裁剪半径，默认屏幕宽度一半
    showCropFrame: true,       // 是否显示裁剪区域，默认true
    showCropGrid: false,       // 是否隐藏裁剪区域网格，默认false
    quality: 50,                // 压缩质量
    enableBase64: true
};
/**
 * 质保单录入页面
 */
export default class InputQualityBill extends Component {

    constructor(props) {
        super(props);
        const {params} = this.props.navigation.state;
        consoleLog('params', params);
        this.state = {
            orderInfo: params && params.orderInfo ? params.orderInfo : {id: '', is_zhibao: 0},
            product_name: '',
            product_number: '',
            zhibao_number: '',
            server_time: '',
            zhibao_time: '',
            client_name: '',
            client_telephone: '',
            car_type: '',
            car_number: '',
            server_address: '',
        }
    }

    componentDidMount() {
        this.getQualityBillDetail();
    }

    onBack = () => {
        let {state, goBack} = this.props.navigation;
        state.params && state.params.onCallBack && state.params.onCallBack();
        goBack && goBack();
    }

    componentWillUnmount() {
        this.onBack();
    }

    getQualityBillDetail = async () => {
        let {orderInfo} = this.state;
        let url = ServicesApi.zhibaoinfo;
        let data = {
            id: orderInfo.id,
        };
        let result = await Services.Post(url, data, true);
        if (result && result.code == 1) {
            let {car_number, car_type, client_name,
                client_telephone, create_time, id, product_name,
                product_number, server_address, server_order_id,
                server_time, update_time, zhibao_number, zhibao_time,} = result.data;
            this.setState({
                product_name,
                product_number,
                zhibao_number,
                server_time,
                zhibao_time,
                client_name,
                client_telephone,
                car_type,
                car_number,
                server_address,
            });
        }
    }


    onPushNavigator = (compent, back_msg) => {
        const {navigate} = this.props.navigation;
        navigate(compent, {
            back_msg: back_msg,
        })
    }

    submit = () => {
        let {
            orderInfo, product_name, product_number, zhibao_number, server_time, zhibao_time,
            client_name, client_telephone, car_type, car_number, server_address
        } = this.state;
        let url = ServicesApi.addZhibao;
        let data = {
            server_order_id: orderInfo.id,
            product_name,
            product_number,
            zhibao_number,
            server_time,
            zhibao_time,
            client_name,
            client_telephone,
            car_type,
            car_number,
            server_address,
        };
        Services.Post(url, data, true)
            .then(result => {
                toastShort(result.msg);
                if (result.code == 1) {
                    this.onBack();
                }
            })
            .catch(error => {
                toastShort('error');
            })
    };

    render() {
        let {orderInfo, product_name, product_number, zhibao_number, server_time, zhibao_time,
            client_name, client_telephone, car_type, car_number, server_address} = this.state;
        let editAble = orderInfo.is_zhibao === 0;
        let {params} = this.props.navigation.state;
        let pageTitle = params && params.pageTitle ? params.pageTitle : '添加质保信息';
        return (
            <View style={styles.container}>
                <NavigationBar
                    title={pageTitle}
                />
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                >
                    <View style={[GlobalStyles.whiteModule, {marginTop: 10}]}>

                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>产品名称:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${product_name}`}
                                    placeholder={'请输入产品名称'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.product_name = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>产品码:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${product_number}`}
                                    placeholder={'请输入产品码'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.product_number = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>质保编号:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${zhibao_number}`}
                                    placeholder={'请输入质保编号'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.zhibao_number = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>

                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>服务时间:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${server_time}`}
                                    placeholder={'请输入服务时间'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.server_time = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>

                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>质保到期时间:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${zhibao_time}`}
                                    placeholder={'请输入质保到期时间'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.zhibao_time = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>

                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>客户名:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${client_name}`}
                                    placeholder={'请输入客户名'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.client_name = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>客户手机号:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${client_telephone}`}
                                    placeholder={'请输入客户手机号'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.client_telephone = text;
                                    }}
                                    keyboardType="numeric"
                                    maxLength={11}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>车型:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${car_type}`}
                                    placeholder={'请输入车型'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.car_type = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>车牌号:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${car_number}`}
                                    placeholder={'请输入车牌号'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.car_number = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                        <View style={GlobalStyles.userlist}>
                            <View style={GlobalStyles.userlistright}>
                                <Text style={GlobalStyles.userlisttext}>施工地点:</Text>
                                <TextInput
                                    editable={editAble}
                                    defaultValue={`${server_address}`}
                                    placeholder={'请输入施工地点'}
                                    placeholderTextColor={'#999'}
                                    onChangeText={(text) => {
                                        this.state.server_address = text;
                                    }}
                                    style={[styles.inputItemCon]}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                        </View>
                    </View>
                    {orderInfo.is_zhibao === 0 ?
                        <TouchableOpacity
                            style={[GlobalStyles.submit, {marginTop: 30}]}
                            onPress={() => {
                                this.submit()
                            }}
                        >
                            <Text style={GlobalStyles.btna}>提交</Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity
                            style={[GlobalStyles.submit, {marginTop: 30}]}
                            onPress={() => RouterHelper.goBack()}
                        >
                            <Text style={GlobalStyles.btna}>返回</Text>
                        </TouchableOpacity>
                }
                </ScrollView>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    userlistRightText: {
        position: 'absolute',
        right: 20,
        color: '#585858',
    },
    userPhoto: {
        width: 32,
        height: 32,
        position: 'absolute',
        right: 20,
        borderRadius: 16,
    },
    middle: {
        height: 30,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
    },
    backcolor: {
        backgroundColor: '#ffffff'
    },
    inputItemCon: {
        flex: 1,
        height: 45,
        fontSize: 14,
        marginLeft: 10,
        color: '#555',
    }
});
