/**
 * 云技师 - OrderList
 * http://menger.me
 * @大梦
 */


'use strict';

import React, {Component} from 'react';
import {
    View,
    Text,
    Image,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';

import SpinnerLoading from "../../components/loading/SpinnerLoading";
import ListView from "../../components/list/ListView";
import OrderItem from "../../components/item/OrderItem";
import {HorizontalLine} from "../../components/common/commonLine";

export default class OrderList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            loading: false,
            dataSource: [],
            type: this.props.type,
            status: this.props.status,
        };
        this.page = 1;
        this.pageSize = 10;
    }

    static defaultProps = {
        status: 0,
    };

    componentDidMount() {
        let {status} = this.props;
        this.requestDataSource(this.page, status);
    }

    componentWillReceiveProps(nextProps) {
        // console.log('componentWillReceiveProps---->', nextProps);
        if (nextProps.type !== this.props.type) {
            this.page = 1;
            this.requestDataSource(this.page, nextProps.type, nextProps.status);
        }
    }

    componentWillUnmount() {
        let timers = [this.timer1, this.timer2];
        ClearTimer(timers);
    }

    _captureRef = (v) => {
        this.flatListRef = v;
    };

    _keyExtractor = (item, index) => {
        return `z_${index}`
    };

    requestDataSource = async (page, status) => {
        let {dataSource} = this.state;
        let url = ServicesApi.get_list;
        let data = {
            page,
            status,
            page_size: this.pageSize,
        };
        let result = await Services.Post(url, data, true);
        let endStatus = false;
        if (result && parseInt(result.code) === 1) {
            endStatus = result.data.order_list.length < data.page_size;
            if (data.page === 1) {
                dataSource = result.data.order_list;
            } else {
                let temp = dataSource;
                if (result.data.order_list.length !== 0) {
                    dataSource = temp.concat(result.data.order_list)
                }
            }
        } else {
            endStatus = true;
        }
        this.setState({
            ready: true,
            dataSource,
        });
        this.flatListRef && this.flatListRef.stopRefresh();
        this.flatListRef && this.flatListRef.stopEndReached({allLoad: endStatus});
    };

    _onRefresh = () => {
        this.page = 1;
        let {status} = this.props;
        this.requestDataSource(this.page, status);
    };

    _onEndReached = () => {
        this.page++;
        let {status} = this.props;
        this.requestDataSource(this.page, status);
    };

    _renderSeparator = () => {
        return <HorizontalLine lineStyle={styles.horLine}/>;
    };

    _renderEmptyComponent = () => {
        return (
            <View style={GlobalStyles.emptyComponentView}>
                <Text style={GlobalStyles.emptyText}>亲！您还没有相关的订单哦</Text>
            </View>
        );
    };

    _renderListItem = ({item, index}) => {
        return (
            <OrderItem
                item={item}
                onPushToDetail={() => this.onPushToDetail(item)}
                onRefresh={() => this._onRefresh()}
                {...this.props}
            />
        );
    };

    onPushToDetail = (item) => {
        RouterHelper.navigate('订单详情', 'OrderDetail', {
            id: item.id,
            status: item.status,
            onCallBack: () => this._onRefresh(),
        });
    };

    render() {
        let {ready, dataSource} = this.state;
        return (
            <View style={styles.container}>
                {ready ?
                    <ListView
                        style={styles.listContent}
                        initialRefresh={true}
                        ref={this._captureRef}
                        data={dataSource}
                        removeClippedSubviews={false}
                        renderItem={this._renderListItem}
                        keyExtractor={this._keyExtractor}
                        onEndReached={this._onEndReached}
                        onRefresh={this._onRefresh}
                        // ItemSeparatorComponent={this._renderSeparator}
                        ListEmptyComponent={this._renderEmptyComponent}
                    />
                    : <SpinnerLoading/>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.bgColor,
    },
    // 内容区
    content: {
        flex: 1,
    },
    headerComponentView: {},
    // 列表区
    listContent: {
        flex: 1,
        width: SCREEN_WIDTH,
    },
    horLine: {
        backgroundColor: '#dbdbdb',
    },
});