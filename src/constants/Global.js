/**
 * 云技师 - Global
 * http://menger.me
 * @大梦
 */


'use strict';

// 项目中的图片可以通过Images.xxx 获取
import {Platform, YellowBox} from 'react-native'
import * as Tool from '../utils/utilsTool'
import * as Toast from '../utils/utilsToast'

import InteractionManager from '../manager/InteractionManager'
import StorageManager from '../manager/StorageManager'
import RouterHelper from '../routers/RouterHelper'

import Images from './GlobalImage'
import GlobalStyles from './GlobalStyles'
import Constant from './Constant'

import ServicesApi from './GlobalApi'
import Services from '../utils/utilsRequest'

import moment from 'moment'
import 'moment/locale/zh-cn'
import ActionsManager from "../manager/ActionsManager";
import AlertManager from "../manager/AlertManager";

// 本地化
moment.locale('zh-cn');

// 发布版屏蔽日志打印
if (!__DEV__) {
    global.console = {
        info: () => { },
        log: () => { },
        warn: () => { },
        debug: () => { },
        error: () => { }
    };
}

// 禁止输出警告模式
console.disableYellowBox = true;

// 屏蔽调试警告
YellowBox.ignoreWarnings(['Remote debugger is in', 'Warning: isMounted(...)']);

// 系统是iOS
global.__IOS__ = (Platform.OS === 'ios');

// 系统是安卓
global.__ANDROID__ = (Platform.OS === 'android');

// 获取屏幕宽度
global.SCREEN_WIDTH = GlobalStyles.width;

// 获取屏幕高度
global.SCREEN_HEIGHT = GlobalStyles.height;

// 图片加载
global.Images = Images;

// 存储
global.StorageManager = StorageManager;

// 网络请求
global.Services = new Services();

// 网络接口
global.ServicesApi = ServicesApi;

// 弹窗
global.Toast = Toast;

// 时间处理
global.Moment = moment;

// 路由管理
global.RouterHelper = RouterHelper;

// 交互管理，系统的有bug,https://github.com/facebook/react-native/issues/8624
global.InteractionManager = InteractionManager;

// 全局的主题和控件的配置以及样式
global.GlobalStyles = GlobalStyles;

// 全局的主题和控件的配置以及样式
global.Constant = Constant;

// 适配字体
global.FontSize = Tool.fontSize;

// 屏幕适配
global.ScaleSize = Tool.scaleSize;

// 清楚定时器
global.ClearTimer = Tool.clearTimer;

// 清楚定时器
global.Tool = Tool;

// ActionsManager
global.ActionsManager = ActionsManager;

//用户登录数据
global.user = {
    loginState: '', //登录状态
    userData: [], //用户数据
};

// 弹窗
global.AlertManager = AlertManager;