/**
 * 速芽物流 - 全局样式
 * http://menger.me
 * @大梦
 */

import {
    Platform,
    Dimensions,
    PixelRatio,
} from 'react-native'
import {Theme} from "teaset";
import {fontSize} from "../utils/utilsTool";

const __IOS__ = Platform.OS === 'ios';
const {width, height} = Dimensions.get('window');
const themeColor = '#617995';

// 配置全局的teaset的Theme
Theme.set({
    // 开启iphoneX适配
    fitIPhoneX: true,

    asItemTitleColor: '#333',
    // 设置ActionsManager的颜色和字体大小
    asItemFontSize: fontSize(16),
    asCancelItemFontSize: fontSize(16),

    // 设置MenuManager的颜色和字体大小
    menuItemTitleColor: '#53812f',
    menuItemFontSize: fontSize(14),
    menuItemSeparatorColor: '#d8d8d8',

    // 设置ToastManager的颜色和字体大小
    toastTextColor: '#fff',
    toastFontSize: fontSize(16),
});

const GlobalStyles = {
    width: width,
    height: height,
    statusBar_Height_Ios: Theme.isIPhoneX ? 64 : 44,
    statusBar_Height_Android: 50,

    bgColor: '#ededed',
    themeColor: themeColor,
    textColor: themeColor,
    minPixel: 1 / PixelRatio.get(),
    overNavigationBar: __IOS__ ? (Theme.isIPhoneX ? -84 : -64) : -70,
    bannerContainer: {
        height: width * 409 / 700,
        position: 'relative'
    },
    bannerViewWrap: {
        flex: 1,
        position: 'relative',
    },
    bannerImg: {
        width: width,
        height: width * 409 / 700,
        resizeMode: 'cover'
    },
    bannerDot: {
        width: 10,
        height: 10,
        borderRadius: 10,
        marginHorizontal: 5,
        backgroundColor: '#d1d1d1',
        marginBottom: -15
    },
    bannerActiveDot: {
        width: 10,
        height: 10,
        borderRadius: 10,
        marginHorizontal: 5,
        backgroundColor: '#fff',
        marginBottom: -15
    },
    bannerTop: {
        position: 'absolute',
        top: 32,
        left: 0,
        width: width,
        paddingLeft: 15,
        paddingRight: 15,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },


    scTabColor: '#fff',
    scActiveTabColor: '#337ab7',
    scTabTextColor: '#337ab7',
    scActiveTabTextColor: '#fff',
    scTabTextFontSize: 14,
    scActiveTabTextFontSize: 14,
    scBorderWidth: 1 / PixelRatio.get(),



    // 弹窗提示组件的样式
    alertWidth: 300,
    alertMinHeight: 52,
    alertTitleMaxWidth: 200,
    alertDetailMaxWidth: 250,
    alertActionHeight: 42,
    alertActionColor: '#348fe4',
    alertSeparatorColor: '#eaeaea',
    alertTitleFontSize: fontSize(15),
    alertTitleColor: '#000',
    alertDetailFontSize: fontSize(12),
    alertDetailColor: '#000',
    alertActionFontSize: fontSize(13),

    // action组件
    actionMaxHeight: 230,
    actionTitleFontSize: fontSize(14),
    actionTitleColor: '#333',
    cancelTitleFontSize: fontSize(14),
    cancelTitleColor: '#333',
    titleFontSize: fontSize(12),
    titleColor: '#999',

    centerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    emptyComponentView: {
        flex: 1,
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    listEmptyTipsImg: {
        marginTop: 20,
        resizeMode: 'contain',
        width: width / 1.5,
    },
    emptyText: {
        color: '#666',
        fontSize: 14,
    },


    iconsNews: {
        width: 25,
        height: 25,

    },
    iconsKefu: {
        width: 25,
        height: 25,

    },
    flexRowStart: {
        paddingTop: 2,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    flexRowEnd: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    flexRowStartStart: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    flexRowCenter: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexRowAround: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    flexRowBetween: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    flexColumnCenter: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    flexColumnBetween: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    whiteModule: {
        marginTop: 10,
        backgroundColor: '#fff',

    },
    fundModule: {
        marginTop: 15,
        paddingRight: 15,
        paddingLeft: 15,
    },
    fundList: {
        borderRadius: 8,
        borderColor: '#ececec',
        borderWidth: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 2,
        paddingBottom: 0,
        marginBottom: 8
    },
    fundTop: {
        height: 30,
    },
    fundTitle: {
        fontSize: 20,
        color: themeColor,
    },
    fundDakuan: {
        backgroundColor: themeColor,
        color: '#fff',
        fontSize: 14,
        paddingTop: 2,
        paddingBottom: 2,
        paddingLeft: 4,
        paddingRight: 4,
        borderRadius: 3
    },
    fundMid: {
        marginTop: 5,

    },
    fundThumb: {
        width: width - 50,
        height: (width - 50) * 218 / 700,

    },
    fundMohu: {
        width: width - 50,
        height: (width - 50) * 111 / 602,

    },
    fundMohu1: {
        width: 18 * 164 / 44,
        height: 18,

    },
    fundBot: {
        marginTop: 10,
        marginBottom: 10
    },
    fundLeft: {},
    fundLeftValue: {
        color: themeColor,
        fontSize: 20,
        height: 30
    },
    fundLeftKey: {
        color: '#878787',
        fontSize: 15
    },
    fundCenter: {},
    fundCenterValue: {
        color: themeColor,
        fontSize: 20,
        height: 30
    },
    fundCenterKey: {
        color: '#878787',
        fontSize: 15
    },
    fundRight: {},
    fundRightValue: {
        color: themeColor,
        fontSize: 15,
        height: 30
    },
    fundRightKey: {
        color: '#878787',
        fontSize: 15
    },
    titleModule: {
        height: 50,
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#ececec',
    },
    titleLeft: {},
    titleIcon: {
        width: 27 * 60 / 85,
        height: 27,
        marginRight: 10
    },
    titleText: {
        fontSize: 19,
        color: '#324A66',
    },
    titleRight: {},
    titleMoreText: {
        fontSize: 16,
        color: '#666',

    },
    titleMoreIcon: {
        height: 15,
        width: 15 * 28 / 47,
        marginLeft: 5
    },
    video: {
        padding: 15,
        paddingBottom: 5
    },
    videoImg: {
        width: width - 30,
        height: (width - 30) * 378 / 700,
    },
    newsModule: {
        paddingLeft: 15,
        paddingRight: 15
    },
    newsList: {
        borderBottomColor: '#ececec',
        borderBottomWidth: 1,
        width: width - 30,
        paddingTop: 14,
        paddingBottom: 12
    },
    newsLeft: {
        width: (width - 30) * 0.35
    },
    newsThumb: {
        width: (width - 30) * 0.35,
        height: (width - 30) * 0.35 * 500 / 800,
        borderRadius: 3,
        paddingTop: 5
    },
    newsRight: {
        flex: 1,
        marginLeft: 10,
        height: (width - 30) * 0.35 * 500 / 800,
    },
    newsTitle: {
        width: 200,
        fontSize: 18,
        color: themeColor,
        height: 30,
        lineHeight: 30,
    },
    newsDesc: {
        width: 150,
        color: '#878787',
        fontSize: 14,
        height: 20,
        lineHeight: 20
    },
    newsInfo: {
        height: 30
    },
    newsDate: {
        color: '#999',
        fontSize: 12,
    },
    newsClick: {},
    newsClickNum: {
        fontSize: 12,
        color: themeColor,
    },
    newsClickText: {
        fontSize: 12,
        color: '#999',
    },
    userlist: {
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: '#f2f2f2',
        borderBottomWidth: 1,
        paddingLeft: 15,
        paddingRight: 15,
    },
    userlistleft: {
        width: 30
    },
    usericon: {
        width: 22,
        height: 22
    },
    userlistright: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // backgroundColor: '#123',
    },
    userlistrightDanwei: {
        // position: 'absolute',
        // right: 0,
        // top: 17,
        fontSize: 14,
        color: '#333',
        width: 20,
        marginLeft: 3
    },
    userlisttext: {
        color: '#333',
        fontSize: 15
    },
    userlistRightText: {
        color: '#999',
        fontSize: 14,
    },
    userlistmore: {
        width: 12 * 28 / 47,
        height: 12
    },
    mcell: {
        backgroundColor: '#fff',
        position: 'relative',
        zIndex: 1,
        padding: 15,
        paddingTop: 5
    },
    cellItem: {
        position: 'relative',
        display: 'flex',
        overflow: 'hidden',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor: '#ececec',
    },
    cellLeft: {
        color: '#333',
        fontSize: 15,
        alignItems: 'center',
        width: 90,
    },
    cellRight: {
        height: 50,
        width: width - 90,
    },
    cellRightText: {},
    cellInput: {
        height: 50,
        fontSize: 15,
        textAlign: 'left',
        color: '#525252',
        flex: 1,
    },
    inputAndroid: {
        padding: 0,
    },
    selectView: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 100,
        height: 50,
        // backgroundColor: '#000'
    },
    selectViewWrap: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 100,
    },
    paymentMethodTitleView: {
        width: 100,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    cargoAttributesTitle: {
        textAlign: 'left',
        fontSize: 14,
        color: '#bbbbc1',
        paddingLeft: 0
    },
    dropdownStyle: {
        width: 100,
        marginRight: -10,
    },
    dropdownRow: {
        height: 40,
        justifyContent: 'center',
    },
    dropdownRowText: {
        fontSize: 14,
        color: '#666',
        textAlign: 'left',
        marginLeft: 3
    },
    mradio: {
        marginBottom: 0,
        width: width - 30,
        marginTop: 15,
        marginLeft: 15
    },
    radioItem: {
        marginBottom: 10
    },
    radioLeft: {
        width: 16,
        height: 16,
        borderRadius: 8,
        borderColor: themeColor,
        borderWidth: 1,
        marginRight: 8,
        marginTop: 3
    },
    radioLeftActive: {
        borderColor: themeColor,
        backgroundColor: themeColor,
    },
    gouxuanIco: {
        width: 12,
        height: 12,
        top: 1,
        left: 1,
    },
    checkboxLeft: {
        width: 16,
        height: 16,
        borderRadius: 2,
        borderColor: themeColor,
        borderWidth: 1,
        marginRight: 8,
        marginTop: 3
    },
    radioActive: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: themeColor,
        marginTop: 2,
        marginLeft: 2
    },
    radioRight: {
        color: '#666',
        fontSize: 15,
        lineHeight: 22,
    },
    radioRightThemeColor: {
        color: themeColor,
        fontSize: 15,
        lineHeight: 22,
    },
    defaultText: {
        padding: 15,
        color: '#666',
        fontSize: 13,
        paddingTop: 10,
        paddingBottom: 10
    },
    submit: {
        margin: 15,
        marginTop: 25,
        backgroundColor: themeColor,
        height: 42,
        borderRadius: 5,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btn: {},
    btna: {
        color: '#fff',
        fontSize: 16
    },
    tishiItem: {
        padding: 15,

    },
    tishititle: {
        color: themeColor,
        height: 20,
        textAlign: 'center',
        fontSize: 16,
        marginTop: 15,
        marginBottom: 20
    },
    tishitext: {
        color: '#929292',
        lineHeight: 20,
        fontSize: 14,
        marginBottom: 5
    },
    hide: {
        display: 'none'
    },
    userNeiTop: {
        backgroundColor: themeColor,
        height: 130,
        padding: 15,
        alignItems: 'center',
    },
    userNeiTopline1: {
        fontSize: 14,
        color: '#fff',
        marginTop: 5,
        lineHeight: 24,
    },
    userNeiTopline2: {
        fontSize: 24,
        color: '#fff',
        fontWeight: 'bold',
        lineHeight: 40,
    },
    userNeiTopline3: {
        fontSize: 14,
        color: '#fff',
        marginTop: 5
    },
    mt10: {
        marginTop: 10
    },
    mt20: {
        marginTop: 20
    },
    pt0: {
        paddingTop: 0
    },
    cellTextColor: {
        color: '#bbbbc1'
    },

    singleline: {
        height: 1,
        color: '#dddddd',
        width: 100
    },

    firstMiddle: {
        paddingLeft: 13,
        paddingRight: 13,
        paddingTop: 8,
        paddingBottom: 8,
    },
    transparentNavBar: {
        borderBottomWidth: 0,
        backgroundColor: 'rgba(0,0,0,0)',
    },

};

export default GlobalStyles;