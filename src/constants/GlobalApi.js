/**
 * 云技师 - API
 * http://menger.me
 * UpdateTime: 2017/12/25 14:55
 * @大梦
 */

const HOST = 'http://yunjishi.hangtianyunmo.com';
const API_HOST = HOST + '/api/';

const ServicesApi = {

    // 主域名
    base: API_HOST,

    // 验证码
    verificationCode: 'Alisms/send_sms',

    // 版本升级
    getVersion: 'Message/version',

    // 登录
    login: 'Login/login',

    // 注册
    register: 'Login/register',

    // 忘记密码
    retrievePassword: 'Login/rewardPassword',

    // 关于我们
    about: HOST + '/index/article/info/id/3',

    // 用户协议
    protocol: HOST + '/index/article/info/id/6',

    // 联系客服
    customer_services: HOST + '/index/article/info/id/7',

    // 获取轮播图数据
    getBannerList: 'Banner/getBannerList',

    // 获取首页数据
    getIndexDataSource: 'index/index',

    // 获取公告数据
    getNoticeList: 'Message/getHomeMessages',

    // 获取产品内容
    getProductChoose: 'Article/getProductChoose',

    // 获取培训内容
    getTrainChoose: 'Article/getTrainChoose',

    // 首页工作台数据
    getOrderNumber: 'Order/getOrderNumber',

    // 获取文章排序方式
    ArticleFilter: 'Article/getArticleSort',

    // 获取文章内容列表
    ArticleList: 'Article/getArticleList',

    // 文章收藏
    contentCollect: 'Member/collectArticle',

    // 银行卡列表
    get_bank_card: 'Member/getMemberBank',

    // 添加银行卡
    insert_bank_card: 'Member/addMemberBank',

    // 获取用户基本信息
    get_member_info: 'Member/getMemberInfo',

    // 图片处理
    base64: 'Upload/qiniuBaseUpload',

    // 多图上传
    uploadImageMultiple: 'Upload/qiniuBaseUpload',

    // 收藏文章
    collectArticle: 'Member/collectArticle',

    // 修改用户资料
    put_member_info: 'Member/updateMemberInfo',

    // 获取用户最近收入列表
    getRecentmemberIncome: 'Member/getMyRecentIncome',

    // 获取个人技能认证情况
    getSkill: 'Member/getMySkill',

    // 更新工作时间
    updateMyWorkeTime: 'Member/updateMyWorkeTime',

    // 获取个人工作时间
    getMyworkTime: 'Member/getMyworkTime',

    // 获取所有技能
    getAllSkill: 'Member/getAllSkill',

    // 获取我的认证
    getMySkill: 'Member/getMySkill',

    // 提交技能认证
    addSkill: 'Member/addMyskill',

    // 收藏的文章列表
    collection_list: 'Member/getMyCollection',

    // 获取最近提现列表
    getRecentWithdrawlist: 'Member/getMyRecentWithdraw',

    // 获取订单列表
    get_list: 'Order/getOrderList',

    // 获取消息列表
    getMessageList: 'Message/getMessageList',

    // 修改订单状态
    changeOrderStatus: 'Order/changeOrderStatus',

    // 
    addWithdraw: 'Member/addWithdraw',

    // 添加质保订单
    addZhibao: 'Order/addZhibao',

    // 质保单详情
    zhibaoinfo: 'Order/zhibaoinfo',

    // 获取订单详情
    getOrderInfo: 'Order/getOrderInfo',

    // 获取首页走进我们
    getUsChoose: 'Article/getUsChoose',

    // 添加服务器相册
    addServerAlbum: 'Order/addServerAlbum',

    // 修改密码
    changepass: 'Member/changePassword',

    //  发送短信
    sendSMS: 'index/sms',

    // 修改用户密码
    change_password: 'index/change_password',

    // 退出
    logout: 'Member/logout',

    // 首页
    home: 'index/get_home_page',

    // 每日签到
    sign_in: 'member/sign_in',

    // 推广二维码
    tuiguang: 'member/tuiguang',

    // 基金列表
    fund_list: 'fund/fund_list',

    // 基金详情
    get_one: 'fund/get_one',

    // 提交投资者资料认证
    put_investor_info: 'member/put_investor_info',

    // 风险承受能力评估
    insert_risk: 'risk/insert_risk',

    // 设置投资者类型
    set_special_type: 'member/set_special_type',

    // 获取投资者审核状态
    get_investor_status: 'member/get_investor_status',

    // 是否有购买基金资格
    is_pay_auth: 'check/is_pay_auth',

    // 获取用户身份认证信息
    get_member_status: 'check/get_member_status',

    // 获取用户投资者资料
    get_investor_info: 'member/get_investor_info',

    // 设置默认银行卡
    set_default_card: 'member/set_default_card',

    // 申请购买基金
    apply_buy_fund: 'fund/apply_buy_fund',

    // 申请购买基金
    get_buy_fund_rate: 'fund/get_buy_fund_rate',

    // 确认已汇款
    confirm_payment: 'fund/confirm_payment',

    // 我的资产
    get_assets_list: 'member/get_assets_list',

    // 我的基金详情
    get_fund_detaile: 'member/get_fund_detaile',

    // 我的基金详情 - 公告列表
    get_notice_by_fund: 'fund/get_notice_by_fund',

    // 我的基金详情 - 合同协议列表
    get_file_by_fund: 'fund/get_file_by_fund',

    // 收藏基金
    collection_fund: 'fund/collection_fund',

    // 发送汇款短信
    send_bank_msg: 'fund/send_bank_msg',

    // 我的基金下面的订单列表
    get_fund_order_list: 'member/get_fund_order_list',

    // 认申购订单详情
    get_apply_buy_order_detaile: 'member/get_apply_buy_order_detaile',

    // 取消订单
    close_fund_order: 'member/close_fund_order',

    // 反悔撤销订单
    reback_fund_order: 'member/reback_fund_order',

    // 基金赎回申请
    redeem_fund: 'fund/redeem_fund',

    // 赎回订单详情
    get_redeem_order_detaile: 'member/get_redeem_order_detaile',

    // 某基金下的结算收益
    get_profit_list: 'member/get_profit_list',

    // 平台公告列表
    get_notice_list: 'fund/get_notice_list',

    // 平台消息列表
    get_msg_list: 'member/get_msg_list',

    // 修改消息状态
    read_msg: 'member/read_msg',

    // 我的推荐页面数据
    get_team_info: 'member/get_team_info',

    // 推荐注册人列表
    get_team_reg_list: 'member/get_team_reg_list',

    // 推荐绑卡人列表
    get_team_bind_card_list: 'member/get_team_bind_card_list',

    // 推荐人首投金额列表
    get_team_first_buy_list: 'member/get_team_first_buy_list',

    // 我的积分列表页面数据
    integral_list: 'member/integral_list',

    // 我的积分列表页面数据
    get_info_page: 'index/get_info_page',

    // 收益计算器
    calculator: 'fund/calculator',

    // 系统设置
    systemSetting: 'system/systemSetting',
};
export default ServicesApi;